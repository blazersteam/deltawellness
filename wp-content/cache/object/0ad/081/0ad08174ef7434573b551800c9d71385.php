���]<?php exit; ?>a:1:{s:7:"content";a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"


";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:49:"
	
	
	
	
	
	
	
	
	
	
		
		
		
		
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:27:"News –  – WordPress.org";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:26:"https://wordpress.org/news";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:14:"WordPress News";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 30 Sep 2019 21:43:39 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-US";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:40:"https://wordpress.org/?v=5.3-beta2-46370";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:10:{i:0;a:6:{s:4:"data";s:48:"
		
		
				
		
				

		
				
								
										";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:20:"WordPress 5.3 Beta 2";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:56:"https://wordpress.org/news/2019/09/wordpress-5-3-beta-2/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 30 Sep 2019 21:43:38 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7262";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:321:"WordPress 5.3 Beta 2 is now available! This software is still in development, so we don’t recommend running it on a production site. Consider setting up a test site to play with the new version. You can test the WordPress 5.3 beta in two ways: Try the WordPress Beta Tester plugin (choose the “bleeding edge [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Francesca Marano";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3037:"
<p>WordPress 5.3 Beta 2 is now available!</p>



<p><strong>This software is still in development,</strong> so we don’t recommend running it on a production site. Consider setting up a test site to play with the new version.</p>



<p>You can test the WordPress 5.3 beta in two ways:</p>



<ul><li>Try the <a href="https://wordpress.org/plugins/wordpress-beta-tester/">WordPress Beta Tester</a> plugin (choose the “bleeding edge nightlies” option)</li><li>Or <a href="https://wordpress.org/wordpress-5.3-beta2.zip">download the beta here</a> (zip).</li></ul>



<p>WordPress 5.3 is slated for release on <a href="https://make.wordpress.org/core/5-3/">November 12, 2019</a>, and we need your help to get there. </p>



<p>Thanks to the testing and feedback from everyone who tested <a href="https://wordpress.org/news/2019/09/wordpress-5-3-beta-1/">beta 1</a>, over <a href="https://core.trac.wordpress.org/query?status=closed&amp;changetime=09%2F24%2F2019..&amp;milestone=5.3&amp;group=component&amp;col=id&amp;col=summary&amp;col=owner&amp;col=type&amp;col=priority&amp;col=component&amp;col=version&amp;order=priority">45 tickets have been closed</a>&nbsp;since then. </p>



<h2>Some highlights</h2>



<ul><li>Work continues on the <strong>block editor</strong>.</li><li>Bugs fixed on<strong> Twenty Twenty</strong>.</li><li><strong>Accessibility</strong> bugs fixes and enhancements on the interface changes introduced with 5.3 beta 1:<ul><li>Iterate on the admin interface</li><li>Reduce potential backward compatibility issues</li><li>Improve consistency between admin screens and the block editor</li><li>Better text zoom management</li></ul></li><li>Support <code>rel="ugc"</code> attribute value in comments (<a href="https://core.trac.wordpress.org/ticket/48022">#48022</a>) &#8211; this particular ticket shows the WordPress project ability to integrate quick solutions to things that are changing unexpectedly – like Google new features.</li></ul>



<h2>Developer notes</h2>



<p>WordPress 5.3 has lots of refinements to polish the developer experience. To keep up, subscribe to the&nbsp;<a href="https://make.wordpress.org/core/">Make WordPress Core blog</a>&nbsp;and pay special attention to the&nbsp;<a href="https://make.wordpress.org/core/tag/5-3+dev-notes/">developers notes</a>&nbsp;for updates on those and other changes that could affect your products.</p>



<h2>How to Help</h2>



<p>Do you speak a language other than English? <a href="https://translate.wordpress.org/projects/wp/dev/">Help us translate WordPress into more than 100 languages</a>!</p>



<p>If you think you’ve found a bug, you can post to the <a href="https://wordpress.org/support/forum/alphabeta/">Alpha/Beta area</a> in the support forums. We’d love to hear from you! If you’re comfortable writing a reproducible bug report, <a href="https://core.trac.wordpress.org/newticket">file one on WordPress Trac</a> where you can also find a list of <a href="https://core.trac.wordpress.org/tickets/major">known bugs</a>.<br></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"7262";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:48:"
		
		
				
		
				

		
				
								
										";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:20:"WordPress 5.3 Beta 1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:56:"https://wordpress.org/news/2019/09/wordpress-5-3-beta-1/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Sep 2019 18:36:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7114";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:321:"WordPress 5.3 Beta 1 is now available! This software is still in development, so we don’t recommend running it on a production site. Consider setting up a test site to play with the new version. You can test the WordPress 5.3 beta in two ways: Try the WordPress Beta Tester plugin (choose the “bleeding edge [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Francesca Marano";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:9121:"
<p>WordPress 5.3 Beta 1 is now available!</p>



<p><strong>This software is still in development,</strong> so we don’t recommend running it on a production site. Consider setting up a test site to play with the new version.</p>



<p>You can test the WordPress 5.3 beta in two ways:</p>



<ul><li>Try the <a href="https://wordpress.org/plugins/wordpress-beta-tester/">WordPress Beta Tester</a> plugin (choose the “bleeding edge nightlies” option)</li><li>Or <a href="https://wordpress.org/wordpress-5.3-beta1.zip">download the beta here</a> (zip).</li></ul>



<p>WordPress 5.3 is slated for release on <a href="https://make.wordpress.org/core/5-3/">November 12, 2019</a>, and we need your help to get there. Here are some of the big items to test, so we can find and resolve as many bugs as possible in the coming weeks.</p>



<h2>Block Editor: features and improvements</h2>



<p>Twelve releases of the Gutenberg plugin are going to be merged into 5.3 which means there’s a long list of exciting new features.&nbsp;</p>



<p>Here are just a few of them:</p>



<ul><li>Group block and grouping interactions</li><li>Columns block improvements (width support + patterns)</li><li>Table block improvements (text alignment support, header/footer support, colors)</li><li>Gallery block improvements (reordering inline, caption support)</li><li>Separator block improvements (color support)</li><li>Latest Posts block improvements (support excerpt, content)</li><li>List block improvements (indent/outdent shortcuts, start value and reverse order support)</li><li>Button block improvements (support target, border radius)</li><li>Animations and micro interactions (moving blocks, dropdowns, and a number of small animations to improve the UX)</li><li>Accessibility Navigation Mode which will allow you to navigate with the keyboard between blocks without going into their content.</li><li>Block Style Variations API</li></ul>



<p>Plus a number of other improvements, amongst them:</p>



<ul><li>Data Module API improvements (useSelect/useEffect)</li><li>Inserter Help Panel</li><li>Extensibility: DocumentSettingsPanel</li><li>Snackbar notices</li><li>Typewriter Experience</li><li>Fix a number of Accessibility report issues</li></ul>



<p>If you want to see all the features for each release, here are direct links to the release posts: <a href="https://make.wordpress.org/core/2019/09/19/whats-new-in-gutenberg-18-september/">6.5</a>, <a href="https://make.wordpress.org/core/2019/08/28/whats-new-in-gutenberg-28-august/">6.4</a>, <a href="https://make.wordpress.org/core/2019/08/14/whats-new-in-gutenberg-14-august/">6.3</a>, <a href="https://make.wordpress.org/core/2019/07/31/whats-new-in-gutenberg-31-july/">6.2</a>, <a href="https://make.wordpress.org/core/2019/07/10/whats-new-in-gutenberg-10-july/">6.1</a>, <a href="https://make.wordpress.org/core/2019/06/26/whats-new-in-gutenberg-26th-june/">6.0</a>, <a href="https://make.wordpress.org/core/2019/06/12/whats-new-in-gutenberg-12th-june/">5.9</a>, <a href="https://make.wordpress.org/core/2019/05/29/whats-new-in-gutenberg-29th-may/">5.8</a>, <a href="https://make.wordpress.org/core/2019/05/15/whats-new-in-gutenberg-15th-may/">5.7</a>, <a href="https://make.wordpress.org/core/2019/05/01/whats-new-in-gutenberg-1st-may/">5.6</a>, <a href="https://make.wordpress.org/core/2019/04/17/whats-new-in-gutenberg-17th-april/">5.5</a>, and <a href="https://make.wordpress.org/core/2019/04/03/whats-new-in-gutenberg-3rd-april/">5.4</a>.</p>



<h3>Continuous effort on performance</h3>



<p>The team working on the block editor managed to shave off 1.5 seconds of loading time for a particularly sizeable post (~ 36,000 words, ~ 1,000 blocks) since WordPress 5.2.</p>



<h2>A new default theme: welcome Twenty Twenty</h2>



<p>WordPress 5.3 introduces <a href="https://make.wordpress.org/core/2019/09/06/introducing-twenty-twenty/">Twenty Twenty</a>, the latest default theme in our project history.&nbsp;</p>



<p>This elegant new theme is based on the WordPress theme <a href="https://www.andersnoren.se/teman/chaplin-wordpress-theme/">Chaplin</a> which was released on the WordPress.org theme directory earlier this summer.&nbsp;</p>



<p>It includes full support for the block editor, empowering users to find the right design for their message.</p>



<h2>Wait! There is more</h2>



<p>5.3 is going to be a rich release with the inclusion of numerous enhancements to interactions and the interface.</p>



<h2>Admin interface enhancements</h2>



<p>Design and Accessibility teams worked together to port some parts of Gutenberg styles into the whole wp-admin interface. Both teams are going to iterate on these changes during the 5.3 beta cycle. These improved styles fix many accessibility issues, improve color contrasts on form fields and buttons, add consistency between editor and admin interfaces, modernize the WordPress color scheme, add better zoom management, and more.</p>



<h3>Big Images are coming to WordPress</h3>



<p>Uploading non-optimized, high-resolution pictures from your smartphone isn’t a problem anymore. WordPress now supports resuming uploads when they fail as well as larger default image sizes. That way pictures you add from the block editor look their best no matter how people get to your site.</p>



<h3>Automatic image rotation during upload</h3>



<p>Your images will be correctly rotated upon upload according to the EXIF orientation. This feature was first proposed nine years ago. Never give up on your dreams to see your fixes land in WordPress!</p>



<h3>Site Health Checks</h3>



<p>The improvements introduced in 5.3 make it easier to identify and understand areas that may need troubleshooting on your site from the Tools -&gt; Health Check screen.</p>



<h3>Admin Email Verification</h3>



<p>You’ll now be periodically asked to check that your admin email address is up to date when you log in as an administrator. This reduces the chance that you’ll get locked out of your site if you change your email address.</p>



<h2>For Developers</h2>



<h3>Time/Date component fixes</h3>



<p>Developers can now work with <a href="https://make.wordpress.org/core/2019/09/23/date-time-improvements-wp-5-3/">dates and timezones</a> in a more reliable way. Date and time functionality has received a number of new API functions for unified timezone retrieval and PHP interoperability, as well as many bug fixes.</p>



<h3>PHP 7.4 Compatibility</h3>



<p>The WordPress core team is actively preparing to support PHP 7.4 when it is released later this year. WordPress 5.3 contains <a href="https://core.trac.wordpress.org/query?status=accepted&amp;status=assigned&amp;status=closed&amp;status=new&amp;status=reopened&amp;status=reviewing&amp;keywords=~php74&amp;milestone=5.3&amp;order=priority">multiple changes</a> to remove deprecated functionality and ensure compatibility. Please test this beta release with PHP 7.4 to ensure all functionality continues to work as expected and does not raise any new warnings. </p>



<h3>Other Changes for Developers</h3>



<ul><li>Multisite<ul><li>Filter sites by status<ul><li><a href="https://core.trac.wordpress.org/ticket/37392">https://core.trac.wordpress.org/ticket/37392</a>&nbsp;</li><li><a href="https://core.trac.wordpress.org/ticket/37684">https://core.trac.wordpress.org/ticket/37684</a>&nbsp;</li></ul></li><li>Save database version in site meta<ul><li><a href="https://core.trac.wordpress.org/ticket/41685">https://core.trac.wordpress.org/ticket/41685</a>&nbsp;</li></ul></li></ul></li><li>Code modernization and PHP 7.4 support<ul><li><a href="https://core.trac.wordpress.org/ticket/47678">https://core.trac.wordpress.org/ticket/47678</a>&nbsp;</li><li><a href="https://core.trac.wordpress.org/ticket/47783">https://core.trac.wordpress.org/ticket/47783</a></li></ul></li><li>Toggle password view<ul><li><a href="https://core.trac.wordpress.org/ticket/42888">https://core.trac.wordpress.org/ticket/42888</a></li></ul></li></ul>



<p>Keep your eyes on the <a href="https://make.wordpress.org/core/">Make WordPress Core blog</a> for more <a href="https://make.wordpress.org/core/tag/5-3+dev-notes/">5.3 related developer notes</a> in the coming weeks detailing other changes that you should be aware of.</p>



<h2>What’s next</h2>



<p>There have been over 400 tickets fixed in WordPress 5.3 so far with numerous bug fixes and improvements to help smooth your WordPress experience.</p>



<h2>How to Help</h2>



<p>Do you speak a language other than English? <a href="https://translate.wordpress.org/projects/wp/dev/">Help us translate WordPress into more than 100 languages</a>!</p>



<p>If you think you’ve found a bug, you can post to the <a href="https://wordpress.org/support/forum/alphabeta/">Alpha/Beta area</a> in the support forums. We’d love to hear from you! If you’re comfortable writing a reproducible bug report, <a href="https://core.trac.wordpress.org/newticket">file one on WordPress Trac</a> where you can also find a list of <a href="https://core.trac.wordpress.org/tickets/major">known bugs</a>.<br></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"7114";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:51:"
		
		
				
		
				
		

		
				
								
										";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:36:"People of WordPress: Abdullah Ramzan";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://wordpress.org/news/2019/09/people-of-wordpress-abdullah-ramzan/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 06 Sep 2019 18:21:22 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:9:"heropress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:10:"Interviews";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7086";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:391:"You’ve probably heard that WordPress is open-source software, and may know that it’s created and run by volunteers. WordPress enthusiasts share many examples of how WordPress changed people’s lives for the better. This monthly series shares some of those lesser-known, amazing stories. Meet Abdullah Ramzan, from Lahore, Punjab, Pakistan. Abdullah Ramzan was born and brought [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Yvette Sonneveld";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:6788:"
<p><em>You’ve probably heard that WordPress is open-source software, and may know that it’s created and run by volunteers. WordPress enthusiasts share many examples of how WordPress changed people’s lives for the better. This monthly series shares some of those lesser-known, amazing stories.</em></p>



<h2><strong>Meet Abdullah Ramzan, from Lahore, Punjab, Pakistan.</strong></h2>



<p>Abdullah Ramzan was born and brought up in the under-developed city of <a href="https://en.wikipedia.org/wiki/Layyah"><strong>​Layyah​</strong></a>, which is situated in Southern Punjab, Pakistan and surrounded by desert and the river ​Sindh​.</p>



<p>He graduated from college in his home town and started using a computer in ​2010​ when he joined <a href="https://gcuf.edu.pk/"><strong>​Government College University Faisalabad​</strong></a>. Abdullah’s introduction to WordPress happened while he was finishing the last semester of his degree. His final project was based in WordPress.</p>



<p>Ramzan’s late mother was the real hero in his life, helping him with his Kindergarten homework and seeing him off to school every day.&nbsp;</p>



<p>Before her heart surgery, Ramzan visited her in the hospital ICU, where she hugged him and said: ​“<strong>Don’t worry, everything will be good</strong>.” Sadly, his mother died during her surgery. However, her influence on Ramzan’s life continues.</p>



<h3><strong>Start of Ramzan’s Career:</strong></h3>



<p>After graduation, Ramzan struggled to get his first job. He first joined PressTigers<strong>​</strong> as a Software Engineer and met Khawaja Fahad Shakeel<a href="https://twitter.com/FahadShakeel"><strong>​</strong></a>, his first mentor. Shakeel provided Ramzan with endless support. Something had always felt missing in his life, but he felt like he was on the right track for the first time in his life when he joined the WordPress community.&nbsp;</p>



<h3><strong>Community – WordCamps and Meetups:</strong></h3>



<p>Although Ramzan had used WordPress since ​2015​, attending WordPress meetups and open source contributions turned out to be a game-changer for him. He learned a lot from the WordPress community and platform, and developed strong relationships with several individuals. One of them is <a href="https://twitter.com/jainnidhi03"><strong>​</strong></a>Nidhi Jain​ from Udaipur India who he works with on WordPress development. The second is <a href="https://twitter.com/desrosj"><strong>​</strong></a>Jonathan Desrosiers​ who he continues to learn a lot from.</p>



<p>In addition, Usman Khalid<a href="https://twitter.com/Usman__Khalid"><strong>​</strong></a>, the lead organizer of WC Karachi, mentored Ramzan, helping him to develop his community skills.&nbsp;</p>



<p>With the mentorship of these contributors, Ramzan is confident supporting local WordPress groups and helped to organize ​WordCamp Karachi​, where he spoke for the first time at an international level event. He believes that WordPress has contributed much to his personal identity.&nbsp;</p>



<figure class="wp-block-image"><img src="https://i0.wp.com/wordpress.org/news/files/2019/09/AbdullahRamzan.jpeg?resize=632%2C422&#038;ssl=1" alt="Abdullah Ramzan among a group of community members at WordCamp Karachi 2018" class="wp-image-7088" srcset="https://i0.wp.com/wordpress.org/news/files/2019/09/AbdullahRamzan.jpeg?resize=1024%2C683&amp;ssl=1 1024w, https://i0.wp.com/wordpress.org/news/files/2019/09/AbdullahRamzan.jpeg?resize=300%2C200&amp;ssl=1 300w, https://i0.wp.com/wordpress.org/news/files/2019/09/AbdullahRamzan.jpeg?resize=768%2C512&amp;ssl=1 768w, https://i0.wp.com/wordpress.org/news/files/2019/09/AbdullahRamzan.jpeg?w=2048&amp;ssl=1 2048w, https://i0.wp.com/wordpress.org/news/files/2019/09/AbdullahRamzan.jpeg?w=1264&amp;ssl=1 1264w, https://i0.wp.com/wordpress.org/news/files/2019/09/AbdullahRamzan.jpeg?w=1896&amp;ssl=1 1896w" sizes="(max-width: 632px) 100vw, 632px" data-recalc-dims="1" /><figcaption>Abdullah Ramzan at WordCamp Karachi 2018</figcaption></figure>



<h3><strong>WordPress and the Future:</strong></h3>



<p>As a <a href="https://www.meetup.com/WordPress-Lahore/members/?op=leaders&amp;sort=name"><strong>​co-organizer of WordPress Meetup Lahore,​</strong></a> he would love to involve more people in the community leadership team, to provide a platform for people to gather under one roof, to learn and share something with each other. </p>



<p>But he has loftier ambitions. Impressed by <a href="https://walktowc.eu/">Walk to WordCamp Europe</a>, Abdullah is seriously considering walking to WordCamp Asia. He also one day hopes for the opportunity to serve his country as a senator of Pakistan<a href="http://www.senate.gov.pk/"><strong>​</strong></a> and intends to enter the next senate election.</p>



<h3><strong>Words of Encouragement</strong></h3>



<p>Abdullah Ramzan knows there is no shortcut to success. “You have to work hard to achieve your goals,” explained Ramzan. He still has much he wishes to accomplish and hopes to be remembered for his impact on the project.</p>



<p>Abdullah believes WordPress can never die as long as people don’t stop innovating to meet new demands. The beauty of WordPress is that it is made for everyone.</p>



<p>Ramzan encouraged, “If you seriously want to do something for yourself, do something for others first. Go for open source, you’ll surely learn how to code. You’ll learn how to work in a team. Join local meetups, meet with the folks: help them, learn from them, and share ideas.”</p>



<hr class="wp-block-separator" />



<div class="wp-block-image"><figure class="alignleft is-resized"><img src="https://i1.wp.com/wordpress.org/news/files/2019/07/heropress_large_white_logo.jpg?resize=109%2C82&#038;ssl=1" alt="" class="wp-image-7025" width="109" height="82" srcset="https://i1.wp.com/wordpress.org/news/files/2019/07/heropress_large_white_logo.jpg?w=1024&amp;ssl=1 1024w, https://i1.wp.com/wordpress.org/news/files/2019/07/heropress_large_white_logo.jpg?resize=300%2C225&amp;ssl=1 300w, https://i1.wp.com/wordpress.org/news/files/2019/07/heropress_large_white_logo.jpg?resize=768%2C576&amp;ssl=1 768w" sizes="(max-width: 109px) 100vw, 109px" data-recalc-dims="1" /></figure></div>



<p><em>This post is based on an article originally published on HeroPress.com, a community initiative created by <a href="https://profiles.wordpress.org/topher1kenobe/">Topher DeRosia</a>. HeroPress highlights people in the WordPress community who have overcome barriers and whose stories would otherwise go unheard.</em></p>



<p><em>Meet more WordPress community members over at </em><a href="https://heropress.com/"><em>HeroPress.com</em></a><em>!</em></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"7086";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:51:"
		
		
				
		
				
		

		
				
								
										";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"WordPress 5.2.3 Security and Maintenance Release";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:84:"https://wordpress.org/news/2019/09/wordpress-5-2-3-security-and-maintenance-release/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 05 Sep 2019 01:51:15 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:8:"Security";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7064";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:368:"WordPress 5.2.3 is now available! This security and maintenance release features 29 fixes and enhancements. Plus, it adds a number of security fixes—see the list below. These bugs affect WordPress versions 5.2.2 and earlier; version 5.2.3 fixes them, so you&#8217;ll want to upgrade. If you haven&#8217;t yet updated to 5.2, there are also updated versions [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Jake Spurlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:7645:"
<p>WordPress 5.2.3 is now available! </p>



<p>This security and maintenance release features 29 fixes and enhancements. Plus, it adds a number of security fixes—see the list below.</p>



<p>These bugs affect WordPress versions 5.2.2 and earlier; version 5.2.3 fixes them, so you&#8217;ll want to upgrade. </p>



<p>If you haven&#8217;t yet updated to 5.2, there are also updated versions of 5.0 and earlier that fix the bugs for you.</p>



<h3>Security Updates</h3>



<ul><li>Props to&nbsp;<a href="https://blog.ripstech.com/authors/simon-scannell/">Simon Scannell of RIPS Technologies</a>&nbsp;for finding and disclosing two issues. The first, a cross-site scripting (XSS) vulnerability found in post previews by contributors. The second was a cross-site scripting vulnerability in stored comments.&nbsp;</li><li>Props to&nbsp;<a href="https://security-consulting.icu/blog/">Tim Coen</a>&nbsp;for disclosing an issue where validation and sanitization of a URL could lead to an open redirect.&nbsp;</li><li>Props to Anshul Jain for disclosing reflected cross-site scripting during media uploads.</li><li>Props to&nbsp;<a href="https://fortiguard.com/">Zhouyuan Yang of Fortinet’s FortiGuard Labs</a>&nbsp;who disclosed a vulnerability for cross-site scripting (XSS) in shortcode previews.</li><li>Props to Ian Dunn of the Core Security Team for finding and disclosing a case where reflected cross-site scripting could be found in the dashboard.</li><li>Props to Soroush Dalili (<a href="https://twitter.com/irsdl?lang=en">@irsdl</a>) from NCC Group for disclosing an issue with URL sanitization that can lead to cross-site scripting (XSS) attacks.</li><li>In addition to the above changes, we are also updating jQuery on older versions of WordPress. This change was&nbsp;<a href="https://core.trac.wordpress.org/ticket/47020">added in 5.2.1</a>&nbsp;and is now being brought to older versions.&nbsp;</li></ul>



<p>You can browse the&nbsp;<a href="https://core.trac.wordpress.org/query?status=closed&amp;resolution=fixed&amp;milestone=5.2.3&amp;order=priority">full list of changes on Trac</a>.</p>



<p>For more info, browse the full list of changes on Trac or check out the Version&nbsp;<a href="https://wordpress.org/support/wordpress-version/version-5-2-3/">5.2.3 documentation page</a>.</p>



<p>WordPress 5.2.3 is a short-cycle maintenance release. The next major release will be&nbsp;<a href="https://make.wordpress.org/core/5-3/">version 5.3.</a></p>



<p>You can download WordPress 5.2.3 from the button at the top of this page, or visit your<strong> Dashboard → Updates</strong> and click <strong>Update Now</strong>. </p>



<p>If you have sites that support automatic background updates, they&#8217;ve already started the update process.</p>



<h2>Thanks and props!</h2>



<p>This release brings together contributions from more than 62 other people. Thank you to everyone who made this release possible!</p>



<p><a href="https://profiles.wordpress.org/adamsilverstein/">Adam Silverstein</a>,&nbsp;<a href="https://profiles.wordpress.org/xknown/">Alex Concha</a>,&nbsp;<a href="https://profiles.wordpress.org/alpipego/">Alex Goller</a>,&nbsp;<a href="https://profiles.wordpress.org/afercia/">Andrea Fercia</a>,&nbsp;<a href="https://profiles.wordpress.org/aduth/">Andrew Duthie</a>,&nbsp;<a href="https://profiles.wordpress.org/azaozz/">Andrew Ozz</a>,&nbsp;<a href="https://profiles.wordpress.org/afragen/">Andy Fragen</a>, <a href="https://profiles.wordpress.org/762e5e74/">Ashish Shukla</a>,&nbsp;<a href="https://profiles.wordpress.org/wpboss/">Aslam Shekh</a>,&nbsp;<a href="https://profiles.wordpress.org/backermann1978/">backermann1978</a>,&nbsp;<a href="https://profiles.wordpress.org/cdog/">Catalin Dogaru</a>,&nbsp;<a href="https://profiles.wordpress.org/chetan200891/">Chetan Prajapati</a>,&nbsp;<a href="https://profiles.wordpress.org/aprea/">Chris Aprea</a>,&nbsp;<a href="https://profiles.wordpress.org/christophherr/">Christoph Herr</a>,&nbsp;<a href="https://profiles.wordpress.org/danmicamediacom/">dan@micamedia.com</a>,&nbsp;<a href="https://profiles.wordpress.org/diddledan/">Daniel Llewellyn</a>,&nbsp;<a href="https://profiles.wordpress.org/donmhico/">donmhico</a>,&nbsp;<a href="https://profiles.wordpress.org/iseulde/">Ella van Durpe</a>,&nbsp;<a href="https://profiles.wordpress.org/epiqueras/">epiqueras</a>,&nbsp;<a href="https://profiles.wordpress.org/fencer04/">Fencer04</a>,&nbsp;<a href="https://profiles.wordpress.org/flaviozavan/">flaviozavan</a>,&nbsp;<a href="https://profiles.wordpress.org/garrett-eclipse/">Garrett Hyder</a>,&nbsp;<a href="https://profiles.wordpress.org/pento/">Gary Pendergast</a>,&nbsp;<a href="https://profiles.wordpress.org/gqevu6bsiz/">gqevu6bsiz</a>,&nbsp;<a href="https://profiles.wordpress.org/thakkarhardik/">Hardik Thakkar</a>,&nbsp;<a href="https://profiles.wordpress.org/ianbelanger/">Ian Belanger</a>,&nbsp;<a href="https://profiles.wordpress.org/iandunn/">Ian Dunn</a>,&nbsp;<a href="https://profiles.wordpress.org/whyisjake/">Jake Spurlock</a>,&nbsp;<a href="https://profiles.wordpress.org/audrasjb/">Jb Audras</a>,&nbsp;<a href="https://profiles.wordpress.org/jeffpaul/">Jeffrey Paul</a>,&nbsp;<a href="https://profiles.wordpress.org/jikamens/">jikamens</a>,&nbsp;<a href="https://profiles.wordpress.org/johnbillion/">John Blackbourn</a>,&nbsp;<a href="https://profiles.wordpress.org/desrosj/">Jonathan Desrosiers</a>, <a href="https://profiles.wordpress.org/jorgefilipecosta/">Jorge Costa,</a> <a href="https://profiles.wordpress.org/karlgroves/">karlgroves</a>,&nbsp;<a href="https://profiles.wordpress.org/kjellr/">Kjell Reigstad</a>,&nbsp;<a href="https://profiles.wordpress.org/laurelfulford/">laurelfulford</a>,&nbsp;<a href="https://profiles.wordpress.org/majemedia/">Maje Media LLC</a>,&nbsp;<a href="https://profiles.wordpress.org/mspatovaliyski/">Martin Spatovaliyski</a>,&nbsp;<a href="https://profiles.wordpress.org/marybaum/">Mary Baum</a>,&nbsp;<a href="https://profiles.wordpress.org/monikarao/">Monika Rao</a>,&nbsp;<a href="https://profiles.wordpress.org/mukesh27/">Mukesh Panchal</a>,&nbsp;<a href="https://profiles.wordpress.org/nayana123/">nayana123</a>,&nbsp;<a href="https://profiles.wordpress.org/greatislander/">Ned Zimmerman</a>,&nbsp;<a href="https://profiles.wordpress.org/nickdaugherty/">Nick Daugherty</a>, <a href="https://profiles.wordpress.org/rabmalin/">Nilambar Sharma</a>,&nbsp;<a href="https://profiles.wordpress.org/nmenescardi/">nmenescardi</a>,&nbsp;<a href="https://profiles.wordpress.org/bassgang/">Paul Vincent Beigang</a>,&nbsp;<a href="https://profiles.wordpress.org/pedromendonca/">Pedro Mendonça</a>,&nbsp;<a href="https://profiles.wordpress.org/peterwilsoncc/">Peter Wilson</a>,&nbsp;<a href="https://profiles.wordpress.org/sergeybiryukov/">Sergey Biryukov</a>,&nbsp;<a href="https://profiles.wordpress.org/vjik/">Sergey Predvoditelev</a>,&nbsp;<a href="https://profiles.wordpress.org/sharaz/">Sharaz Shahid</a>,&nbsp;<a href="https://profiles.wordpress.org/sstoqnov/">Stanimir Stoyanov</a>,&nbsp;<a href="https://profiles.wordpress.org/ryokuhi/">Stefano Minoia</a>,&nbsp;<a href="https://profiles.wordpress.org/karmatosed/">Tammie Lister</a>,&nbsp;<a href="https://profiles.wordpress.org/isabel_brison/">tellthemachines</a>,&nbsp;<a href="https://profiles.wordpress.org/tmatsuur/">tmatsuur</a>,&nbsp;<a href="https://profiles.wordpress.org/vaishalipanchal/">Vaishali Panchal</a>,&nbsp;<a href="https://profiles.wordpress.org/vortfu/">vortfu</a>,&nbsp;<a href="https://profiles.wordpress.org/tsewlliw/">Will West</a>, and&nbsp;<a href="https://profiles.wordpress.org/yarnboy/">yarnboy</a>.</p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"7064";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:48:"
		
		
				
		
				

		
				
								
										";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"The Month in WordPress: August 2019";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:70:"https://wordpress.org/news/2019/09/the-month-in-wordpress-august-2019/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 02 Sep 2019 10:00:13 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:18:"Month in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7059";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:374:"This has been a particularly busy month, with a number of interesting and ambitious proposals for the WordPress project along with active progress across the entire community. Core Development and Schedule The upcoming minor release of WordPress, v5.2.3, is currently in the release candidate phase and available for testing. Following that, the next major release [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Hugh Lashbrooke";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:9644:"
<p>This has been a particularly busy month, with a number of interesting and ambitious proposals for the WordPress project along with active progress across the entire community. </p>



<hr class="wp-block-separator" />



<h2>Core Development and Schedule</h2>



<p>The upcoming minor release of WordPress, v5.2.3, is currently <a href="https://make.wordpress.org/core/2019/08/22/wordpress-5-2-3-rc-1/">in the release candidate phase</a> and available for testing.</p>



<p>Following that, the next major release is v5.3 and the Core team has laid out <a href="https://make.wordpress.org/core/2019/08/21/wordpress-5-3-schedule-and-scope/">a schedule and scope</a> for development. In addition, <a href="https://make.wordpress.org/core/2019/08/27/bug-scrub-schedule-for-5-3/">a bug scrub schedule</a> and <a href="https://make.wordpress.org/accessibility/2019/08/28/wordpress-5-3-accessibility-focused-bug-scrub-schedule/">an accessibility-focused schedule</a> have been set out to provide dedicated times for contributors to work on ironing out the bugs in the release.</p>



<p>Want to get involved in building WordPress Core? Follow <a href="https://make.wordpress.org/core/">the Core team blog</a>, and join the #core channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>.</p>



<h2>Proposal for User Privacy Improvements</h2>



<p>The Core Privacy Team <a href="https://make.wordpress.org/core/2019/08/07/feature-plugin-discussion-a-consent-and-logging-mechanism-for-user-privacy/">has proposed a feature plugin</a> to build a consent and logging mechanism for user privacy. This project will focus on improving the user privacy controls in WordPress Core in order to protect site owners and users alike.</p>



<p>The proposal includes some useful information about building effective controls for users, how other projects have worked on similar efforts, and what kind of time and resources the project will need in order to be developed.</p>



<p>Want to get involved in this feature project? Follow <a href="https://make.wordpress.org/core/">the Core team blog</a>, and join the #core-privacy channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a> where there are open office hours every Wednesday at 19:00 UTC.</p>



<h2>Core Notification System Proposal</h2>



<p><a href="https://make.wordpress.org/core/2019/08/05/feature-project-proposal-wp-notify/">A proposal has been made</a> for a new feature project to build a robust notification system for WordPress Core. The aim of the project is to build a system to handle notifications for site owners that can be extended by plugin and theme developers.</p>



<p>This proposal comes on the back of <a href="https://core.trac.wordpress.org/ticket/43484">a Trac ticket</a> opened 18 months ago. With weekly meetings to discuss the project, the team behind WP Notify are <a href="https://make.wordpress.org/core/2019/08/28/wp-notify-meeting-recap-august-26-2019/">in the planning phase</a> while they establish exactly how to develop the feature.<br></p>



<p>Want to get involved in this feature project? Follow <a href="https://make.wordpress.org/core/">the Core team blog</a>, and join the #core channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a> &#8211; meetings for this project happen every Monday at 14:00 and 22:00 UTC.</p>



<h2>Local WordPress Development Environment</h2>



<p>Members of the Core Team <a href="https://make.wordpress.org/core/2019/08/05/wordpress-local-environment/">have put together a local development environment for WordPress</a> that runs on Docker. This environment provides an easy way for developers to get involved with WordPress core development. </p>



<p>The work on this was inspired by the environment used for local Gutenberg development, <a href="https://make.wordpress.org/core/2019/08/30/gutenberg-local-environment-rewrite/">which has since been improved</a> based on the new work that has been done here.</p>



<p><a href="https://make.wordpress.org/core/2019/08/05/wordpress-local-environment/">The announcement post</a> explains how to use the Docker environment. If you have any feedback or bug reports, please comment on the post directly.</p>



<h2>Updates for Older Versions of WordPress</h2>



<p>On July 30, the Security Team shared that security updates need to undergo the same testing and release process for every major version of WordPress. This means they have to provide long-term support for over fifteen major versions of WordPress. This requires a lot of time and effort, and <a href="https://make.wordpress.org/core/2019/07/29/should-security-fixes-continue-to-be-backported-to-very-old-versions-of-wordpress/">the team has sought feedback on potential solutions for this challenge</a>. </p>



<p>Following this discussion, <a href="https://make.wordpress.org/core/2019/08/07/proposal-auto-update-old-versions-to-4-7/">a proposal was made to auto-update old versions of WordPress to v4.7</a>. This proposal garnered many responses and has since been updated to incorporate feedback from comments. The current recommendation is to secure the six latest versions and to eventually auto-update all older versions of WordPress to 4.7. Since this proposal was made, it has been discussed at <a href="https://make.wordpress.org/hosting/2019/08/26/hosting-meeting-notes-august-19-2019/">Hosting Team meetings</a> and <a href="https://make.wordpress.org/core/2019/08/16/follow-up-discussion-on-major-auto-updates/">Dev Chat meetings</a>, and the conversation is still ongoing.</p>



<p>Want to provide feedback on this proposal? Comment on <a href="https://make.wordpress.org/core/2019/08/07/proposal-auto-update-old-versions-to-4-7/">the original post</a> with your thoughts.</p>



<hr class="wp-block-separator" />



<h2>Further Reading:</h2>



<ul><li>The recommended minimum PHP version for WordPress Core <a href="https://make.wordpress.org/core/2019/08/13/increasing-the-recommended-php-version-in-core/">has been increased to 7.0</a>.</li><li>Gutenberg development continues at a rapid pace with <a href="https://make.wordpress.org/core/2019/08/28/whats-new-in-gutenberg-28-august/">new updates</a> coming out every month.</li><li>The Core Team is kicking off bug scrub and triage sessions <a href="https://make.wordpress.org/core/2019/08/26/apac-triage-and-bug-scrub-sessions/">at APAC-friendly times</a>.</li><li>WordCamp US announced <a href="https://2019.us.wordcamp.org/schedule/">the event schedule</a> to take place on November 1-3.</li><li>The Plugin Team reminded developers that <a href="https://make.wordpress.org/plugins/2019/08/23/reminder-developers-must-comply-with-the-forum-guidelines/">they need to stick to the Plugin Directory forum guidelines</a> if they choose to use them for support.</li><li>WordPress project leadership is looking at <a href="https://make.wordpress.org/updates/2019/07/30/update-sanctions-and-open-source/">how to respond to political sanctions</a> in light of the open-source nature of the project.&nbsp;</li><li>The Community Team has proposed <a href="https://make.wordpress.org/community/2019/08/19/proposal-speaker-feedback-tool/">a WordCamp speaker feedback tool</a> that will allow more reliable and consistent feedback for WordCamps speakers all over the world.</li><li>The Five for the Future project now has <a href="https://make.wordpress.org/updates/2019/08/29/five-for-the-future-proposed-scope-and-mockups/">more complete mockups</a> and a plan to move forward.</li><li>The Theme Review Team decided to terminate the Trusted Authors program for a number of reasons <a href="https://make.wordpress.org/themes/2019/08/14/trusted-author-program-a-year-of-its-journey/">outlined in the announcement post</a>.</li><li>The Design Team is taking a look at <a href="https://make.wordpress.org/design/2019/08/28/discussion-about-the-about-page/">how they can improve the About page</a> in future WordPress releases.</li><li>This month saw <a href="https://make.wordpress.org/cli/2019/08/14/wp-cli-release-v2-3-0/">the release of v2.3 of WP-CLI</a>, including a number of new commands and improvements.</li><li>WordCamp websites can now make use of <a href="https://make.wordpress.org/community/2019/08/19/wordcamp-blocks-are-live/">custom blocks in the block editor</a> for crafting their content.</li><li>The Mobile Team are looking for testers for the v13.2 release of the <a href="https://make.wordpress.org/mobile/2019/08/27/call-for-testing-wordpress-for-android-13-2/">Android</a> and <a href="https://make.wordpress.org/mobile/2019/08/29/call-for-testing-wordpress-for-ios-13-2/">iOS</a> apps.</li><li>The WordCamp Asia team <a href="https://2020.asia.wordcamp.org/2019/08/20/wordcamp-asia-logo-a-design-journey">published an interesting look</a> at the journey they took to design the event logo.</li><li><a href="https://make.wordpress.org/community/2019/08/26/call-for-volunteers-2020-global-sponsorship-working-group/">A working group of volunteers is being formed</a> to work out the details for the Global Sponsorship Program in 2020.</li><li>In an effort to increase the accessibility of available WordPress themes, the Theme Review Team now requires that <a href="https://make.wordpress.org/themes/2019/08/03/planning-for-keyboard-navigation/">all themes include keyboard navigation</a>.</li></ul>



<p><em>Have a story that we should include in the next “Month in WordPress” post? Please </em><a href="https://make.wordpress.org/community/month-in-wordpress-submissions/"><em>submit it here</em></a><em>.</em></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"7059";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:51:"
		
		
				
		
				
		

		
				
								
										";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:32:"People of WordPress: Amanda Rush";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:67:"https://wordpress.org/news/2019/08/people-of-wordpress-amanda-rush/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 09 Aug 2019 21:23:23 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:9:"heropress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:10:"Interviews";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7047";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:373:"You’ve probably heard that WordPress is open source software, and may know that it’s created and run by volunteers. WordPress enthusiasts share many examples of how WordPress changed people’s lives for the better. This monthly series shares some of those lesser-known, amazing stories. Meet Amanda Rush from Augusta, Georgia, USA. Amanda Rush is a WordPress [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Yvette Sonneveld";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:6543:"
<p><em>You’ve probably heard that WordPress is open source software, and may know that it’s created and run by volunteers. WordPress enthusiasts share many examples of how WordPress changed people’s lives for the better. This monthly series shares some of those lesser-known, amazing stories.</em></p>



<h2><strong>Meet Amanda Rush from Augusta, Georgia, USA.</strong></h2>



<p>Amanda Rush is a WordPress advocate with a visual disability. She first started using computers in 1985, which enabled her to turn in homework to her sighted teachers. Screen reader technology for Windows was in its infancy then, so she worked in DOS almost exclusively.</p>



<p>After graduating high school, Amanda went to college to study computer science, programming with DOS-based tools since compilers for Windows were still inaccessible. As part of her computer science course of study, she learned HTML which began her career in web development.</p>



<h2>How Amanda got started with WordPress</h2>



<p>Amanda began maintaining a personal website, and eventually began publishing her own content using LiveJournal. However, controlling the way the page around her content looked was hard, and she soon outgrew the hosted solution.</p>



<p>So in 2005, Amanda bought customerservant.com, set up a very simple CMS for blogging, and started publishing there. She accepted the lack of design and content, and lack of easy customization because she wasn’t willing to code her own solution. Nor did she want to move to another hosted solution, as she liked being able to customize her own site, as well as publish content.</p>



<h3><strong>Hebrew dates led her to WordPress</strong></h3>



<p>At some point, Amanda was looking for an easy way to display the Hebrew dates alongside the Gregorian dates on her blog entries. Unfortunately, the blogging software she was using at the time, did not offer customization options at that level. She decided to research alternative solutions and came across a WordPress plugin that did just that.&nbsp;</p>



<p>The fact that WordPress would not keep her locked into a visual editor, used themes to customize styling, and offered ways to mark up content, immediately appealed to Amanda. She decided to give it a go.</p>



<h3><strong>Accessibility caused her to dive deeper</strong></h3>



<p>When the software Amanda used at work became completely inaccessible, she started learning about WordPress. While she was learning about this new software, <a href="https://en.wikipedia.org/wiki/Web_2.0">Web 2.0</a> was introduced. The lack of support for it in the screen reader she used meant that WordPress administration was completely inaccessible. To get anything done, Amanda needed to learn to find her way in WordPress’ file structure.</p>



<p>Eventually Amanda started working as an independent contractor for the largest screen reader developer in the market, Freedom Scientific. She worked from home every day and hacked on WordPress after hours.</p>



<p>Unfortunately Amanda hit a rough patch when her job at Freedom Scientific ended. Using her savings she undertook further studies for various Cisco and Red Hat certifications, only to discover that the required testing for these certifications were completely inaccessible. She could study all she wanted, but wasn’t able to receive grades to pass the courses.</p>



<p>She lost her financial aid, her health took a turn for the worse, she was diagnosed with Lupus, and lost her apartment. Amanda relocated to Augusta where she had supportive friends who offered her a couch and a roof over her head.</p>



<h3><strong>But Amanda refused to give up</strong></h3>



<p>Amanda continued to hack WordPress through all of this. It was the only stable part of her life. She wanted to help make WordPress accessible for people with disabilities, and in 2012 joined the&nbsp; WordPress Accessibility Team. Shortly after that, she finally got her own place to live, and started thinking about what she was going to do with the rest of her working life.</p>



<p>Listening to podcasts led her to take part in <a href="http://wordsesh.org/">WordSesh</a>, which was delivered completely online and enabled Amanda to participate without needing to travel. She began to interact with WordPress people on Twitter, and continued to contribute to the community as part of the WordPress Accessibility Team. Things had finally started to pick up.</p>



<h2><strong>Starting her own business</strong></h2>



<p>In 2014, Amanda officially launched her own business, <a href="http://www.customerservant.com/">Customer Servant Consultancy</a>. Since WordPress is open source, and becoming increasingly accessible, Amanda could modify WordPress to build whatever she wanted and not be at the mercy of web and application developers who know nothing about accessibility. And if she got stuck, she could tap into the community and its resources.</p>



<p>Improving her circumstances and becoming more self-sufficient means Amanda was able to take back some control over her life in general. She was able to gain independence and create her own business despite being part of the blind community, which has an 80% unemployment rate.&nbsp;</p>



<p>In her own words:</p>



<blockquote class="wp-block-quote"><p><em>We’re still fighting discrimination in the workplace, and we’re still fighting for equal access when it comes to the technology we use to do our jobs. But the beauty of WordPress and its community is that we can create opportunities for ourselves.</em></p><p><em>I urge my fellow blind community members to join me inside this wonderful thing called WordPress. Because it will change your lives if you let it.</em></p><cite>Amanda Rush, entrepreneur</cite></blockquote>



<hr class="wp-block-separator" />



<div class="wp-block-image"><figure class="alignleft is-resized"><img src="https://i0.wp.com/wordpress.org/news/files/2019/07/heropress_large_white_logo-1.jpg?fit=632%2C474&amp;ssl=1" alt="" class="wp-image-7026" width="110" height="83" /></figure></div>



<p><em>This post is based on an article originally published on HeroPress.com, a community initiative created by <a href="https://profiles.wordpress.org/topher1kenobe/">Topher DeRosia</a>. HeroPress highlights people in the WordPress community who have overcome barriers and whose stories would otherwise go unheard.</em></p>



<p><em>Meet more WordPress community members over at </em><a href="https://heropress.com/"><em>HeroPress.com</em></a><em>!</em></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"7047";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:48:"
		
		
				
		
				

		
				
								
										";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:33:"The Month in WordPress: July 2019";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:68:"https://wordpress.org/news/2019/08/the-month-in-wordpress-july-2019/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 01 Aug 2019 09:56:05 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:18:"Month in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7040";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:336:"This month has been characterized by exciting plans and big announcements &#8211; read on to find out what they are and what it all means for the future of the WordPress project. WordCamp Asia Announced The inaugural WordCamp Asia will be in Bangkok, Thailand, on February 21-23, 2020. This will be the first regional WordCamp [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Hugh Lashbrooke";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:6983:"
<p>This month has been characterized by exciting plans and big announcements &#8211; read on to find out what they are and what it all means for the future of the WordPress project.</p>



<hr class="wp-block-separator" />



<h2>WordCamp Asia Announced</h2>



<p>The inaugural WordCamp Asia will be in Bangkok, Thailand, on February 21-23, 2020. This will be the first regional WordCamp in Asia and it comes after many years of discussions and planning. You can find more information about the event <a href="https://2020.asia.wordcamp.org/">on their website</a> and subscribe to stay up to date with the latest information.</p>



<p>This is the latest flagship event in the WordCamp program, following WordCamps Europe and US. Tickets <a href="https://2020.asia.wordcamp.org/tickets/">are now on sale</a> and the <a href="https://2020.asia.wordcamp.org/call-for-speakers/">call for speakers</a> is open. Want to get involved in WordCamp Asia? Keep an eye out for volunteer applications, or buy a micro sponsor ticket. You can also join the #wcasia channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a> for updates.</p>



<h2>WordCamp US Planning Continues</h2>



<p>The WordCamp US organizing team is excited to announce some new additions to this year’s WCUS in St. Louis, Missouri, on November 1-3, 2019. The first is that there will be an onsite KidsCamp: child-friendly lessons that introduce your young one(s) to the wonderful world of WordPress.&nbsp; <a href="https://2019.us.wordcamp.org/kidscamp/">You can register your child for KidsCamp here</a>. In addition, free, onsite childcare will be provided at this year’s event &#8211; <a href="https://2019.us.wordcamp.org/child-care/">you can sign up here</a>.</p>



<p>Looking for further ways to get involved? The <a href="https://2019.us.wordcamp.org/call-for-volunteers-form/">call for volunteers is now open</a>. For more information on WordCamp US, <a href="https://2019.us.wordcamp.org/">please visit the event website</a>.</p>



<h2>Exploring Updates to the WordPress User &amp; Developer Survey</h2>



<p>To improve the annual WordPress User &amp; Developer Survey, <a href="https://make.wordpress.org/updates/2019/06/28/updates-to-the-wordpress-user-developer-survey/">a call has been made</a> for updates and additional questions that can help us all better understand how people use WordPress.</p>



<p>To improve the survey, contributor teams are suggesting topics and information that should be gathered to inform contributor work in 2020. Please add your feedback <a href="https://make.wordpress.org/updates/2019/06/28/updates-to-the-wordpress-user-developer-survey/">to the post</a>.</p>



<h2>Gutenberg Usability Testing Continues</h2>



<p>Usability tests for Gutenberg continued through June 2019, and <a href="https://make.wordpress.org/test/2019/07/10/gutenberg-usability-testing-for-june-2019/">insights from three recent videos were published</a> last month. This month’s test was similar to WordCamp Europe’s usability tests, and you can read more about those in the <a href="https://make.wordpress.org/test/2019/07/05/wceu-usability-test-results-part-one/">part one</a> and <a href="https://make.wordpress.org/test/2019/07/09/wceu-usability-test-results-part-two/">part two</a> posts. Please help by watching these videos and sharing your observations as comments on the relevant post.</p>



<p>If you want to help with usability testing, you can also join the #research channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>, or you can write a test script that can be usability tested for Gutenberg.</p>



<hr class="wp-block-separator" />



<h2>Further Reading:</h2>



<ul><li><a href="https://make.wordpress.org/updates/2019/07/23/proposal-a-wordpress-advisory-board/">A proposal has been made</a> to put together a nominated WordPress Advisory Board &#8211; this is certainly an exciting development for the project.</li><li>The Design team <a href="https://make.wordpress.org/design/2019/06/28/wceu-contribution-day-recap-design-team/">reported on the work they did</a> at the WordCamp Europe Contributor Day.</li><li>The Theme Review Team <a href="https://make.wordpress.org/themes/2019/07/22/theme-sniffer-v1-1-0-and-wpthemereview-v0-2-0-release/">has released updated versions</a> of their ThemeSniffer tool and coding standards.</li><li>The Security team <a href="https://make.wordpress.org/core/2019/07/29/should-security-fixes-continue-to-be-backported-to-very-old-versions-of-wordpress/">is looking for feedback</a> on whether security fixes should continue to be backported to very old versions of WordPress. </li><li>The Design and Community teams have worked together to come up with <a href="https://make.wordpress.org/community/2019/07/29/proposal-clearer-wordcamp-and-wordpress-chapter-meetup-logo-guidelines/">official guidelines for how WordCamp logos should be designed</a>.</li><li>The Core team has implemented <a href="https://make.wordpress.org/core/2019/07/12/php-coding-standards-changes/">a few changes</a> to the PHP coding standards within WordPress Core.</li><li>The Community Team <a href="https://make.wordpress.org/community/2019/07/26/discussion-what-to-do-in-case-of-irreconcilable-differences/">is looking for feedback</a> on a tough decision that needs to be made regarding the implementation of the licence expectations within the meetup program.</li><li>The Design team <a href="https://make.wordpress.org/design/2019/07/11/block-directory-in-wp-admin-concepts/">has presented some designs</a> for a Block Directory within the WordPress dashboard.</li><li>A recent release of WordPress saw an increase in the minimum required version of PHP &#8211; the Core team is now looking at <a href="https://make.wordpress.org/core/2019/07/29/proposal-for-increasing-recommended-php-version-in-wordpress/">increasing that minimum further</a>.</li><li>The Site Health feature was first introduced in the 5.1 release of WordPress, and at WordCamp Europe this year <a href="https://make.wordpress.org/core/2019/07/01/new-core-component-site-health/">a new Core component for the feature was added to the project structure</a>.</li><li>The Community Team has posted some interesting data regarding <a href="https://make.wordpress.org/community/2019/07/29/numbers-in-the-netherlands/">WordCamps in the Netherlands</a> over the last few years, as well as <a href="https://make.wordpress.org/community/2019/07/31/wordcamps-in-2018/">WordCamps in 2018</a>.</li><li>The WordCamp Europe team <a href="https://2019.europe.wordcamp.org/2019/07/15/survey-results/">released the results of the attendee survey</a> from this year&#8217;s event in Berlin.</li></ul>



<p><em>Have a story that we should include in the next “Month in WordPress” post? Please </em><a href="https://make.wordpress.org/community/month-in-wordpress-submissions/"><em>submit it here</em></a><em>.</em></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"7040";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:51:"
		
		
				
		
				
		

		
				
								
										";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:32:"People of WordPress: Ugyen Dorji";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:67:"https://wordpress.org/news/2019/07/people-of-wordpress-ugyen-dorji/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 12 Jul 2019 17:20:27 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:9:"heropress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:10:"Interviews";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7013";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:386:"You&#8217;ve probably heard that WordPress is open source software, and may know that it&#8217;s created and run by volunteers. WordPress enthusiasts share many examples of how WordPress changed people&#8217;s lives for the better. This monthly series shares some of those lesser-known, amazing stories. Meet Ugyen Dorji from Bhutan Ugyen lives in Bhutan, a landlocked country [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Aditya Kane";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:7264:"
<p><em>You&#8217;ve probably heard that WordPress is open source software, and may know that it&#8217;s created and run by volunteers. WordPress enthusiasts share many examples of how WordPress changed people&#8217;s lives for the better. This monthly series shares some of those lesser-known, amazing stories.</em></p>



<h2><strong>Meet Ugyen Dorji from Bhutan</strong></h2>



<p>Ugyen lives in <a href="https://en.wikipedia.org/wiki/Bhutan">Bhutan</a>, a landlocked country situated between two giant neighbors, India to the south and China to the north. He works for ServMask Inc and is responsible for the Quality Assurance process for All-in-One WP Migration plugin. <br><br>He believes in the Buddhist teaching that “the most valuable service is one rendered to our fellow humans,” and his contributions demonstrates this through his WordPress translation work and multi-lingual support projects for WordPress.</p>



<figure class="wp-block-image"><img src="https://i2.wp.com/wordpress.org/news/files/2019/07/60340743_2330687777177099_8058690662683377664_o.jpg?fit=632%2C474&amp;ssl=1" alt="" class="wp-image-7023" srcset="https://i2.wp.com/wordpress.org/news/files/2019/07/60340743_2330687777177099_8058690662683377664_o.jpg?w=1728&amp;ssl=1 1728w, https://i2.wp.com/wordpress.org/news/files/2019/07/60340743_2330687777177099_8058690662683377664_o.jpg?resize=300%2C225&amp;ssl=1 300w, https://i2.wp.com/wordpress.org/news/files/2019/07/60340743_2330687777177099_8058690662683377664_o.jpg?resize=768%2C576&amp;ssl=1 768w, https://i2.wp.com/wordpress.org/news/files/2019/07/60340743_2330687777177099_8058690662683377664_o.jpg?resize=1024%2C768&amp;ssl=1 1024w, https://i2.wp.com/wordpress.org/news/files/2019/07/60340743_2330687777177099_8058690662683377664_o.jpg?w=1264&amp;ssl=1 1264w" sizes="(max-width: 632px) 100vw, 632px" /><figcaption>Bhutanese contributors to the Dzongkha locale on WordPress Translation Day</figcaption></figure>



<h2><strong>How Ugyen started his career with WordPress</strong></h2>



<p>Back in 2016, Ugyen was looking for a new job after his former cloud company ran into financial difficulties.</p>



<p>During one interview he was asked many questions about WordPress and, although he had a basic understanding of WordPress, he struggled to give detailed answers. After that interview he resolved to develop his skills and learn as much about WordPress as he could.&nbsp;</p>



<p>A few months passed and he received a call from ServMask Inc, who had developed a plugin called All-in-One WP Migration. They offered him a position, fulfilling his wish to work with WordPress full-time. And because of that, Ugyen is now an active contributor to the WordPress community.</p>



<h3><strong>WordCamp Bangkok 2018</strong></h3>



<p>WordCamp Bangkok 2018 was a turning point event for Ugyen. WordCamps are a great opportunity to meet WordPress community members you don’t otherwise get to know, and he was able to attend his first WordCamp through the sponsorship of his company.</p>



<p>The first day of WordCamp Bangkok was a Contributor Day, where people volunteer to work together to contribute to the development of WordPress. Ugyen joined the Community team to have conversations with WordPress users from all over the world. He was able to share his ideas for supporting new speakers, events and organizers to help build the WordPress community in places where it is not yet booming.</p>



<p>During the main day of the event, Ugyen managed a photo booth for speakers, organizers, and attendees to capture their memories of WordCamp.&nbsp;He also got to take some time out to attend several presentations during the conference. What particularly stuck in Ugyen’s mind was learning that having a website content plan has been shown to lead to 100% growth in business development.</p>



<h3>Co-Organizing<strong> Thimphu</strong>&#8216;s <strong>WordPress Meetup</strong></h3>



<p>After attending WordCamp Bangkok 2018 as well as a local Meetup event, Ugyen decided to&nbsp;introduce WordPress to his home country and cities.&nbsp;</p>



<p>As one of the WordPress Translation Day organizers, he realized that his local language, Dzongkha, was not as fully translated as other languages in the WordPress Core Translation. That is when Ugyen knew that he wanted to help build his local community. He organized Thimphu’s first WordPress Meetup to coincide with WordPress Translation Day 4, and it was a huge success!</p>



<p>Like all WordPress Meetups, the Thimpu WordPress Meetup is an easygoing, volunteer-organized, non-profit meetup which covers everything related to WordPress. But it also keeps in mind the <a href="https://en.wikipedia.org/wiki/Gross_National_Happiness">Bhutanese Gross National Happiness</a> four pillars by aiming to preserve and promote their unique culture and national language.&nbsp;</p>



<h2><strong>Big dreams get accomplished one step at a time</strong></h2>



<p>Ugyen has taken an active role in preserving his national language by encouraging his community to use WordPress, including Dzongkha bloggers, online Dzongkha news outlets, and government websites.</p>



<p>And while Ugyen has only been actively involved in the community for a short period, he has contributed much to the WordPress community, including:</p>



<ul><li>becoming a Translation Contributor for WordPress Core Translation for Dzongkha;</li><li>participating in the <a href="https://wptranslationday.org/">Global WordPress Translation Day 4</a> Livestream and organizing team;</li><li>inviting WordPress Meetup Thimphu members and WordPress experts from other countries to join the <a href="https://wpbhutan.slack.com/">local Slack instance</a>;</li><li>encouraging ServMask Inc. to become an event sponsor;</li><li>providing the Dzongkha Development Commission the opportunity to involve their language experts.</li></ul>



<p>When it comes to WordPress, Ugyen particularly focuses on encouraging local and international language WordPress bloggers;&nbsp;helping startups succeed with WordPress;&nbsp;and sharing what he has learned from WordPress with his Bhutanese WordPress community.</p>



<p>As a contributor, Ugyen hopes to accomplish even more for the Bhutan and Asian WordPress Communities. His dreams for his local community are big, including teaching more people about open source, hosting a local WordCamp, and helping to organize WordCamp Asia in 2020 &#8212; all while raising awareness of his community.</p>



<hr class="wp-block-separator" />



<div class="wp-block-image"><figure class="alignleft is-resized"><img src="https://i0.wp.com/wordpress.org/news/files/2019/07/heropress_large_white_logo-1.jpg?fit=632%2C474&amp;ssl=1" alt="" class="wp-image-7026" width="110" height="83" /></figure></div>



<p><em>This post is based on an article originally published on HeroPress.com, a community initiative created by <a href="https://profiles.wordpress.org/topher1kenobe/">Topher DeRosia</a>. HeroPress highlights people in the WordPress community who have overcome barriers and whose stories would otherwise go unheard.</em></p>



<p><em>Meet more WordPress community members over at </em><a href="https://heropress.com/"><em>HeroPress.com</em></a><em>!</em></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"7013";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:48:"
		
		
				
		
				

		
				
								
										";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:33:"The Month in WordPress: June 2019";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:68:"https://wordpress.org/news/2019/07/the-month-in-wordpress-june-2019/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 01 Jul 2019 10:07:42 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:18:"Month in WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7009";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:337:"June has certainly been a busy month in the WordPress community — aside from holding the largest WordPress event ever, the project has hit a number of significant milestones and published some big announcements this past month. A Wrap for WordCamp Europe 2019 WordCamp Europe 2019 took place on June 20-22. It was the largest [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Hugh Lashbrooke";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:8174:"
<p>June has certainly been a busy month in the WordPress community — aside from holding the largest WordPress event ever, the project has hit a number of significant milestones and published some big announcements this past month.</p>



<hr class="wp-block-separator" />



<h2>A Wrap for WordCamp Europe 2019</h2>



<p>WordCamp Europe 2019 took place on June 20-22. It was the largest WordPress event ever, with 3,260 tickets sold and 2,734 attendees. The attendees came from 97 different countries and 1,722 of them had never attended WordCamp Europe before.</p>



<p>The event featured 60 speakers who delivered talks and workshops on a variety of topics over two conference days, most notably <a href="https://profiles.wordpress.org/matt/">Matt Mullenweg</a>’s keynote that included an update on the current status of WordPress Core development, along with a lively Q&amp;A session. The full session from the live stream is <a href="https://youtu.be/UE18IsncB7s?t=13033">available to watch online</a>.</p>



<p>For its eighth year, <a href="https://2019.europe.wordcamp.org/2019/06/25/wordcamp-europe-2020/">WordCamp Europe will take place in Porto, Portugal</a>. The 2020 edition of the event will be held on June 4-6. If you would like to get involved with WordCamp Europe next year, fill out <a href="https://2020.europe.wordcamp.org/2019/06/22/call-for-organisers/">the organizer application form</a>.&nbsp;</p>



<h2>Proposal for XML Sitemaps in WordPress Core</h2>



<p><a href="https://make.wordpress.org/core/2019/06/12/xml-sitemaps-feature-project-proposal/">A proposal this month</a> suggested bringing XML sitemap generation into WordPress Core. This is a feature that has traditionally been handled by plugins, which has resulted in many different implementations across different sites. It also means that many sites do not have XML sitemaps, which can be a problem because they are hugely important to having your site correctly indexed by search engines.</p>



<p>The proposal details how core sitemaps would be structured and how the team would build them, as well as what aspects of WordPress would not be considered appropriate information to be included.</p>



<p>Want to get involved in building this feature? Comment on <a href="https://make.wordpress.org/core/2019/06/12/xml-sitemaps-feature-project-proposal/">the proposal</a>, follow <a href="https://make.wordpress.org/core/">the Core team blog</a>, and join the #core channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>.</p>



<h2>Translation Milestone for the Spanish Community</h2>



<p><a href="https://twitter.com/wp_es/status/1138015568563441665">The WordPress community of Spain has worked hard</a> to make <a href="https://translate.wordpress.org/locale/es/">the es_ES locale</a> the first in the world to fully localize all of WordPress Core along with all Meta projects, apps, and the top 200 plugins. This is made possible by having the largest translation team out of any locale, consisting of 2,951 individual contributors.</p>



<p>Want to get involved in translating WordPress into our locale? Find your locale on <a href="https://translate.wordpress.org/">the translation platform</a>, follow <a href="https://make.wordpress.org/polyglots/">the Polyglots team blog</a>, and join the #polyglots channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>.</p>



<h2>WordPress 5.2.2 Maintenance Release</h2>



<p>On June 18, <a href="https://wordpress.org/news/2019/06/wordpress-5-2-2-maintenance-release/">v5.2.2 of WordPress was released</a> as a maintenance release, fixing 13 bugs and improving the Site Health feature that was first published in v5.2. If your site has not already been automatically updated to this version, you can <a href="https://wordpress.org/download/">download the update</a> or manually check for updates in your WordPress dashboard. Thanks to <a href="https://profiles.wordpress.org/audrasjb/">JB Audras</a>, <a href="https://profiles.wordpress.org/justinahinon/">Justin Ahinon</a>, and <a href="https://profiles.wordpress.org/marybaum/">Mary Baum</a> for co-leading this release, as well as the 30 other individuals who contributed to it.</p>



<p>Want to get involved in building WordPress Core? Follow <a href="https://make.wordpress.org/core/">the Core team blog</a>, and join the #core channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>.</p>



<h2>Full End to End Tests for WordPress Core</h2>



<p>On June 27, <a href="https://make.wordpress.org/core/2019/06/27/introducing-the-wordpress-e2e-tests/">e2e (end to end) testing was introduced</a> to WordPress and included in the continuous integration pipeline. E2e testing, which has been successfully used by Gutenberg, is used to simulate real user scenarios and validate process flows. Currently, the setup requires <a href="https://docs.docker.com/install/">Docker</a> to run, and a number of e2e test utilities are already available in the&nbsp; <a href="https://github.com/WordPress/gutenberg/tree/master/packages/e2e-test-utils/src">@wordpress/e2e-test-utils</a> package, in the Gutenberg repository.&nbsp;</p>



<p>Want to use this feature? The more tests that are added, the more stable future releases will be! Follow the <a href="https://make.wordpress.org/core/">the Core team blog</a>, and join the #core-js channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>.</p>



<h2>Feature Packages from the Theme Review Team</h2>



<p>Following a <a href="https://make.wordpress.org/themes/2019/06/07/proposal-theme-feature-repositories/">proposal for theme feature repositories</a>, an <a href="https://make.wordpress.org/themes/2019/06/24/feature-packages-update/">update to the features package was announced</a>. Two new packages have been created that require code review and testing. The first is an Autoload Package, a foundational package for theme developers who are not currently using Composer (although <a href="https://getcomposer.org/">Composer</a> is recommended instead of this package). The second is a Customizer Section Button Package that allows theme authors to create a link/button to any URL.</p>



<p>There are other proposed ideas for packages that require feedback and additional discussion. Want to add your suggestions and thoughts? Join the conversation on the <a href="https://make.wordpress.org/themes/2019/06/24/feature-packages-update/">Theme Review team blog</a> and join the #themereview channel in <a href="https://make.wordpress.org/chat/">the Making WordPress Slack group</a>.</p>



<hr class="wp-block-separator" />



<h2>Further Reading:</h2>



<ul><li>Development continues on the Gutenberg project, with <a href="https://make.wordpress.org/core/2019/06/26/whats-new-in-gutenberg-26th-june/">the latest release</a> including layouts for the Columns block, Snackbar notices, markup improvements, and accessibility upgrades.</li><li>The Community team <a href="https://make.wordpress.org/community/2019/06/26/wordcamp-europe-2019-recap-of-community-team-activities-at-contributor-day-plans-for-the-future/">published the results of their work</a> at the WordCamp Europe contributor day.</li><li>The Polyglots team <a href="https://make.wordpress.org/polyglots/2019/06/26/proposal-for-handling-pte-requests/">has put together a proposal</a> for a new way to handle PTE requests.</li><li>This year’s recipient of the Kim Parsell Memorial Scholarship for WordCamp US <a href="https://wordpressfoundation.org/2019/2019-kim-parsell-memorial-scholarship-recipient-carol-gann/">is Carol Gann</a>.</li><li>The Amurrio WordPress community <a href="http://wpamurrio.es/wordpress-amurrio-mega-meetup-i-edition/">hosted their first “mega meetup”</a> &#8211; this is a great event format that bridges the gap between regular meetup event and WordCamp.</li></ul>



<p><em>Have a story that we should include in the next “Month in WordPress” post? Please </em><a href="https://make.wordpress.org/community/month-in-wordpress-submissions/"><em>submit it here</em></a><em>.</em></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"7009";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:48:"
		
		
				
		
				

		
				
								
										";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"WordPress 5.2.2 Maintenance Release";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://wordpress.org/news/2019/06/wordpress-5-2-2-maintenance-release/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 18 Jun 2019 18:14:34 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"Releases";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=6993";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:348:"WordPress 5.2.2 is now available! This maintenance release fixes 13 bugs and adds a little bit of polish to the Site Health feature&#160;that made its debut in 5.2. For more info, browse the&#160;full list of changes on Trac or check out the Version 5.2.2 documentation page. WordPress 5.2.2 is a short-cycle maintenance release. The next [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"Mary Baum";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:3961:"
<p>WordPress 5.2.2 is now available! This maintenance release fixes 13 bugs and adds a little bit of polish to the Site Health feature&nbsp;<a href="https://wordpress.org/news/2019/05/jaco/">that made its debut in 5.2</a>. </p>



<p>For more info, browse the&nbsp;<a href="https://core.trac.wordpress.org/query?status=closed&amp;resolution=fixed&amp;milestone=5.2.2&amp;order=priority">full list of changes on Trac</a> or check out <a href="https://wordpress.org/support/wordpress-version/version-5-2-2/">the Version 5.2.2 documentation page.</a></p>



<p>WordPress 5.2.2 is a short-cycle maintenance release. The next major release will be version 5.3; check <a href="https://make.wordpress.org/core/">make.wordpress.org/core</a> for details as they happen.  </p>



<p>You can&nbsp;download&nbsp;<a href="https://wordpress.org/download/">WordPress 5.2.2</a>&nbsp;or visit&nbsp;<strong>Dashboard → Updates</strong>&nbsp;and click&nbsp;<strong>Update Now</strong>. Sites that support automatic background updates have already started to update automatically.</p>



<p><a href="https://profiles.wordpress.org/audrasjb/">JB Audras</a>, <a href="https://profiles.wordpress.org/justinahinon/">Justin Ahinon</a> and <a href="https://profiles.wordpress.org/marybaum/">Mary Baum</a> co-led this release, with invaluable guidance from our Executive Director, Josepha Haden Chomphosy, and contributions from 30 other contributors. Thank you to everyone who made this release possible!</p>



<p><a href="https://profiles.wordpress.org/afercia/">Andrea Fercia</a>, <a href="https://profiles.wordpress.org/aduth/">Andrew Duthie</a>, <a href="https://profiles.wordpress.org/azaozz/">Andrew Ozz</a>, <a href="https://profiles.wordpress.org/afragen/">Andy Fragen</a>, <a href="https://profiles.wordpress.org/birgire/">Birgir Erlendsson (birgire)</a>, <a href="https://profiles.wordpress.org/chetan200891/">Chetan Prajapati</a>, <a href="https://profiles.wordpress.org/davidbaumwald/">David Baumwald</a>, <a href="https://profiles.wordpress.org/dkarfa/">Debabrata Karfa</a>, <a href="https://profiles.wordpress.org/garrett-eclipse/">Garrett Hyder</a>, <a href="https://profiles.wordpress.org/jankimoradiya/">Janki Moradiya</a>, <a href="https://profiles.wordpress.org/audrasjb/">Jb Audras</a>, <a href="https://profiles.wordpress.org/jitendrabanjara1991/">jitendrabanjara1991</a>, <a href="https://profiles.wordpress.org/desrosj/">Jonathan Desrosiers</a>, <a href="https://profiles.wordpress.org/spacedmonkey/">Jonny Harris</a>, <a href="https://profiles.wordpress.org/jorgefilipecosta/">Jorge Costa</a>, <a href="https://profiles.wordpress.org/justinahinon/">Justin Ahinon</a>, <a href="https://profiles.wordpress.org/clorith/">Marius L. J.</a>, <a href="https://profiles.wordpress.org/marybaum/">Mary Baum</a>, <a href="https://profiles.wordpress.org/immeet94/">Meet Makadia</a>, <a href="https://profiles.wordpress.org/dimadin/">Milan Dinić</a>, <a href="https://profiles.wordpress.org/mukesh27/">Mukesh Panchal</a>, <a href="https://profiles.wordpress.org/palmiak/">palmiak</a>, <a href="https://profiles.wordpress.org/pedromendonca/">Pedro Mendonça</a>, <a href="https://profiles.wordpress.org/peterwilsoncc/">Peter Wilson</a>, <a href="https://profiles.wordpress.org/ramiy/">Rami Yushuvaev</a>, <a href="https://profiles.wordpress.org/youknowriad/">Riad Benguella</a>, <a href="https://profiles.wordpress.org/tinkerbelly/">sarah semark</a>, <a href="https://profiles.wordpress.org/sergeybiryukov/">Sergey Biryukov</a>, <a href="https://profiles.wordpress.org/shashank3105/">Shashank Panchal</a>, <a href="https://profiles.wordpress.org/karmatosed/">Tammie Lister</a>, <a href="https://profiles.wordpress.org/hedgefield/">Tim Hengeveld</a>, <a href="https://profiles.wordpress.org/vaishalipanchal/">vaishalipanchal</a>, <a href="https://profiles.wordpress.org/vrimill/">vrimill</a>, and <a href="https://profiles.wordpress.org/earnjam/">William Earnhardt</a></p>
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:7:"post-id";a:1:{i:0;a:5:{s:4:"data";s:4:"6993";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"href";s:32:"https://wordpress.org/news/feed/";s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:44:"http://purl.org/rss/1.0/modules/syndication/";a:2:{s:12:"updatePeriod";a:1:{i:0;a:5:{s:4:"data";s:9:"
	hourly	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:15:"updateFrequency";a:1:{i:0;a:5:{s:4:"data";s:4:"
	1	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:30:"com-wordpress:feed-additions:1";a:1:{s:4:"site";a:1:{i:0;a:5:{s:4:"data";s:8:"14607090";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";O:42:"Requests_Utility_CaseInsensitiveDictionary":1:{s:7:" * data";a:9:{s:6:"server";s:5:"nginx";s:4:"date";s:29:"Tue, 01 Oct 2019 09:11:02 GMT";s:12:"content-type";s:34:"application/rss+xml; charset=UTF-8";s:25:"strict-transport-security";s:11:"max-age=360";s:6:"x-olaf";s:3:"⛄";s:13:"last-modified";s:29:"Mon, 30 Sep 2019 21:43:39 GMT";s:4:"link";s:63:"<https://wordpress.org/news/wp-json/>; rel="https://api.w.org/"";s:15:"x-frame-options";s:10:"SAMEORIGIN";s:4:"x-nc";s:9:"HIT ord 1";}}s:5:"build";s:14:"20190920194706";}}