���]<?php exit; ?>a:1:{s:7:"content";a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"


";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:61:"
	
	
	
	




















































";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:16:"WordPress Planet";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:28:"http://planet.wordpress.org/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:2:"en";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:47:"WordPress Planet - http://planet.wordpress.org/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:50:{i:0;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:76:"WPTavern: Automattic Has Discontinued Active Development on Edit Flow Plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94429";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:87:"https://wptavern.com/automattic-has-discontinued-active-development-on-edit-flow-plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4251:"<p><a href="https://wordpress.org/plugins/edit-flow/" rel="noopener noreferrer" target="_blank">Edit Flow</a>, the modular editorial plugin that enables collaboration inside the WordPress admin, is no longer being actively developed. After no updates for nine months, Mark Warbinek, a frustrated user, contacted Automattic to ask if they have abandoned the plugin or still plan to update it. A support representative from Automattic <a href="https://wordpress.org/support/topic/edit-flow-plugin-is-closed-no-longer-supported/" rel="noopener noreferrer" target="_blank">confirmed</a> the company will no longer be updating Edit Flow:</p>
<blockquote><p>At this time there is no active development of the Edit Flow Plugin.</p>
<p>That being the case – two things I can suggest are:</p>
<p>Submitting the issue to the Github repository for the plugin. This is used to track future development of the plugin and will be a canonical place for bugs or issues to be recorded.<br />
<a href="https://github.com/Automattic/Edit-Flow">https://github.com/Automattic/Edit-Flow</a></p>
<p>It is possible to ‘fork’ the plugin and make the changes needed – or use an alternative that has already been forked like PublishPress:<br />
https://github.com/Automattic/Edit-Flow</p></blockquote>
<p>Edit Flow is active on more than 10,000 WordPress sites and its sporadic development has caused users to question whether it was abandoned several times over the years. It is still listed among the <a href="https://wpvip.com/plugins/edit-flow/" rel="noopener noreferrer" target="_blank">WordPress.com VIP plugins</a>, but will likely only be maintained for that platform going forward. <a href="https://github.com/Automattic/Edit-Flow/pull/499" rel="noopener noreferrer" target="_blank">A 10-month old PR</a> was merged on its GitHub repository as recently as 19 days ago, after the contributor began to question whether the project was abandoned.</p>
<p>In 2016, <a href="https://wptavern.com/hey-automattic-whats-going-on-with-edit-flow" rel="noopener noreferrer" target="_blank">Edit Flow went two years in between updates</a>, leaving frustrated users in the dark. After that incident, a representative from Automattic <a href="https://wptavern.com/hey-automattic-whats-going-on-with-edit-flow#comment-164215" rel="noopener noreferrer" target="_blank">said</a> the company was working on an internal effort to improve the maintenance of their own plugins in order to avoid a situation like this happening again. The company currently has 88 plugins listed in the official directory.</p>
<p><a href="https://wordpress.org/plugins/publishpress/" rel="noopener noreferrer" target="_blank">PublishPress</a> is the only alternative editorial plugin with comparable features, including an editorial calendar, notifications, editorial comments, custom statuses, and a content overview. It also offers <a href="https://publishpress.com/knowledge-base/migrate/" rel="noopener noreferrer" target="_blank">seamless migration of Edit Flow data to PublishPress</a>. A commercial version of the plugin includes additional features, such as a publishing checklist, reminders, permissions, a WooCommerce checklist, and more.</p>
<p>&#8220;I think I can speak for those users of this plugin that we are not happy with the horrible handling of this plugin, how Automattic has ignored and abandoned it, leaving users to suffer in the continuing fails this out-of-date plugin is causing,&#8221; Mark Warbinek <a href="https://wordpress.org/support/topic/edit-flow-plugin-is-closed-no-longer-supported/" rel="noopener noreferrer" target="_blank">said</a> in response to to the reply from Automattic&#8217;s support team.</p>
<p>Unfortunately, this is always a risk when using free plugins from WordPress.org, especially ones without a direct business model supporting development. In many instances the plugin author&#8217;s first priority will be maintaining it for the paying customers. In this case that is WordPress.com VIP clients. Automattic has not posted an announcement on Edit Flow&#8217;s support forums, but an official communication would go a long way towards steering users in the right direction when they inevitably come looking for signs of life in the plugin.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 01 Oct 2019 03:31:49 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"WordPress.org blog: WordPress 5.3 Beta 2";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7262";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:56:"https://wordpress.org/news/2019/09/wordpress-5-3-beta-2/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2993:"<p>WordPress 5.3 Beta 2 is now available!</p>



<p><strong>This software is still in development,</strong> so we don’t recommend running it on a production site. Consider setting up a test site to play with the new version.</p>



<p>You can test the WordPress 5.3 beta in two ways:</p>



<ul><li>Try the <a href="https://wordpress.org/plugins/wordpress-beta-tester/">WordPress Beta Tester</a> plugin (choose the “bleeding edge nightlies” option)</li><li>Or <a href="https://wordpress.org/wordpress-5.3-beta2.zip">download the beta here</a> (zip).</li></ul>



<p>WordPress 5.3 is slated for release on <a href="https://make.wordpress.org/core/5-3/">November 12, 2019</a>, and we need your help to get there. </p>



<p>Thanks to the testing and feedback from everyone who tested <a href="https://wordpress.org/news/2019/09/wordpress-5-3-beta-1/">beta 1</a>, over <a href="https://core.trac.wordpress.org/query?status=closed&changetime=09%2F24%2F2019..&milestone=5.3&group=component&col=id&col=summary&col=owner&col=type&col=priority&col=component&col=version&order=priority">45 tickets have been closed</a>&nbsp;since then. </p>



<h2>Some highlights</h2>



<ul><li>Work continues on the <strong>block editor</strong>.</li><li>Bugs fixed on<strong> Twenty Twenty</strong>.</li><li><strong>Accessibility</strong> bugs fixes and enhancements on the interface changes introduced with 5.3 beta 1:<ul><li>Iterate on the admin interface</li><li>Reduce potential backward compatibility issues</li><li>Improve consistency between admin screens and the block editor</li><li>Better text zoom management</li></ul></li><li>Support <code>rel="ugc"</code> attribute value in comments (<a href="https://core.trac.wordpress.org/ticket/48022">#48022</a>) &#8211; this particular ticket shows the WordPress project ability to integrate quick solutions to things that are changing unexpectedly – like Google new features.</li></ul>



<h2>Developer notes</h2>



<p>WordPress 5.3 has lots of refinements to polish the developer experience. To keep up, subscribe to the&nbsp;<a href="https://make.wordpress.org/core/">Make WordPress Core blog</a>&nbsp;and pay special attention to the&nbsp;<a href="https://make.wordpress.org/core/tag/5-3+dev-notes/">developers notes</a>&nbsp;for updates on those and other changes that could affect your products.</p>



<h2>How to Help</h2>



<p>Do you speak a language other than English? <a href="https://translate.wordpress.org/projects/wp/dev/">Help us translate WordPress into more than 100 languages</a>!</p>



<p>If you think you’ve found a bug, you can post to the <a href="https://wordpress.org/support/forum/alphabeta/">Alpha/Beta area</a> in the support forums. We’d love to hear from you! If you’re comfortable writing a reproducible bug report, <a href="https://core.trac.wordpress.org/newticket">file one on WordPress Trac</a> where you can also find a list of <a href="https://core.trac.wordpress.org/tickets/major">known bugs</a>.<br /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 30 Sep 2019 21:43:38 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Francesca Marano";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"BuddyPress: BuddyPress 5.0.0 “Le Gusto”";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://buddypress.org/?p=308041";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:57:"https://buddypress.org/2019/09/buddypress-5-0-0-le-gusto/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:10760:"<p>Here&#8217;s our latest major release featuring the <strong>BuddyPress REST API</strong> !!</p>



<div class="wp-block-button aligncenter is-style-squared"><a class="wp-block-button__link has-background" href="https://downloads.wordpress.org/plugin/buddypress.5.0.0.zip">Get BuddyPress 5.0.0</a></div>



<div class="wp-block-spacer"></div>



<p>We are very excited to announce the BuddyPress community the immediate availability of <strong>BuddyPress 5.0.0</strong> code-named &#8220;<strong>Le Gusto</strong>&#8220;. You can get it clicking on the above button, downloading it from our&nbsp;<a href="https://wordpress.org/plugins/buddypress/">WordPress.org plugin repository</a> or checking it out from our <a href="https://buddypress.trac.wordpress.org/browser/branches/5.0">subversion repository</a>.</p>



<p><em>NB: if you&#8217;re upgrading from a previous version of BuddyPress, please make sure to back-up your WordPress database and files before proceeding.  </em></p>



<p>You can view all the changes we made in 5.0.0 thanks to our <a href="https://codex.buddypress.org/releases/version-5-0-0/">full release note</a>. Below are the key features we want to get your attention on.</p>



<div class="wp-block-spacer"></div>



<div><span class="dashicons dashicons-rest-api"></span></div>



<div class="wp-block-spacer"></div>



<h2>The BP REST API opens a new era for BuddyPress!</h2>



<p>You can now enjoy&nbsp;REST&nbsp;API&nbsp;endpoints&nbsp;for&nbsp;members, groups, activities, private&nbsp;messages, screen notifications and extended profiles.</p>



<p>BuddyPress endpoints provide machine-readable external access to your WordPress site with a clear, standards-driven interface, paving the way for new and innovative methods of interacting with your community through plugins, themes, apps, and beyond.</p>



<p>The BP REST API opens great new opportunities to improve the way you play with the BuddyPress component features: we couldn&#8217;t resist to start building on top of it introducing&#8230; </p>



<h3>A new interface for managing group members.</h3>



<div class="wp-block-image"><a href="https://buddypress.org/wp-content/uploads/1/2019/09/group-manage-members.png"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/group-manage-members.png" alt="Screen Capture of the new Group Mange Members UI" class="wp-image-308052" /></a></div>



<p>Group administrators will love our new interface for managing group membership. Whether you&#8217;re working as a group admin on the front-end Manage tab, or as the site admin on the Dashboard, the new REST API-based tools are faster, easier to use, and more consistent.</p>



<h3>The BP REST API is fully documented</h3>



<p>The development team worked hard on the features but also took the time to <a href="https://buddypress.org/2019/09/bp-devhub-1-0/">write the documentation</a> about how to use it and how to extend it. BuddyPress developers, let&#8217;s start building great stuff for our end users: take a look at <a href="https://developer.buddypress.org/bp-rest-api/">the BP REST API developer reference</a>.</p>



<div class="wp-block-spacer"></div>



<div><span class="dashicons dashicons-buddicons-groups"></span></div>



<div class="wp-block-spacer"></div>



<h2>Improved Group invites and membership requests</h2>



<p>Thanks to the new BP Invitations API, Group invites and membership requests are now managed in a more consistent way. The BP Invitations API abstracts how these two actions are handled and allows developers to use them for any object on your site (e.g., Sites of a WordPress network).</p>



<p>Read&nbsp;more&nbsp;about&nbsp;the&nbsp;<a href="https://bpdevel.wordpress.com/2019/09/16/new-invitations-api-coming-in-buddypress-5-0/">BP&nbsp;Invitations&nbsp;API</a>.</p>



<div class="wp-block-spacer"></div>



<div><span class="dashicons dashicons-buddicons-forums"></span></div>



<div class="wp-block-spacer"></div>



<h2>Help our support volunteers help you.</h2>



<p>Knowing your WordPress and BuddyPress configuration is very important when one of our beloved support volunteers tries to help you fix an issue. That&#8217;s why we added a BuddyPress section to the Site Health Info Administration screen.</p>



<a href="https://buddypress.org/wp-content/uploads/1/2019/09/debug-buddypress.png"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/debug-buddypress.png" alt="Screen capture of the BuddyPress section of the Site Health screen." class="wp-image-308058" /></a>



<p>The panel is displayed at the bottom of the screen. It includes the BuddyPress version, active components, active template pack, and a list of other component-specific settings information.</p>



<div class="wp-block-spacer"></div>



<div class="wp-block-columns has-2-columns">
<div class="wp-block-column">
<div><span class="dashicons dashicons-heart"></span></div>
</div>



<div class="wp-block-column">
<div><span class="dashicons dashicons-wordpress-alt"></span></div>
</div>
</div>



<div class="wp-block-spacer"></div>



<h2>Improved integrations with WordPress</h2>



<h3>BP Nouveau Template Pack</h3>



<p>In BuddyPress 5.0.0, the BP Nouveau template pack looks better than ever with the Twenty Nineteen theme.</p>



<div class="wp-block-image"><a href="https://buddypress.org/wp-content/uploads/1/2019/09/edit-password.png"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/edit-password.png" alt="" class="wp-image-308069" /></a></div>



<p>Nouveau now uses the same password control as the one used in WordPress Core, for better consistency between BuddyPress and WordPress spaces.</p>



<h3>BuddyPress Blocks now have their own category into the Block Editor.</h3>



<div class="wp-block-image"><a href="https://buddypress.org/wp-content/uploads/1/2019/09/bp-blocks.png"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/bp-blocks.png" alt="" class="wp-image-308070" /></a></div>



<p>Developers building tools for the Block Editor can now add their blocks to the BuddyPress category. This change provides a foundation for organizing custom BuddyPress blocks.</p>



<p>Read more about this feature in this <a href="https://bpdevel.wordpress.com/2019/07/31/a-category-to-store-your-buddypress-blocks/">development note</a>.</p>



<div class="wp-block-image"><a href="https://buddypress.org/2018/11/buddypress-4-0-0-pequod/#comment-44752"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/matt-comment.png" alt="" class="wp-image-308075" /></a>Screen capture of the <a href="https://buddypress.org/2018/11/buddypress-4-0-0-pequod/#comment-44752">comment</a> Matt made about BuddyPress 4.0.0</div>



<p><em>PS: we know, just like Matt, you&#8217;re eager to enjoy high quality community blocks: now we have the BP REST API and this new Blocks category available in BuddyPress Core, get ready to be amazed for our next release. Fasten your seatbelts: BuddyPress blocks are arriving!</em></p>



<div class="wp-block-spacer"></div>



<div class="wp-block-image"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/pizza.png" alt="" class="wp-image-308073" /></div>



<h2>BuddyPress Le Gusto</h2>



<p>5.0.0 is code-named <strong>&#8220;Le Gusto&#8221;</strong> after the <a href="https://goo.gl/maps/tpvew6YSivZ5KX218">well known Pizza restaurant</a> in Fortaleza, Brazil. It’s the perfect place to meet with friends and start tasting new flavors like <a class="bp-suggestions-mention" href="https://buddypress.org/members/espellcaste/" rel="nofollow">@espellcaste</a>’s favorite one: the &#8220;Pizza de Camarão&#8221;. </p>



<div class="wp-block-spacer"></div>



<div><span class="dashicons dashicons-buddicons-buddypress-logo"></span></div>



<div class="wp-block-spacer"></div>



<h2>Muito Obrigado</h2>



<p>As usual, this BuddyPress release is only possible thanks to the contributions of the community. Special thanks to the following folks who contributed code and testing to the release: <a href="https://github.com/baconbro">baconbro</a>, <a href="https://profiles.wordpress.org/boonebgorges/">Boone B Gorges (boonebgorges)</a>, <a href="https://profiles.wordpress.org/joncadams/">boop (joncadams)</a>, <a href="https://profiles.wordpress.org/sbrajesh/">Brajesh Singh (sbrajesh)</a>, <a href="https://profiles.wordpress.org/dcavins/">David Cavins (dcavins)</a>, <a href="https://profiles.wordpress.org/ericlewis/">Eric Lewis (ericlewis)</a>, <a href="https://profiles.wordpress.org/geminorum/">geminorum</a>, <a href="https://profiles.wordpress.org/gingerbooch/">gingerbooch</a>, <a href="https://profiles.wordpress.org/ivinco/">Ivinco</a>, <a href="https://profiles.wordpress.org/whyisjake/">Jake Spurlock (whyisjake)</a>, <a href="https://profiles.wordpress.org/JarretC/">Jarret (JarretC)</a>, <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby (johnjamesjacoby)</a>, <a href="https://profiles.wordpress.org/klawton/">klawton</a>, <a href="https://profiles.wordpress.org/kristianngve/">Kristian Yngve (kristianngve)</a>, <a href="https://profiles.wordpress.org/maniou/">Maniou</a>, <a href="https://profiles.wordpress.org/netweblogic/">Marcus (netweblogic)</a>, <a href="https://profiles.wordpress.org/imath/">Mathieu Viet (imath)</a>, <a href="https://github.com/bhoot-biswas">Mithun Biswas</a>, <a href="https://profiles.wordpress.org/modemlooper/">modemlooper</a>, <a href="https://profiles.wordpress.org/DJPaul/">Paul Gibbs (DJPaul)</a>, <a href="https://profiles.wordpress.org/r-a-y/">r-a-y</a>, <a href="https://profiles.wordpress.org/razor90/">razor90</a>, <a href="https://profiles.wordpress.org/espellcaste/">Renato Alves (espellcaste)</a>, <a href="https://profiles.wordpress.org/slaFFik/">Slava Abakumov (slaFFik)</a>, <a href="https://profiles.wordpress.org/netweb/">Stephen Edgar (netweb)</a>, <a href="https://profiles.wordpress.org/truchot/">truchot</a>, <a href="https://profiles.wordpress.org/venutius/">Venutius</a>, <a href="https://profiles.wordpress.org/wegosi/">wegosi</a>, and of course you for using BuddyPress <img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f609.png" alt="😉" class="wp-smiley" /></p>



<div class="wp-block-spacer"></div>



<div><span class="dashicons dashicons-format-chat"></span></div>



<div class="wp-block-spacer"></div>



<h2>Feedbacks welcome!</h2>



<p>Receiving your feedback and suggestions for future versions of BuddyPress genuinely motivates and encourages our contributors. Please share&nbsp;your&nbsp;feedback about this version of BuddyPress in the comments area of this post. And of course, if you&#8217;ve found a bug: please tell us about it into our <a href="https://buddypress.org/support/">Support forums</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 30 Sep 2019 21:30:07 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:5:"imath";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:44:"WPTavern: Preparing Themes For WordPress 5.3";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94401";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://wptavern.com/preparing-themes-for-wordpress-5-3";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5011:"<p>Now that <a href="https://wptavern.com/wordpress-5-3-beta-1-ready-for-testing-includes-12-gutenberg-releases-and-new-twenty-nineteen-default-theme">WordPress 5.3 Beta 1</a> is open for testing and with the official release slated for November 12, it&#8217;s time for theme authors to begin making sure their themes are ready for several changes.</p>



<p>Most work will revolve around the block editor.  WordPress 5.3 will include versions 5.4 &#8211; 6.5 of the Gutenberg plugin, a total of 12 releases.  This makes for a lot of ground to cover.  The next release includes breaking changes.</p>



<p>For themes without custom block styles, little should change.  However, theme authors who have been building custom block designs will likely have some work to do if they haven&#8217;t kept up with the changes in the Gutenberg plugin over the past several months.</p>



<h2>Block Style Variations API Introduced</h2>



<p>WordPress 5.3 introduces new <a href="https://make.wordpress.org/core/2019/09/24/new-block-apis-in-wordpress-5-3/">server-side block style functions</a>.  This means that theme authors who prefer PHP can now register custom block style variations without writing JavaScript code.</p>



<p>The block styles feature allows theme authors to register custom styles for individual blocks.  Then, they must apply custom CSS to these styles in the editor and the front end.</p>



<p>The new functions are basic one-to-one matches to their JavaScript counterparts.  Block styles still need to be registered on a per-block basis.  Support for registering single styles to multiple blocks at once hasn&#8217;t landed in core.</p>



<h2>New Block HTML Creates Breaking Changes</h2>



<p>Despite WordPress&#8217; commitment to backward compatibility over the years, the Gutenberg team hasn&#8217;t maintained that approach with blocks.  Block HTML output in the editor and the front end has changed for some blocks.  These changes will break custom theme styles in many cases.</p>



<p>The following blocks have potential breaking changes for themes:</p>



<ul><li><strong>Group:</strong> A new inner container element was added to the markup.</li><li><strong>Table:</strong> A wrapper element was added and the block class moved to the wrapper.</li><li><strong>Gallery:</strong> Like the table block, it received the same wrapper element treatment. Galleries also support a caption for the entire gallery block.</li></ul>



<p>In my tests, the gallery block had the most obvious breaking changes. Depending on how it is styled, users could be looking at a single column of images instead of their selected number.  The core development blog has a <a href="https://make.wordpress.org/core/2019/09/27/block-editor-theme-related-updates-in-wordpress-5-3/">complete overview of the HTML changes</a> along with code examples for addressing issues.</p>



<p>It&#8217;d be interesting to see if the Gutenberg team makes similar HTML changes with other blocks in the future. Such changes make it tough for theme authors to maintain support between versions of WordPress and versions of the Gutenberg plugin.  It also bloats CSS code when attempting to maintain compatibility.  Adding an extra element doesn&#8217;t typically break things.  However, moving an element&#8217;s class to another element is a dumpster fire waiting to happen.  If these types of changes continue to happen, it could turn some theme authors away from supporting the block editor at a time when core needs to be encouraging more authors to design around it.</p>



<h2>New Block Classes Added</h2>



<p>Several <a href="https://make.wordpress.org/core/2019/09/27/block-editor-theme-related-updates-in-wordpress-5-3/">new CSS classes</a> are making their way into 5.3.  For themes that remove core block styles on the front end, they need to add support for the classes to their theme&#8217;s stylesheet.</p>



<p>WordPress is doing away with inline styles for left, right, and center text alignment. This is a welcome change because it moves CSS to its appropriate place, which is in a stylesheet.  Theme authors need to make sure they support these new classes for the following blocks.</p>



<ul><li>Heading</li><li>Paragraph</li><li>Quote</li><li>Verse</li></ul>



<p>The columns block no longer supports column-specific class names.  Version 5.3 supports custom column widths, which are handled with inline styles.  It&#8217;s unlikely this will break most themes, but it&#8217;s worth testing.</p>



<p>The separator block now supports custom colors.  It is given both the text and background color class names on the front end.  This allows theme authors to utilize the styling method they prefer.  Ideally, a border color class would exist, but the block editor does not yet support selecting a custom border color.  </p>



<p>Quick developer tip: if your theme uses a border color for the separator block, use <a href="https://css-tricks.com/currentcolor/">currentColor</a> to handle custom colors.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 30 Sep 2019 16:45:33 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:97:"WPTavern: Secure the News Project Finds 93% of Major Publishers Offer HTTPS Encryption by Default";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94365";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:107:"https://wptavern.com/secure-the-news-project-finds-93-of-major-publishers-offer-https-encryption-by-default";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4469:"<p><a href="https://securethe.news" rel="noopener noreferrer" target="_blank">Secure the News</a> is a project that was created by the <a href="https://freedom.press/" rel="noopener noreferrer" target="_blank">Freedom of the Press Foundation</a> in 2016 to track HTTPS encryption across major news organizations&#8217; websites. It lists the publications and automatically scores them on a scale of 0-100, based on HTTPS implementation according to <a href="https://securethe.news/methodology-and-metrics/" rel="noopener noreferrer" target="_blank">best practices</a>, as defined by General Services Administration (GSA) Pulse’s current criteria for modern and secure HTTPS deployment. The score is converted to an A-F letter grade.</p>
<p><a href="https://i0.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-27-at-4.29.56-PM.png?ssl=1"><img /></a></p>
<p>The primary benefits of news organizations adopting HTTPS include reader privacy and website security, but there are also other positive byproducts, such as protecting sources and preventing censorship. Secure the News provides some interesting data in its campaign to encourage more broad HTTPS adoption.</p>
<p>In 2018, after one year of collecting data on HTTPS encryption at more than 130 major world news sites, the project found that HTTPS was available on 2/3 of the sites it monitors (89 of 131), up from 1/3 in 2016. Approximately <a href="https://securethe.news/blog/how-secure-are-news-sites-report-first-year-secure-news/" rel="noopener noreferrer" target="_blank">60% of news organizations offered HTTPS encryption by default in 2018</a> and that number is up to 93% today.</p>
<p><a href="https://i2.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-27-at-5.40.27-PM.png?ssl=1"><img /></a></p>
<p>Most of the WordPress-powered major news sites, such as <a href="https://arstechnica.com/" rel="noopener noreferrer" target="_blank">Ars Technica</a>, <a href="https://time.com/" rel="noopener noreferrer" target="_blank">Time</a> and the <a href="https://nypost.com/" rel="noopener noreferrer" target="_blank">New York Post</a>, get a B ranking, with the exception of <a href="https://techcrunch.com" rel="noopener noreferrer" target="_blank">TechCrunch</a> and <a href="https://qz.com/" rel="noopener noreferrer" target="_blank">Quartz</a>, which both scored an A.</p>
<p>The most recent addition to the project is the ability to <a href="https://securethe.news/blog/introducing-regional-leaderboards-secure-news/" rel="noopener noreferrer" target="_blank">sort publications by region on the homepage</a>. Publications based in North America and Europe lead the world in having the most secure HTTPS implementations. Asia has a smaller percentage of major news sites with a score of A- or higher. Some smaller regions, such as the Middle East and North Africa, Oceana, and South America, list just a handful of news organizations but they all have a score of B or higher. Secure the News is just getting started with this feature and is accepting feedback on the project&#8217;s GitHub account.</p>
<p>In addition to promoting HTTPS adoption, the team behind Secure the News is also considering broadening its coverage to measure other ways that news sites are delivering secure content, such as whether the site has an <a href="https://en.wikipedia.org/wiki/.onion" rel="noopener noreferrer" target="_blank">onion service</a>, is <a href="https://www.torproject.org/" rel="noopener noreferrer" target="_blank">Tor project</a> friendly, or has a <a href="https://freedom.press/training/blog/first-time-they-reach-out-protect-sources-themselves/" rel="noopener noreferrer" target="_blank">confidential tip line</a>. The project also has more news sites to add and a long list of improvements they want to make to the <a href="https://securethe.news/methodology-and-metrics/" rel="noopener noreferrer" target="_blank">metrics used to rank sites</a>.</p>
<p>The code for <a href="https://securethe.news/blog/secure-news-open-source/" rel="noopener noreferrer" target="_blank">Secure the News is open source</a> (licensed under the GNU AGPL) and <a href="https://github.com/freedomofpress/securethenews" rel="noopener noreferrer" target="_blank">available on GitHub</a> for anyone who wants to contribute or fork it for use with other site categories where browsing might be sensitive, such as libraries, adult sites, educational institutes, or medical facilities.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 27 Sep 2019 23:58:59 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:77:"WPTavern: Rebirth of Creativity: Gutenberg and the Future of WordPress Themes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94231";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:87:"https://wptavern.com/rebirth-of-creativity-gutenberg-and-the-future-of-wordpress-themes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:10060:"<p>I began using WordPress in 2005.  I&#8217;d already been learning HTML and CSS for a couple of years.  I even had a home-brewed blog that pulled posts from plain text files at one point.  I knew enough JavaScript to do pop-up alerts and other annoying things that served no purpose and made for a poor user experience, even if they were fun for me.</p>



<p>This was my second attempt at using WordPress.  This time it was after a botched go of making PHP Nuke behave how I wanted.  I had big dreams for my website but lacked the coding skills to make them happen.  WordPress was simple enough to hack for a novice like me at the time.  Sure, I broke my site more times than I could count, but I managed to put together my first real theme.</p>



<p>I popped open Photoshop; grabbed a few images from <em>Angel</em>, my favorite TV show at the time; and began my work.  I&#8217;d recently watched <em>Soul Purpose</em>, an episode that explored whether the titular character was truly the hero mentioned in an ancient prophecy.  It was foretold that the vampire with a soul would shed his demon half and live as a human.  It explored themes of the character&#8217;s place in the world.  At 21 years old, it&#8217;s the sort of episode that resonated with a young man who was also looking for his place.  I thought it fitting to work that into my theme&#8217;s design and began hacking away at a header for my theme.</p>



<div class="wp-block-image"><img />Screenshot of my first WordPress theme header.</div>



<p>At that time, there was this loosely-connected underground of themers and hobbyists who were building WordPress themes based on their favorite TV series, movies, comic books, and more.  That was my first real introduction to WordPress.  These people were not building themes for profit.  They were searching for their place in this small corner of the internet.  At most, some were looking for validation from like-minded people who might enjoy their art.  It was about creation for the sake of creation.  Anyone could be an artist with a simple lesson in CSS, an image manipulation program, and enough grit to pour their soul into the project for a few hours.</p>



<p>If there were ever a time that WordPress themes died, it was when the hobbyists who built for pure passion were overshadowed by business interests.</p>



<p>Don&#8217;t get me wrong; business interests played a crucial role in propelling WordPress to become the most dominant CMS in the world.  However, the balance has clearly shifted in favor of building WordPress themes for business and ecommerce rather than for the enthusiasts who just want to create.  Other platforms have better catered to these users and filled in the gaps left open by WordPress.  Tumblr became a safe-haven for popular culture fans.  DeviantArt a home for artists.  Wattpad for aspiring writers and fanfic lovers.</p>



<p>Somewhere along the way, we lost the innocence and artistry of building WordPress themes for the pure fun of it.  WordPress grew up and WordPress themes along with it.</p>



<h2>Today&#8217;s Themes Are Not Tomorrow&#8217;s</h2>



<p>In his post, <a href="https://www.binarymoon.co.uk/2019/09/the-end-of-wordpress-themes-is-in-sight/">The End of WordPress Themes is in Sight</a>, Ben Gillbanks said, &#8220;Themes as we know them will no longer be made.&#8221;  It is a bleak look at the future of WordPress theming.  He notes that he doesn&#8217;t believe that he&#8217;ll be able to make a living building WordPress themes in the next couple of years.</p>



<p>His worries are warranted.  They have been shared by several theme authors over the past couple of years as the block editor (Gutenberg) was making its way into core WordPress.  The official theme review team has discussed the team&#8217;s future role surrounding the coming changes.</p>



<p>Gillbanks&#8217; post comes on the heels of a post written by Matias Ventura on <a href="https://make.wordpress.org/core/2019/09/05/defining-content-block-areas/">defining content block areas</a>.  Essentially, the idea is for WordPress to allow users to edit areas outside of the post content via the block editor.  Anything from the header, footer, sidebar, or otherwise would likely be fair game.</p>



<div class="wp-block-embed__wrapper">

</div>



<p>In such a system, themes would be relegated to defining block areas, providing base styles, and designing block output.  In many ways, this is what WordPress themes <em>should</em> be.  Some might say that WordPress is putting themes back into their proper place of simply styling content.  With the behemoth themes with hundreds or thousands of features we&#8217;ve seen over the past few years, this could be a welcome change.  </p>



<p>There&#8217;s huge potential for designers to step up and make their mark.  I, for one, wouldn&#8217;t mind seeing CSS artists unleashed in the WordPress theme ecosystem.</p>



<p>Gillbanks went on to say:</p>



<blockquote class="wp-block-quote"><p>There are definite benefits to doing this from a user&#8217;s perspective – they will have full control of their site – but it&#8217;s going to result in some very boring website layouts.</p></blockquote>



<p>This is the point where I&#8217;ll respectfully disagree.  Putting control in the hands of non-designers will be anything but boring.</p>



<p><em>Do we all so easily forget the days of GeoCities?</em>  The websites built from it may have been horribly inaccessible.  They may have blared midi files as soon as you opened a webpage.  They may have even had a flashing, scrolling marquee zipping across the header.  Boring is not the word I&#8217;d use to describe them.</p>



<p>As much as many of us want to put those days behind us (Come on, you had one of those sites at one point, right? Tell the truth.), there was something fascinating about it all.  Real people built these sites because they were fun.  The sites told you something about that person.  It was a deeply personal look into this stranger&#8217;s world.  Sometimes it was just a bunch of junk spewed onto the screen, but most sites were a reflection of the site owners at that point in time.</p>



<p>It was ugly and beautiful all the same.</p>



<p>Web developers and designers joke about those dark days of the web.  It&#8217;s easy to look back at sites from the &#8217;90s and cringe at the silliness (It makes you wonder what designers of 2050 will think about today&#8217;s designs, doesn&#8217;t it?).  I choose to look fondly upon those days.  It was a time before I became a &#8220;designer&#8221; with rules to follow.</p>



<p>But, here&#8217;s the important point.  We are not the arbiters of the web.  It&#8217;s all about the user.  If someone wants a blinking Justin Bieber GIF in their site header, more power to them.  It&#8217;s the developer&#8217;s job to enable the user to do this in an easy-to-configure way.</p>



<p><em>Wait?  So Geocities is your argument for full-site editing in WordPress?</em></p>



<p>Understanding why WordPress should become a full-site editor means understanding the average user.  Developers are more apt to view things in a structured manner.  I spent over a decade honing my development skills.  Logic and order are old friends.  </p>



<p>With end-users, things may seem a bit more chaotic.  A teenager might want to plaster a picture of her favorite band anywhere she wants on her site.  A soccer mom might want to show her kid slamming home the winning goal.  A poet may want to showcase one of his poems as a background image on his blog.  Humans are creative beings.  While our unique brand of artistry might not appeal to others, it&#8217;s still something we crave to share.</p>



<p>It&#8217;s also important to understand that building WordPress themes is nowhere near as simple in 2019 as it was in 2005 when I started hacking away.  The code is much more complex.  It&#8217;s not quite as easy for a new user to piece together something fun as it once was.  Unless you have a theme or plugin that allows you to do this with simple drag-and-drop or similar tools, users have little control over their own sites.  And, that&#8217;s why the Gutenberg project is so revolutionary.  Its mission is to put the power back in the hands of the people.</p>



<p>Theme authors need to evolve.  They will need to find a way to balance good design principles with the insane amount of freedom users will have.  There&#8217;s nothing stopping designers from making sure the Bieber screengrab looks more presentable.</p>



<h2>Are WordPress Themes Dead?</h2>



<p>No.  But, the theme landscape will certainly change and not for the first time.  We need not look at that as a bad thing.</p>



<p>Those hobbyists who like to tinker with their site, they will once again have power that was so long ago lost to more advanced code.</p>



<p>There will also be sub-communities within the WordPress landscape.  Some people will want something more akin to classic WordPress.  Others will want a simple blog handled with Markdown (side note: I&#8217;m one of those people, and Gutenberg actually handles pasting from Markdown well).  Plugins will be built to cater to every user&#8217;s needs.  Themes will exist for different types of users.  Client builds and enterprise solutions that look nothing like core WordPress aren&#8217;t going anywhere.</p>



<p>There&#8217;s still a long road ahead.  Theme authors need to be more involved with the development of Gutenberg as these features make their way into the plugin and eventually into WordPress.  Otherwise, they&#8217;ll risk losing the opportunity to help shape the future theme landscape.</p>



<p>Truth be told, I&#8217;m not sure what themes will look like in a few years.  I have a horrible track record with predictions.  However, I think it&#8217;s safe to say that there&#8217;ll be a place for designers.</p>



<p>I&#8217;m excited because I feel like it will bring back the potential for users to have the control they once had and more.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 27 Sep 2019 18:56:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:86:"WPTavern: Rich Reviews Plugin Discontinued after Vulnerabilities Exploited in the Wild";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94302";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:97:"https://wptavern.com/rich-reviews-plugin-discontinued-after-vulnerabilities-exploited-in-the-wild";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3397:"<p>After <a href="https://www.wordfence.com/blog/2019/09/rich-reviews-plugin-vulnerability-exploited-in-the-wild/" rel="noopener noreferrer" target="_blank">tracking exploits of a zero day XSS vulnerability</a> in the <a href="https://wordpress.org/plugins/rich-reviews/" rel="noopener noreferrer" target="_blank">Rich Reviews plugin</a> for WordPress, Wordfence is recommending that users remove it from their websites. The company estimates that there are 16,000 active installations vulnerable to unauthenticated plugin option updates:</p>
<blockquote><p>Attackers are currently abusing this exploit chain to inject malvertising code into target websites. The malvertising code creates redirects and popup ads. Our team has been tracking this attack campaign since April of this year. </p></blockquote>
<p>Rich Reviews was removed from the WordPress.org Plugin Directory on March 11, 2019, due to a security issue.</p>
<p><a href="https://i2.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-27-at-10.38.09-AM.png?ssl=1"><img /></a></p>
<p>One week ago, a Rich Reviews plugin user reported 3 out of 4 of her sites using the plugin were infected with redirect scripts and that removing the plugin fixed the issue. A digital marketing agency called Nuanced Media, the author of the plugin, <a href="https://wordpress.org/support/topic/plugin-not-supported-open-to-malware-uninstall-now/#post-11953681" rel="noopener noreferrer" target="_blank">responded</a> to the post indicating that a new version would be released within two weeks:</p>
<blockquote><p>We’ve been working on an overall rewrite of this plugin for a while now, but someone out there apparently wanted us to work faster on it, and decided to exploit our plugin to get some malware out there. We’re now going double-quick on it, and hope to have it back up (and newly cozy and secure) within the next two weeks.</p></blockquote>
<p>Oddly, there seemed to be no rush to patch the issue that is currently being exploited. Yesterday, less than a week after assuring users that a new version is coming, the company behind the plugin announced that it is <a href="https://nuancedmedia.com/wordpress-rich-reviews-plugin/" rel="noopener noreferrer" target="_blank">discontinuing active support and development on Rich Reviews</a>.</p>
<p>Nuanced Media CEO Ryan Flannagan cited Google&#8217;s recent changes to its <a href="https://developers.google.com/search/docs/data-types/review-snippet#local-business-reviews" rel="noopener noreferrer" target="_blank">business review guidelines</a> as the reason for discontinuing its development.</p>
<p>&#8220;As part of this update, in the organic search results, Google has decided to remove all merchant review star ratings that businesses display on their own URL,&#8221; Flannagan said.</p>
<p>&#8220;Based on this information, we have discontinued all active development and support on Rich  Reviews. We apologize for any inconvenience.&#8221;</p>
<p>The announcement does not include any information about the vulnerability or the recent exploits. Users should assume that no patch is coming to the plugin, since it has been officially discontinued. It&#8217;s already not available to potential new users on WordPress.org, but those who have Rich Reviews active on their sites should deactivate it and remove the plugin as soon as possible to avoid getting hacked.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 27 Sep 2019 18:25:56 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:92:"WPTavern: Gatsby Raises $15M, Plans to Invest More Heavily in WordPress and CMS Integrations";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94300";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:101:"https://wptavern.com/gatsby-raises-15m-plans-to-invest-more-heavily-in-wordpress-and-cms-integrations";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5857:"<p><a href="https://www.gatsbyjs.com/" rel="noopener noreferrer" target="_blank">Gatsby Inc</a>. CEO Kyle Mathews announced a <a href="https://www.gatsbyjs.org/blog/2019-09-26-announcing-gatsby-15m-series-a-funding-round/" rel="noopener noreferrer" target="_blank">$15M Series A funding round</a> today, just one year after creating the company with GatsbyJS core contributors. The open source Gatsby project started in 2015 to provide a framework for developers to quickly build websites with React. As the project soared in popularity, Mathews formed a company to fund its ongoing development and further invest in the growing Gatsby ecosystem of products.</p>
<p>This round of funding will enable Gatsby to grow its 35-person team while investing in open source and cloud services that complement the company&#8217;s products.</p>
<p>&#8220;With Gatsby, we’re striving to create a business model that will drive many millions of dollars of investment in open-source tools and enable people to build the next generation of web experiences,&#8221; Mathews said.</p>
<p>At the forefront of the company&#8217;s vision is the idea of &#8220;reinventing website development.&#8221; Gatsby has popularized the concept of a “<a href="https://www.gatsbyjs.org/blog/2018-10-04-journey-to-the-content-mesh/" rel="noopener noreferrer" target="_blank">content mesh</a>,” a platform that provides the infrastructure layer for a decoupled website and reimagines the role of a CMS within this architecture.</p>
<p>Gatsby&#8217;s goal of creating more integrations for CMS&#8217;s was a big part of Mathews&#8217; funding announcement. Instead of writing off LAMP stack architecture as slow and obsolete, Gatsby is creating bridges to the CMS&#8217;s that power a large portion of the web:</p>
<blockquote><p>Instead of a monolithic CMS powering everything, Gatsby ties together specialized services with a modern development experience and optimized website delivery.</p>
<p>This content mesh empowers developers while preserving content creators’ workflows. It gives developers access to great cloud services without the pain of manual integration.</p>
<p>Web developers from dozens of web CMS communities like WordPress and Drupal are going “headless” and using Gatsby as the presentation layer for their CMS.</p>
<p>We’re forming partnerships with these communities to create seamless integrations between their solutions and Gatsby.</p></blockquote>
<p>Gatsby will be using some of its funding to invest more heavily in the WordPress ecosystem. The company hired <a href="https://wptavern.com/jason-bahl-joins-the-gatsby-team-to-work-on-wpgraphql-full-time" rel="noopener noreferrer" target="_blank">hiring Jason Bahl</a>, creator of the GraphQL for WordPress project, in June, and plans to add more WordPress developers.</p>
<p>&#8220;We recently hired someone else to work alongside Jason in developing WPGraphQL (announcement coming soon!) and are currently hiring for several roles on the team,&#8221; Mathews told the Tavern.</p>
<p>WordPress powers <a href="https://w3techs.com/technologies/details/cm-wordpress/all/all" rel="noopener noreferrer" target="_blank">34.6% of the top 10 million websites</a> and Matt Mullenweg has previously estimated its ecosystem to be a $10 billion industry. The CMS is <a href="https://joost.blog/cms-market-share-a-numbers-analysis/" rel="noopener noreferrer" target="_blank">showing no signs of decline</a>, and is a market that Gatsby product developers are strategically targeting.</p>
<p>WordPress adopted React as its JavaScript framework in 2017, and built its new Gutenberg editor on top of it. Although some early adopters began digging deeper into React and creating their own products with it, the majority of PHP developers have been slow to move in that direction. Gatsby provides a bridge for those who are just getting started.</p>
<p>&#8220;We think that for many web developers, a Gatsby project may be the first time they are using React, GraphQL, webpack or even Node.js,&#8221; Mathews said. &#8220;And that’s not just the case for WordPress developers &#8211; the same can be true for professionals in the Drupal, Rails, or .NET ecosystems.</p>
<p>&#8220;It’s our goal to make a framework that empowers developers to use these technologies easily, then dive deeper as they gain more experience. So, instead of taking days to configure webpack for the first time, you can use a Gatsby Theme that connects to WordPress as a data source, and automatically get a blazing fast site. Later, you can learn the innards of the system, and begin customizing Gatsby yourself.&#8221;</p>
<p>While Gatsby as a framework enables developers to bypass a lot of the technical and tooling jargon that has made modern development so complex, it is still a framework geared almost exclusively towards developers. Mathews said the company&#8217;s vision will continue to focus on enabling developers, not on creating solutions to make Gatsby more accessible to the non-technical crowd.</p>
<p>&#8220;We are focused on making Gatsby the best choice for WordPress developers who want a flexible and powerful presentation layer for their headless WordPress sites,&#8221; he said. &#8220;Non-technical team members or clients will still use WordPress to create and manage content, while the web developers on their team or at the agency they hired get to be productive using the best development tools available.&#8221;</p>
<p>Gatsby plans to use the funding to invest $3 million per year in open source, including the core Gatsby project, official plugins, and more learning materials. The funding is also good news for the future of the <a href="https://www.wpgraphql.com/" rel="noopener noreferrer" target="_blank">WPGraphQL</a> project, which should see deeper integration with Gatsby in the near future.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 26 Sep 2019 22:35:21 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:57:"WPTavern: Long-Needed Date/Time Improvements Land in Core";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94295";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:68:"https://wptavern.com/long-needed-date-time-improvements-land-in-core";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4026:"<div class="wp-block-image"><img /></div>



<p>After more than a year and several WordPress updates, an <a href="https://make.wordpress.org/core/2019/09/23/date-time-improvements-wp-5-3/">overhaul of the core Date/Time component</a> concluded.  WordPress 5.3 will ship with fixes for long-standing bugs and new API functions.</p>



<p>Andrey &#8220;Rarst&#8221; Savchenko spearheaded this project and worked through most of the issues in his <a href="https://github.com/Rarst/wp-date">WP Date</a> fork of WordPress.  Much of his work toward addressing the problems with this core component goes back further with the initialization of his <a href="https://github.com/Rarst/wpdatetime">WPDateTime project</a>.</p>



<p>Diving into the Date/Time component is no small feat.  Addressing one issue leads to another.  It&#8217;s a rabbit hole that few in the community have navigated.  Many developers were also unaware of the issues.  However, the bugs lingered for years, and users had no working solution for the problems they were facing.</p>



<p>The most common errors were caused by core bugs or developer errors due to compatibility issues, described Savchenko.  This would cause user-facing issues such as post scheduling and other time-based operations.</p>



<p>With the release of WordPress 5.3, all existing functions should behave more reliably.  Developers working on the component fixed several bugs and updated incorrect inline code documentation for many core functions. Along with the fixes, 5.3 will ship with new <a href="https://github.com/Rarst/wp-date/issues/4">Date/Time API functions</a>.  The updated API includes unified time zone retrieval, localization, and PHP interoperability functions.</p>



<p>Savchenko called it &#8220;the slow descent into madness&#8221; when asked of the catalyst for diving into the Date/Time component and its underlying issues.  &#8220;I started to notice serious bugs in the component from WordPress Stack Exchange questions about them, and the more I looked over years the more clear the dire state of it became to me.&#8221;</p>



<p>One of the major problems is the way WordPress handles timestamps.  &#8220;I actually had to invent the &#8216;WordPress timestamp&#8217; term,&#8221; said Savchenko.  &#8220;There was no name for it in core development and inline documentation incorrectly called these Unix timestamps before.&#8221;  WordPress adds a time zone offset to the real Unix timestamp, which causes issues with upstream PHP and external systems.  </p>



<p>WordPress timestamps couldn&#8217;t be removed from core without breaking backward compatibility.  Plugin and theme developers should avoid working with the WordPress timestamp and opt to use the <a href="https://make.wordpress.org/core/2019/09/23/date-time-improvements-wp-5-3/">recommended methods</a> outlined in Savchenko&#8217;s post.</p>



<p>WordPress date functions were originally written in PHP 4, a version of PHP so long-dead that it&#8217;s almost not worth digging up the end-of-life date (it&#8217;s <a href="https://www.php.net/eol.php">11 years</a>, by the way).  PHP 5.2 introduced the PHP <code>DateTime</code> and <code>DateTimeZone</code> classes and has continued receiving improvements over the years.  WordPress date functions were never updated to utilize newer standards.  The platform&#8217;s more recent bump to a minimum of PHP 5.6 also meant that the <code>DateTimeImmutable</code> class introduced in PHP 5.5 would be available.  The version bump helped land the new API functions in WordPress 5.3.</p>



<p>Some bugs go as far back as 7 years, such as <a href="https://core.trac.wordpress.org/ticket/20973">shorthand formats not working with the core date_i18n() function</a>, which was fixed in WordPress 5.1.  With any luck, core may also adopt such features as <a href="https://core.trac.wordpress.org/ticket/18146">user-based timezones</a> in the future, which would better handle time differences on WordPress installs with users all over the world.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 26 Sep 2019 16:33:40 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:28:"Gary: Talking with WP&amp;UP";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:25:"https://pento.net/?p=5120";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:47:"https://pento.net/2019/09/26/talking-with-wpup/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:348:"<p>At WordCamp Europe this year, I had the opportunity to chat with the folks at WP&amp;UP, who are doing wonderful work providing mental health support in the WordPress community.</p>



<p><a href="https://wpandup.org/podcast/getting-to-the-core-of-wordpress-021/">Listen to the podcast</a>, and check out the services that WP&amp;UP provide!</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 26 Sep 2019 04:35:45 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Gary";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:10;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"BuddyPress: BuddyPress 5.0.0 Release Candidate 2";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://buddypress.org/?p=308016";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:68:"https://buddypress.org/2019/09/buddypress-5-0-0-release-candidate-2/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2087:"<p>Hi!</p>



<p><a href="https://downloads.wordpress.org/plugin/buddypress.5.0.0-RC2.zip">The second release candidate for BuddyPress 5.0.0</a> is now available for an ultimate round of testing!</p>



<p>Since the <a href="https://buddypress.org/2019/09/buddypress-5-0-0-release-candidate/">first release candidate</a>, we&#8217;ve improved the way BP REST API Controllers are loaded inside BuddyPress component classes.</p>



<p>This is an important milestone as we progress toward the BuddyPress 5.0.0 final release date. &#8220;Release Candidate&#8221; means that we think the new version is ready for release, but with more than 200,000 active installs, hundreds of BuddyPress plugins and Thousands of WordPress themes, it’s possible something was missed. BuddPress 5.0.0 is&nbsp;scheduled to be released&nbsp;on&nbsp;<strong>Monday, September 30</strong>, but we need&nbsp;<em>your</em>&nbsp;help to get there—if you haven’t tried 5.0.0 yet, <strong>now is the time!</strong> </p>



<div class="wp-block-button aligncenter is-style-squared"><a class="wp-block-button__link has-background" href="https://downloads.wordpress.org/plugin/buddypress.5.0.0-RC2.zip">Download and test the 5.0.0-RC2</a></div>



<div class="wp-block-spacer"></div>



<p><em>PS: as usual you alternatively get a copy via our Subversion repository.</em></p>



<p>A detailed changelog will be part of our official release note, but&nbsp;you can get a quick overview by reading the post about the&nbsp;<a href="https://buddypress.org/2019/08/buddypress-5-0-0-beta1/">5.0.0 Beta1</a>&nbsp;release.</p>



<div class="wp-block-image"><img src="https://plugins.svn.wordpress.org/buddypress/assets/icon.svg" alt="" width="33" height="33" /></div>



<p><strong>If you think you&#8217;ve found a bug</strong>, please let us know reporting it on&nbsp;<a href="https://buddypress.org/support">the support forums</a>&nbsp;and/or&nbsp;on&nbsp;<a href="https://buddypress.trac.wordpress.org/">our development tracker</a>.</p>



<p>Thanks in advance for giving this second release candidate a test drive!</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 26 Sep 2019 02:31:06 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:5:"imath";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:11;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:53:"WPTavern: Hacktoberfest 2019 Registration is Now Open";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94243";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:64:"https://wptavern.com/hacktoberfest-2019-registration-is-now-open";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3413:"<p><a href="https://hacktoberfest.digitalocean.com/" rel="noopener noreferrer" target="_blank">Hacktoberfest</a> has started back up again for its sixth year running, sponsored by <a href="http://digitalocean.com" rel="noopener noreferrer" target="_blank">DigitalOcean</a> and <a href="https://dev.to/" rel="noopener noreferrer" target="_blank">DEV</a>. The annual event brings together open source communities from all over the world for virtual and <a href="https://hacktoberfest.digitalocean.com/events" rel="noopener noreferrer" target="_blank">local collaboration</a>. Organizers are expecting approximately 150,000 participants this year.</p>
<p>The first 50,000 participants who make four pull requests to any GitHub-hosted repositories between October 1-31, will receive a commemorative Hacktoberfest T-shirt. Organizers have introduced a one-week review period for PRs this year in order to give maintainers the opportunity to flag any spammy PRs as invalid. The goal is to encourage participants to submit more thoughtful contributions.</p>
<p>More than 21,000 issues on GitHub have already been <a href="https://github.com/search?q=label%3Ahacktoberfest+state%3Aopen&type=Issues" rel="noopener noreferrer" target="_blank">labeled for Hacktoberfest</a>. Maintainers who want to have their projects included should identify issues best suited to new contributors and apply the &#8220;Hacktoberfest&#8221; label. Organizers also recommend creating a CONTRIBUTING.md file with contribution guidelines and adopting a code of conduct for the project.</p>
<p>Adding WordPress to a search for Hacktoberfest issues displays <a href="https://github.com/search?utf8=%E2%9C%93&q=label%3Ahacktoberfest+state%3Aopen+wordpress&type=Issues&ref=advsearch&l=&l=" rel="noopener noreferrer" target="_blank">120 issues</a> that are related in some way to themes, plugins, apps, and other products with WordPress-specific needs. The event is a good opportunity for maintainers to get more exposure for their projects and help new contributors gain confidence through a structured contribution process.</p>
<p>This year Hacktoberfest&#8217;s organizers are also featuring <a href="https://github.com/topics/climate-change" rel="noopener noreferrer" target="_blank">projects focused on combating climate change</a>. These include repos for open source technologies, such as an <a href="https://github.com/CodeForAfrica/ClimateChangeProjections" rel="noopener noreferrer" target="_blank">embeddable map that shows climate change projections</a>, an <a href="https://github.com/juancoob/Vegginner" rel="noopener noreferrer" target="_blank">app targeting consumption habits</a>, and <a href="https://github.com/sphericalpm/ghgdata" rel="noopener noreferrer" target="_blank">greenhouse gas emissions data packaged for exploration and charting</a>, to name a few.</p>
<p>Hacktoberfest is open to contributors at any level of experience. For those just getting started, DigitalOcean has created an <a href="https://www.digitalocean.com/community/tutorial_series/an-introduction-to-open-source" rel="noopener noreferrer" target="_blank">Introduction to Open Source</a> series that covers the basics of git and how to create a pull request. DEV also has a <a href="https://dev.to/tvanblargan/crash-course-git-lingo-1enj" rel="noopener noreferrer" target="_blank">Git crash course</a> available to get new contributors up to speed.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 25 Sep 2019 22:39:40 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:12;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:88:"WPTavern: Human Made Releases Publication Checklist Plugin Designed for the Block Editor";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94238";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:99:"https://wptavern.com/human-made-releases-publication-checklist-plugin-designed-for-the-block-editor";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2502:"<p>Human Made has created a <a href="https://github.com/humanmade/publication-checklist/" rel="noopener noreferrer" target="_blank">Publication Checklist</a> plugin built specifically for the block editor. It was developed as a headline feature of <a href="https://humanmade.com/2019/06/17/a-technical-introduction-to-altis-enterprise-augmented-wordpress-platform/" rel="noopener noreferrer" target="_blank">Altis</a>, the company&#8217;s enterprise publishing platform based on WordPress, but is also available as a standalone plugin that developers can customize for their own particular use cases.</p>
<p>Ryan McCue, Human Made&#8217;s Director of Engineering, shared screenshots of the plugin on <a href="https://twitter.com/rmccue/status/1173550662296190976" rel="noopener noreferrer" target="_blank">Twitter</a> but noted that it may require more manual configuration when used outside of Altis. Developers familiar with React can extend the checklist to provide a more interactive experience for users completing the required publishing tasks.</p>
<p>&#8220;Because this is built for the block editor, you can build the UI for your checks in React, allowing users to fix issues inline, or providing richer interaction; e.g. &#8216;jump to block failing this check,'&#8221; McCue said.</p>
<p><a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-25-at-10.43.20-AM.png?ssl=1"><img /></a></p>
<p>Status of the publishing tasks is also shown in its own column in the posts list table, a useful feature for giving editorial teams a better overall picture of posts in progress. (The plugin also provides a way to disable this view.)</p>
<p><a href="https://i0.wp.com/wptavern.com/wp-content/uploads/2019/09/publication-checklist-posts-list-table.jpeg?ssl=1"><img /></a></p>
<p>It&#8217;s important to note that the Publication Checklist plugin only provides a framework for the pre-publish checks, and does not include a settings interface for users to create their own checks. For this reason, the current version is more geared towards developers who are capable of registering checks using the provided function. The checks display a warning if incomplete but users are still allowed to publish. A more strict enforcement that blocks publishing can also be applied. For more information on customizing the plugin, check out the <a href="https://github.com/humanmade/publication-checklist/" rel="noopener noreferrer" target="_blank">documentation</a> on GitHub.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 25 Sep 2019 17:44:10 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:13;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:69:"WPTavern: Theme Review Team Restructures Into Project Representatives";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94224";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:80:"https://wptavern.com/theme-review-team-restructures-into-project-representatives";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5229:"<p>The WordPress Theme Review Team (TRT) restructured its administrative duties and laid out its <a href="https://make.wordpress.org/themes/2019/09/24/new-theme-review-team-structure/">new team organization</a> after yesterday&#8217;s semimonthly team meeting. This is not the first time the TRT has restructured to meet the growing demands of the official theme directory over the years.  The team is moving toward a flat structure that spreads its responsibilities to various project representatives.</p>



<p>The original team consisted of a purely merit-based system where members worked their way up the ranks, becoming moderators and eventually admins.  Each level provided more access and responsibility.  In 2017, the team restructured to a <a href="https://make.wordpress.org/themes/2017/04/08/restructuring-the-theme-review-team/">lead-based system</a> in which two team leads rotated every six months.  The time limit was put in place to prevent burnout.  Some leads ran the team beyond the six-month limit during this time, but it was not always easy to find replacements who wanted to take on the full responsibilities of managing everything.  There was also concern among some team members that the rotation schedule wasn&#8217;t strictly followed with some leads overstaying their allotted time.</p>



<p>In meetings and discussions over the last several months, various members drafted proposals on changing the team structure.  The now-former team leads and a group of moderators created the new plan to split the team into specific projects, each with at least one representative.</p>



<p>The following are the new sub-teams and representatives.</p>



<ul><li>Theme review representatives: Sandilya Kafle and Carolina Nymark</li><li>Theme packages representative: Ari Stathopoulos</li><li>Automation representative: Denis Žoljom</li><li>Theme handbook representative: Ana Alfieri</li><li>Communications representative: William Patton</li></ul>



<p>The five projects cover the team&#8217;s current duties and spread out the workload. &#8220;That&#8217;s kind of what this is about,&#8221; said William Patton.  &#8220;It&#8217;s making sure that no one single person handles all the things and that it&#8217;s shared between all.&#8221;</p>



<p>The new structure doesn&#8217;t mean there&#8217;s no room for other projects.  If a team member has a particular itch they want to scratch, they&#8217;re open to spearhead that project.  All the power is no longer consolidated into a couple of people&#8217;s hands.</p>



<p>&#8220;Sharing the load and spreading people&#8217;s specific skills between things they know and are investing time into makes sense at this point,&#8221; said Patton.</p>



<p>The team will no longer rotate leads (or representatives in this case) every six months.  If someone needs to step down from their representative role or take a break, finding a new representative will be handled on a case-by-case basis.  &#8220;We all have our strengths and passions. The thing that we also need to work on is finding people who are willing to participate and eventually take over when we feel tired,&#8221; said Denis Žoljom.</p>



<p>Žoljom has been leading the automation project for while by maintaining the Theme Review coding standards and Theme Sniffer plugin.  He&#8217;s currently looking to move the WPThemeReview ruleset to the official WordPress GitHub.  &#8220;This is necessary because we want to use it in Tide,&#8221; said Žoljom.  Tide is an automated tool for improving code quality in plugins and themes.</p>



<p>&#8220;My personal goal would be to see if we can improve the review process &#8211; either by working on the GitHub review idea I had a few months ago, or by working on the automated tools that help the users,&#8221; said Žoljom.</p>



<p>The theme review representatives will handle the traditional role of overseeing the reviewing responsibilities of the team.  Little will change in that regard since it&#8217;s the primary duty of the TRT.  They will continue moderating themes and handling guideline changes.  &#8220;However, they can consult with other reps to make the final decision and to make new changes,&#8221; said Sandilya Kafle.</p>



<p>The WordPress docs team has now handed over responsibility of the <a href="https://developer.wordpress.org/themes/">theme developer handbook</a> to the TRT.  &#8220;I think we should try to keep coherence between the two handbooks, so we avoid saying one thing in one and another in the other,&#8221; said Ana Alfieri about the differences between the developer and review handbooks.  At times, such difference have been points of contention between TRT members.  Having both handbooks in sync on best practices will help keep reviewers and theme authors on the same page.</p>



<p>Ari Stathopoulos recently took over as the <a href="https://wptavern.com/behind-new-packages-project-lead-theme-review-team-launches-admin-notices-solution">representative for theme packages</a> in the past month.  The packages project aims to build standardized drop-in modules for developers to use in their themes.  This specific project may also have various developers handling specific packages.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 25 Sep 2019 17:18:34 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:14;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:72:"WPTavern: WordPress 5.3 to Introduce New Admin Email Verification Screen";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94193";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:83:"https://wptavern.com/wordpress-5-3-to-introduce-new-admin-email-verification-screen";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2769:"<p>WordPress 5.3 is set to introduce an admin email verification screen that will be shown every six months after an admin has logged in. The feature was proposed seven months ago in a <a href="https://core.trac.wordpress.org/ticket/46349" rel="noopener noreferrer" target="_blank">ticket</a> that contributor <a href="https://www.andreidraganescu.info/" rel="noopener noreferrer" target="_blank">Andrei Draganescu</a> opened as part of the Site Health component improvements.</p>
<p><a href="https://i0.wp.com/wptavern.com/wp-content/uploads/2019/09/admin-email-verification.png?ssl=1"><img /></a></p>
<p>Draganescu said the idea came from discussions in the #core-php channel regarding WSOD (White Screen of Death) recovery emails, which are sent when a site experiences a fatal error and the administrator may be locked out of their WordPress site. Participants in the discussion raised the issue of how common it is for admin email addresses to be outdated or set to a &#8220;catch all&#8221; address that is never checked. The email address may also be set automatically by the host during the process of a one-click installation.</p>
<p>The &#8220;Why is this important?&#8221; link leads to a WordPress support article that describes the <a href="https://wordpress.org/support/article/settings-general-screen/#email-address" rel="noopener noreferrer" target="_blank">various uses for the admin email address</a>, such as new user registration notifications, comment approval, and maintenance messages.</p>
<p>Although it wasn&#8217;t the stated intention for the new admin email verification screen, the feature could become important for improving communication prior to automatic updates. Requiring admins to verify their email addresses after six months could ensure that more addresses are kept current, especially for admins who check their sites infrequently.</p>
<p>When the WordPress security team <a href="https://wptavern.com/proposal-to-auto-update-old-versions-of-wordpress-to-4-7-sparks-heated-debate" rel="noopener noreferrer" target="_blank">proposed auto-updating older versions of WordPress to 4.7</a>, one of the chief concerns is whether WordPress will be able to reach admins whose emails have been abandoned. This new admin email verification screen will not be be useful for older WordPress sites, but in the future, when auto-updating for major versions becomes the standard, it will help ensure more administrators are getting those notices to a working address.</p>
<p>A new <code>admin_email_check_interval</code> filter is available for developers to customize the interval for redirecting the user to the admin email confirmation screen. Those who find it to be unnecessary or annoying can set a very large interval for the check.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 24 Sep 2019 18:30:33 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:15;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:63:"WPTavern: Twenty Twenty Bundled in Core, Beta Features Overview";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94038";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:73:"https://wptavern.com/twenty-twenty-bundled-in-core-beta-features-overview";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5884:"<div class="wp-block-image"><a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/twenty-editor.jpg?ssl=1" target="_blank" rel="noreferrer noopener"><img /></a></div>



<p>Twenty Twenty, the upcoming default WordPress theme, was committed to core and shipped with <a href="https://wptavern.com/wordpress-5-3-beta-1-ready-for-testing-includes-12-gutenberg-releases-and-new-twenty-nineteen-default-theme">WordPress 5.3 Beta 1</a> yesterday.</p>



<p>Like most core themes, Twenty Twenty is simple in function.  It comes packaged with a handful of custom features and options, but it remains true to the mission of being an easy-to-use default theme.</p>



<p>The theme has personality.  Its headings are bold and opinionated.  Its pull quotes grab your attention.  It&#8217;s unafraid of making a splash with its design.   This is a blog theme that&#8217;s meant to showcase what the block editor is capable of doing.   It is a refreshing change of pace from the current slew of themes landing in the directory.</p>



<p>Twenty Twenty is not ideal for every use case.  Some users will no doubt dislike the design choices.  Others will love everything about it.</p>



<p><em>Note: Twenty Twenty is still in beta, so its features could change between now and the official release of WordPress 5.3.</em></p>



<h2>Customizer Options</h2>



<div class="wp-block-image"><img />Hue-only picker for the accent color in the customizer.</div>



<p>The theme has a few custom options available within the customizer:</p>



<ul><li>A retina logo option, which scales the logo image to half its size to make it sharper on hi-res screens.</li><li>An option for showing or hiding a search icon in the header.</li><li>A choice between showing the full post text or summary (excerpt) on archive pages.</li><li>A header and footer background color.</li><li>An accent color used for links and other elements.</li><li>Support for the core custom background feature.</li></ul>



<p>The accent color option is an interesting choice.  Rather than providing the full breadth of all colors, the theme includes a hue-only color picker.  This feature allows users to select from a more limited set of colors within an accessible color range.</p>



<p>There is also a ticket for removing core <a href="https://github.com/WordPress/twentytwenty/issues/480">custom background image support</a> to help users avoid accessibility issues.</p>



<h2>Custom Page Templates</h2>



<a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/twenty-cover.jpg?ssl=1" target="_blank" rel="noreferrer noopener"><img /></a>Cover template options in the customizer.



<p>Twenty Twenty has a fresh take on creating a cover page not seen in previous default themes.  The &#8220;Cover Template&#8221; works for both posts and pages.  When selecting it, the template displays the post featured image similar to the cover block in core.  The featured image spans the full width of the screen and extends behind the header and navigation area.  The post title and byline/meta are set over the image.</p>



<p>The theme provides a few options for customizing the output of the cover area and allows the user to:</p>



<ul><li>Set a fixed background for a parallax effect.</li><li>Add an overlay background color that sits over the featured image.</li><li>Change the color of the text on top of the image.</li><li>Choose a blend mode for the overlay background color.</li><li>Alter the overlay background color opacity.</li></ul>



<p>Having a core theme explore this feature is a nice. Ideally, users would be able to create a featured cover area on a per-post basis and adjust the colors for the specific image on that post.  However, core has yet to bundle such a feature with the block editor.  There is an open Gutenberg ticket for <a href="https://github.com/WordPress/gutenberg/issues/16281">expanding the editor outside of the post content</a> that may help theme authors address this common feature, but we&#8217;re likely several releases from seeing that become a reality.</p>



<p>The theme also includes a wide (full width) template, which is a fairly common feature.  At the moment, this template doesn&#8217;t seem to do anything in particular when assigned to a page.  There&#8217;s an <a href="https://github.com/WordPress/twentytwenty/issues/185">open GitHub ticket</a> that addresses what it should do at some point.  Considering that the theme has no left/right sidebar, it&#8217;d be interesting to see how this template functions.</p>



<h2>Page Loading Speed</h2>



<p>Page load is something to keep an eye on.  Twenty Twenty currently ships a 100 kb stylesheet on top of the block editor&#8217;s 40 kb CSS file (from the Gutenberg plugin).  This number doesn&#8217;t include the font and JavaScript files also loaded for the page.  This is a far cry from the behemoth 223 kb stylesheet included in Twenty Nineteen, but it&#8217;s still concerning because more development time means that more code will likely get added as tweaks are made and bugs are fixed.  </p>



<p>To be fair, the block editor has many elements to style with no unified design framework for theme authors to take advantage of.  Keeping a Gutenberg-ready stylesheet under 100 kb that also styles each block is a feat of engineering few can master.</p>



<h2>Follow Twenty Twenty Development</h2>



<p>Theme development is currently happening on the <a href="https://github.com/WordPress/twentytwenty/">Twenty Twenty GitHub repository</a>.  If you want to track its changes as the theme is imported into core, the changes are happening on the <a href="https://core.trac.wordpress.org/ticket/48110">Import Twenty Twenty</a> Trac ticket.</p>



<p>The theme still has work to be done before it&#8217;s ready for public release.  Now would be a great time to start testing it and reporting issues.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 24 Sep 2019 17:29:22 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:16;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:82:"BuddyPress: An online community learning hub to deepen studies during IRL meetings";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://buddypress.org/?p=307967";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:102:"https://buddypress.org/2019/09/an-online-community-learning-hub-to-deepen-studies-during-irl-meetings/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:6001:"<div><em>This is a guest post by Tanner Moushey, Founder and Lead Engineer of <a href="https://study.church/about/">StudyChurch</a>. He is a BP REST API early adopter and we thought his achievments implementing Headless BuddyPress was a great source of inspirations for the BuddyPress community. Many thanks to him for taking the time to share with us about this case study.</em></div>



<p>Peer reviewed by <a class="bp-suggestions-mention" href="https://buddypress.org/members/imath/" rel="nofollow">@imath</a></p>



<div class="wp-block-image"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/StudyChurch-Organization-Dashboard.png" alt="" /></div>



<p><a href="https://study.church/">StudyChurch</a> is an ambitious startup seeking to make a mark in the church product marketplace. With a unique approach to online interaction, StudyChurch combines elements of engagement and learning in a way that is both simple and intuitive for the end user.</p>



<div class="wp-block-spacer"></div>



<h2>Background</h2>



<p>I began working on StudyChurch as a side project in 2015. It started as a proof of concept and an excuse to dive deeply into BuddyPress. I wanted to leverage the group and activity components that BuddyPress provides and combine that with a custom study module that I created with a custom post type, BackboneJS, and the WordPress REST API. Answers to study questions were stored in WordPress Comments and synced to a custom BuddyPress activity type which was then used to create the discussion interface. Each question had an activity component under it to show off the other group answers and corresponding discussions.</p>



<p>I finished the first draft of the project after several months and before too long I had groups signing up to use the system. I continued to make minor modifications over the next few years but kept running into complaints about speed and the user interface.</p>



<p>When I was approached in 2018 by a publisher that wanted to use StudyChurch on a larger scale it sounded like a great opportunity to rebuild.</p>



<div class="wp-block-spacer"></div>



<h2>Implementing Headless BuddyPress </h2>



<p>One of the big changes that I wanted to make in the rebuild was to switch to a JavaScript front end. I wanted something that was going to allow us to make numerous asynchronous data requests without using Ajax, which can be slow and difficult to maintain over a large project. I decided on VueJS and started building out the API to handle the data that was previously controlled by the BuddyPress templates. </p>



<div class="wp-block-spacer"></div>



<h3>Building a custom API with the BuddyPress REST API </h3>



<p>I’d done quite a bit of work extending the WordPress REST API on previous projects and was excited to discover the <strong>BuddyPress REST API</strong> that extended it. This took care of a lot of the structure and allowed me to focus my time on building out our custom modules and functionality. Anytime I ran into something that needed to be more flexible, I’d submit a patch to the BuddyPress REST API repository and would get a prompt resolution.</p>



<p>Now that we are able to post and retrieve data through the API, the user interactive elements on the site are noticeably faster and the overall load on the server is much less. Not only that, but we are ready for a native app once we get to that point.</p>



<div class="wp-block-image"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/studychurch-case-study-image-1024x482.png" alt="" class="wp-image-307971" /></div>



<div class="wp-block-spacer"></div>



<h3>Creating&nbsp;a VueJS front end </h3>



<p>Building a completely JavaScript front end for BuddyPress was fun challenge. I underestimated how many different components I’d need to build out since I wasn’t able to rely on the BuddyPress default templates, but the end result was well worth the effort.With VueJS we were able to leverage a lot of prebuilt UI packages (like&nbsp;<a href="https://element.eleme.io/#/en-US">Element</a>) to do a lot of the heavy lifting for us. Since we were no longer tied to the BuddyPress template engine, we were able to get creative with how we displayed information and handled user interactions. The end result was a clean, fast, and user friendly interface that was simple and straightforward to use.</p>



<p>I made a few modifications to allow WordPress and BuddyPress recognize our front end app and use it for BuddyPress components. I solved this with a pretty simple hook into the template include filter and included our template instead of the default. A few custom rewrite rules handled any non-BuddyPress url structures I needed to support and I soon had a fully functional and detached front end.</p>



<div class="wp-block-spacer"></div>



<h2>Conclusion</h2>



<p>StudyChurch is now a powerful, robust social network ready for scale. We are still working on improving the system and adding new features which are now easier and faster to implement with the new structure.</p>



<p>We’ve received some great feedback from users who find the app fast and intuitive. We are hoping to build out a native app in the near future.</p>



<p>I’m so thankful for the work done by all of the volunteers who’ve put so much time into WordPress, BuddyPress, and now the BuddyPress REST API. I think there are going to be many more projects like StudyChurch in the near future that will leverage these great tools to build amazing and helpful solutions.</p>



<p>Feel free to reach out if you have any questions or comments on what we’ve done with StudyChurch. Also, you are welcome to browse our code base on <a href="https://github.com/studychurch/sc-dashboard" target="_blank" rel="noreferrer noopener">GitHub</a>.</p>



<p>You can read more about StudyChurch and other projects we work on at <a href="https://iwitnessdesign.com/" target="_blank" rel="noreferrer noopener">iwitnessdesign.com</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 24 Sep 2019 09:07:27 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:5:"imath";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:17;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:116:"WPTavern: WordPress 5.3 Beta 1 Ready for Testing, Includes 12 Gutenberg Releases and New Twenty Twenty Default Theme";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94165";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:128:"https://wptavern.com/wordpress-5-3-beta-1-ready-for-testing-includes-12-gutenberg-releases-and-new-twenty-nineteen-default-theme";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2577:"<p>WordPress core contributors worked together today to package up <a href="https://wordpress.org/news/2019/09/wordpress-5-3-beta-1/" rel="noopener noreferrer" target="_blank">5.3 Beta 1</a> for release on schedule. The Core slack channel was abuzz this afternoon as developers pushed through last-minute commits and fixed issues ahead of shipping the beta.</p>
<p>Iterations on the block editor are a major part of of this release. WordPress 5.3 will include the last 12 Gutenberg plugin releases. If you have already been using the plugin, you may have forgotten how many features it has that still haven&#8217;t made it into core WordPress. This includes significant improvements to group, column, and gallery blocks, Accessibility Navigation mode, the new inserter help panel, &#8220;snackbar&#8221; notices, and the typewriter experience, to highlight a few big items that have been rolled into 5.3.</p>
<p>The highly anticipated Twenty Twenty default theme is also available in the beta, which which we will explore in greater detail on WP Tavern this week. Its design is <a href="https://wptavern.com/first-look-at-twenty-twenty-new-wordpress-default-theme-based-on-chaplain" rel="noopener noreferrer" target="_blank">based on the Chaplin theme from Anders Norén</a> and showcases what is possible with the block editor.</p>
<p>Some of the UI changes introduced in Gutenberg are starting to make their way into other parts of the WordPress admin.</p>
<p>&#8220;These improved styles fix many accessibility issues, improve color contrasts on form fields and buttons, add consistency between editor and admin interfaces, modernize the WordPress color scheme, add better zoom management, and more,&#8221; release coordinator Francesca Marano said in the 5.3 beta 1 announcement.</p>
<p>A few other notable additions to 5.3 that need testing include the following:</p>
<ul>
<li>Support for resuming uploads on large file sizes</li>
<li>Automatic image rotation during upload</li>
<li>Improvements to Site Health checks</li>
<li>Time/Date component fixes</li>
<li>PHP 7.4 Compatibility and removal of deprecated functionality</li>
</ul>
<p>If you&#8217;re ready to take the beta for a test drive, the easiest way is to install the <a href="https://wordpress.org/plugins/wordpress-beta-tester/" rel="noopener noreferrer" target="_blank">WordPress Beta Tester</a> plugin and select the “bleeding edge nightlies” option. The <a href="https://make.wordpress.org/core/5-3/" rel="noopener noreferrer" target="_blank">official release</a> is targeted for November 12, 2019.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 24 Sep 2019 02:56:22 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:18;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:102:"WPTavern: Google Search Console Adds Breadcrumbs Report, Sends Out Warnings for Structured Data Errors";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94132";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:112:"https://wptavern.com/google-search-console-adds-breadcrumbs-report-sends-out-warnings-for-structured-data-errors";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5150:"<p>Google recently announced a new Breadcrumbs report available in the Search Console to inform site owners about markup issues. In 2015, Google <a href="https://webmasters.googleblog.com/2015/04/better-presentation-of-urls-in-search.html" rel="noopener noreferrer" target="_blank">introduced support for schema.org structured data</a>, including the breadcrumbs URL structure, in order better present URLs in search results. The Search Console&#8217;s new report uses this data to help site owners fix any issues preventing their breadcrumbs from displaying as rich search results.</p>
<blockquote class="twitter-tweet">
<p lang="en" dir="ltr">Great news, we have a new report on Search Console <img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f4e2.png" alt="📢" class="wp-smiley" /> As of today, if you have Breadcrumb <img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f35e.png" alt="🍞" class="wp-smiley" /> structured data in your site, you'll see a new report under Enhancements (see screenshot). Check if you have errors and get to work! <img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f6e0.png" alt="🛠" class="wp-smiley" /> <a href="https://t.co/b8I4vbJwb9">pic.twitter.com/b8I4vbJwb9</a></p>
<p>&mdash; Google Webmasters (@googlewmc) <a href="https://twitter.com/googlewmc/status/1174693878835875840?ref_src=twsrc%5Etfw">September 19, 2019</a></p></blockquote>
<p></p>
<p>Over the weekend, the console started emailing out notices to those who have errors in the breadcrumb structured data on their sites. It includes how many URLs are affected, along with a link to the new report.</p>
<p><a href="https://i0.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-23-at-9.17.17-AM.png?ssl=1"><img /></a></p>
<p>One common error that users are seeing is a &#8220;Missing field &#8216;item,'&#8221; which references one of the properties Google requires for displaying that content as a rich result. The &#8216;item&#8217; property is the URL to the webpage that represents the breadcrumb, as Google prefers the final crumb to be linked.</p>
<p>WordPress site owners have started reporting breadcrumb issues in the support forums for various plugins and themes. <a href="https://wordpress.org/plugins/breadcrumb-navxt/">Breadcrumb NavXT</a>, a plugin that is active on 800,000 sites, allows users to generate customizable breadcrumb trails. There are already half a dozen <a href="https://wordpress.org/support/plugin/breadcrumb-navxt/" rel="noopener noreferrer" target="_blank">support threads</a> opened regarding Breadcrumb markup errors listed in the console. Recommendations for fixing this issue vary based on the specific property that is missing and the breadcrumb configuration the user has in place.</p>
<p>Breadcrumb NavXT plugin author John Havlik has <a href="https://wordpress.org/support/topic/missing-field-id-in-breadcrumbs-on-google-search-console/page/2/#post-11958307" rel="noopener noreferrer" target="_blank">advised</a> some users to remove the schema.org markup for unlinked breadcrumb templates in order to remove the error, although this may not offer the best presentation in search snippets. Others have suggested allowing the %link% tag in the unlinked breadcrumb template and Havlik <a href="https://github.com/mtekk/Breadcrumb-NavXT/issues/226" rel="noopener noreferrer" target="_blank">added this to the 6.4.0 milestone for the plugin</a> over the weekend.</p>
<p>The <a href="https://wordpress.org/plugins/wordpress-seo/" rel="noopener noreferrer" target="_blank">Yoast SEO</a> plugin also has an option for adding breadcrumbs and multiple users are <a href="https://wordpress.org/support/topic/breadcrumbs-missing-field-id-error-in-google-search-sonsole/" rel="noopener noreferrer" target="_blank">reporting</a> <a href="https://wordpress.org/support/topic/google-breadcrumbs-markup-issues/page/2/" rel="noopener noreferrer" target="_blank">errors</a> in the Google Search Console. Solutions vary, depending on what types of pages are outputting the error, but the most common advice Yoast support team members are offering is to check to see if there is a theme or plugin that is adding conflicting breadcrumb markup.</p>
<p>There is no easy prescribed fix that will apply to all situations. It depends on how a site owner has configured breadcrumbs through a plugin or if they are automatically generated by a theme.</p>
<p>If you received a notice from Google Search Console, the first step is to determine whether it&#8217;s a theme or a plugin that is generating your breadcrumbs. Next, browse the support forums for the theme/plugin that provides the breadcrumbs and see if the author recommends a fix or is working on one.</p>
<p>Although breadcrumbs do not currently have a direct affect on rankings, they are prominently displayed in search snippets and generally contribute to a positive user experience. For more information on solving specific errors, check out <a href="https://developers.google.com/search/docs/data-types/breadcrumb" rel="noopener noreferrer" target="_blank">Google&#8217;s documentation on Breadcrumb structured data</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Sep 2019 18:57:32 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:19;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"WordPress.org blog: WordPress 5.3 Beta 1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7114";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:56:"https://wordpress.org/news/2019/09/wordpress-5-3-beta-1/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:9089:"<p>WordPress 5.3 Beta 1 is now available!</p>



<p><strong>This software is still in development,</strong> so we don’t recommend running it on a production site. Consider setting up a test site to play with the new version.</p>



<p>You can test the WordPress 5.3 beta in two ways:</p>



<ul><li>Try the <a href="https://wordpress.org/plugins/wordpress-beta-tester/">WordPress Beta Tester</a> plugin (choose the “bleeding edge nightlies” option)</li><li>Or <a href="https://wordpress.org/wordpress-5.3-beta1.zip">download the beta here</a> (zip).</li></ul>



<p>WordPress 5.3 is slated for release on <a href="https://make.wordpress.org/core/5-3/">November 12, 2019</a>, and we need your help to get there. Here are some of the big items to test, so we can find and resolve as many bugs as possible in the coming weeks.</p>



<h2>Block Editor: features and improvements</h2>



<p>Twelve releases of the Gutenberg plugin are going to be merged into 5.3 which means there’s a long list of exciting new features.&nbsp;</p>



<p>Here are just a few of them:</p>



<ul><li>Group block and grouping interactions</li><li>Columns block improvements (width support + patterns)</li><li>Table block improvements (text alignment support, header/footer support, colors)</li><li>Gallery block improvements (reordering inline, caption support)</li><li>Separator block improvements (color support)</li><li>Latest Posts block improvements (support excerpt, content)</li><li>List block improvements (indent/outdent shortcuts, start value and reverse order support)</li><li>Button block improvements (support target, border radius)</li><li>Animations and micro interactions (moving blocks, dropdowns, and a number of small animations to improve the UX)</li><li>Accessibility Navigation Mode which will allow you to navigate with the keyboard between blocks without going into their content.</li><li>Block Style Variations API</li></ul>



<p>Plus a number of other improvements, amongst them:</p>



<ul><li>Data Module API improvements (useSelect/useEffect)</li><li>Inserter Help Panel</li><li>Extensibility: DocumentSettingsPanel</li><li>Snackbar notices</li><li>Typewriter Experience</li><li>Fix a number of Accessibility report issues</li></ul>



<p>If you want to see all the features for each release, here are direct links to the release posts: <a href="https://make.wordpress.org/core/2019/09/19/whats-new-in-gutenberg-18-september/">6.5</a>, <a href="https://make.wordpress.org/core/2019/08/28/whats-new-in-gutenberg-28-august/">6.4</a>, <a href="https://make.wordpress.org/core/2019/08/14/whats-new-in-gutenberg-14-august/">6.3</a>, <a href="https://make.wordpress.org/core/2019/07/31/whats-new-in-gutenberg-31-july/">6.2</a>, <a href="https://make.wordpress.org/core/2019/07/10/whats-new-in-gutenberg-10-july/">6.1</a>, <a href="https://make.wordpress.org/core/2019/06/26/whats-new-in-gutenberg-26th-june/">6.0</a>, <a href="https://make.wordpress.org/core/2019/06/12/whats-new-in-gutenberg-12th-june/">5.9</a>, <a href="https://make.wordpress.org/core/2019/05/29/whats-new-in-gutenberg-29th-may/">5.8</a>, <a href="https://make.wordpress.org/core/2019/05/15/whats-new-in-gutenberg-15th-may/">5.7</a>, <a href="https://make.wordpress.org/core/2019/05/01/whats-new-in-gutenberg-1st-may/">5.6</a>, <a href="https://make.wordpress.org/core/2019/04/17/whats-new-in-gutenberg-17th-april/">5.5</a>, and <a href="https://make.wordpress.org/core/2019/04/03/whats-new-in-gutenberg-3rd-april/">5.4</a>.</p>



<h3>Continuous effort on performance</h3>



<p>The team working on the block editor managed to shave off 1.5 seconds of loading time for a particularly sizeable post (~ 36,000 words, ~ 1,000 blocks) since WordPress 5.2.</p>



<h2>A new default theme: welcome Twenty Twenty</h2>



<p>WordPress 5.3 introduces <a href="https://make.wordpress.org/core/2019/09/06/introducing-twenty-twenty/">Twenty Twenty</a>, the latest default theme in our project history.&nbsp;</p>



<p>This elegant new theme is based on the WordPress theme <a href="https://www.andersnoren.se/teman/chaplin-wordpress-theme/">Chaplin</a> which was released on the WordPress.org theme directory earlier this summer.&nbsp;</p>



<p>It includes full support for the block editor, empowering users to find the right design for their message.</p>



<h2>Wait! There is more</h2>



<p>5.3 is going to be a rich release with the inclusion of numerous enhancements to interactions and the interface.</p>



<h2>Admin interface enhancements</h2>



<p>Design and Accessibility teams worked together to port some parts of Gutenberg styles into the whole wp-admin interface. Both teams are going to iterate on these changes during the 5.3 beta cycle. These improved styles fix many accessibility issues, improve color contrasts on form fields and buttons, add consistency between editor and admin interfaces, modernize the WordPress color scheme, add better zoom management, and more.</p>



<h3>Big Images are coming to WordPress</h3>



<p>Uploading non-optimized, high-resolution pictures from your smartphone isn’t a problem anymore. WordPress now supports resuming uploads when they fail as well as larger default image sizes. That way pictures you add from the block editor look their best no matter how people get to your site.</p>



<h3>Automatic image rotation during upload</h3>



<p>Your images will be correctly rotated upon upload according to the EXIF orientation. This feature was first proposed nine years ago. Never give up on your dreams to see your fixes land in WordPress!</p>



<h3>Site Health Checks</h3>



<p>The improvements introduced in 5.3 make it easier to identify and understand areas that may need troubleshooting on your site from the Tools -&gt; Health Check screen.</p>



<h3>Admin Email Verification</h3>



<p>You’ll now be periodically asked to check that your admin email address is up to date when you log in as an administrator. This reduces the chance that you’ll get locked out of your site if you change your email address.</p>



<h2>For Developers</h2>



<h3>Time/Date component fixes</h3>



<p>Developers can now work with <a href="https://make.wordpress.org/core/2019/09/23/date-time-improvements-wp-5-3/">dates and timezones</a> in a more reliable way. Date and time functionality has received a number of new API functions for unified timezone retrieval and PHP interoperability, as well as many bug fixes.</p>



<h3>PHP 7.4 Compatibility</h3>



<p>The WordPress core team is actively preparing to support PHP 7.4 when it is released later this year. WordPress 5.3 contains <a href="https://core.trac.wordpress.org/query?status=accepted&status=assigned&status=closed&status=new&status=reopened&status=reviewing&keywords=~php74&milestone=5.3&order=priority">multiple changes</a> to remove deprecated functionality and ensure compatibility. Please test this beta release with PHP 7.4 to ensure all functionality continues to work as expected and does not raise any new warnings. </p>



<h3>Other Changes for Developers</h3>



<ul><li>Multisite<ul><li>Filter sites by status<ul><li><a href="https://core.trac.wordpress.org/ticket/37392">https://core.trac.wordpress.org/ticket/37392</a>&nbsp;</li><li><a href="https://core.trac.wordpress.org/ticket/37684">https://core.trac.wordpress.org/ticket/37684</a>&nbsp;</li></ul></li><li>Save database version in site meta<ul><li><a href="https://core.trac.wordpress.org/ticket/41685">https://core.trac.wordpress.org/ticket/41685</a>&nbsp;</li></ul></li></ul></li><li>Code modernization and PHP 7.4 support<ul><li><a href="https://core.trac.wordpress.org/ticket/47678">https://core.trac.wordpress.org/ticket/47678</a>&nbsp;</li><li><a href="https://core.trac.wordpress.org/ticket/47783">https://core.trac.wordpress.org/ticket/47783</a></li></ul></li><li>Toggle password view<ul><li><a href="https://core.trac.wordpress.org/ticket/42888">https://core.trac.wordpress.org/ticket/42888</a></li></ul></li></ul>



<p>Keep your eyes on the <a href="https://make.wordpress.org/core/">Make WordPress Core blog</a> for more <a href="https://make.wordpress.org/core/tag/5-3+dev-notes/">5.3 related developer notes</a> in the coming weeks detailing other changes that you should be aware of.</p>



<h2>What’s next</h2>



<p>There have been over 400 tickets fixed in WordPress 5.3 so far with numerous bug fixes and improvements to help smooth your WordPress experience.</p>



<h2>How to Help</h2>



<p>Do you speak a language other than English? <a href="https://translate.wordpress.org/projects/wp/dev/">Help us translate WordPress into more than 100 languages</a>!</p>



<p>If you think you’ve found a bug, you can post to the <a href="https://wordpress.org/support/forum/alphabeta/">Alpha/Beta area</a> in the support forums. We’d love to hear from you! If you’re comfortable writing a reproducible bug report, <a href="https://core.trac.wordpress.org/newticket">file one on WordPress Trac</a> where you can also find a list of <a href="https://core.trac.wordpress.org/tickets/major">known bugs</a>.<br /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Sep 2019 18:36:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Francesca Marano";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:20;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"WPTavern: WPHelpful: A User Feedback Plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94129";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://wptavern.com/wphelpful-a-user-feedback-plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:7082:"<div class="wp-block-image"><img /></div>



<p><a href="https://wordpress.org/plugins/wphelpful/">WPHelpful</a> is a plugin created by Zack Gilbert and Paul Jarvis that allows users to rate the helpfulness of a post.  It can be a useful addition to sites that offer tutorials, lessons, documentation, or any content where user feedback is warranted. Version 1.0 is available for free in the official WordPress plugin directory, but it also has a <a href="https://wphelpful.com/">pro version</a> that offers additional features.</p>



<p>I enjoyed giving this plugin a test drive.  As a former business owner, I could see where this plugin would&#8217;ve helped me gather feedback from my customers on product documentation and better catered to their needs.</p>



<p>WPHelpful has huge potential, but its version 1.0 is still a 1.0.  It&#8217;s far from being a polished product at this stage.  It needs time to mature as a good free plugin.  The current batch of pro features should have made the cut for the free version.</p>



<p>The free plugin available in the plugin directory won&#8217;t get you far unless you just need a basic rating system.  It is limited to:</p>



<ul><li>Showing the feedback form on posts and pages.</li><li>Changing the colors for the form button.</li><li>Adding custom CSS (a feature already available on all WP sites via the customizer).</li></ul>



<p>All other features and settings are available in the pro version.  Unless your goal is to simply allow user ratings on posts or pages, you can&#8217;t do much with a free copy.  There are existing plugins with a more mature codebase for handling basic ratings.</p>



<p>One of the most notable aspects of the free version is that it allows you to test the pro settings in a development environment. This provides an opportunity to decide if you want to shell out the money to go pro. I am now officially recommending that every other plugin developer do this when possible.</p>



<h2>What the Plugin Gets Right</h2>



<div class="wp-block-image"><img /></div>



<p>The plugin is simple to use.  You can choose to automatically append the form to posts on the front end or opt to display the form with the <code>[wphelpful]</code> shortcode.</p>



<p>If nothing else, users shouldn&#8217;t have any problems getting the plugin up and running.  I tested it against a variety of themes with solid results.</p>



<p>A custom [Gutenberg] block would&#8217;ve kicked user-friendliness up a notch.  Plugin authors need to start thinking in terms of building a block first and a shortcode second.  I&#8217;m hoping this makes the feature list for version 2.0.</p>



<h2>Post Types: Paywall for the Most Useful Feature</h2>



<p>The most important feature for this plugin is the ability to select which post types the feedback form can be used on.  Unfortunately, this feature is behind a paywall, limiting user feedback to only posts and pages.  This is a foundational feature that would be nicer in the free version.</p>



<p>The post type feature is also limited in the pro setting.  In 1.0, you cannot pick post types individually.  The drop-down field limits you to a single post type, all post types, or pages plus all custom types.  There&#8217;s no way to select two different custom post types.</p>



<p>The plugin doesn&#8217;t use the proper post type label, so you may get some weird labels like &#8220;Wp Area Types&#8221; (from the Gutenberg plugin) or &#8220;Jt Documentation Types&#8221; (a custom post type on my test install).</p>



<p>Non-public post types also show up in the list. So, post types that don&#8217;t have front-end output show up in the select form.</p>



<p>These issues are easy fixes, and I&#8217;m hoping this review sheds light onto these problems so they might be corrected for users.</p>



<h2>How the Plugin Could Offer Better Pro Features</h2>



<div class="wp-block-image"><img />Screenshot of the current post feedback report.</div>



<p>Plugin authors need to eat.  There&#8217;s always a delicate balance that developers must strike between offering a useful free plugin and making enough of a return on their investment to continue maintaining the code.</p>



<p>Currently, most of the plugin&#8217;s pro features are basic items like custom colors and form labels.  These are things that would better serve users in the free version.</p>



<p>A more useful pro feature would be a &#8220;Reports&#8221; screen in the admin that offered options such as:</p>



<ul><li>Sorting posts by rating and total ratings.</li><li>Displaying a graph of user feedback by month, year, etc.</li><li>Other reports that provided an overall look at feedback.</li></ul>



<p>The plugin also only allows logged-in users to provide feedback.  That&#8217;s certainly an easier way to go to avoid spammers and bots.  Due to the added complexity, a pro extension for enabling any site visitor to provide feedback would be worth exploring.</p>



<h2>How Does the Code Stack Up?</h2>



<p>I&#8217;m going to get a bit technical here. Feel free to skip ahead if programming is not your thing.</p>



<p>What the plugin needs is time to mature.  Version 1.0 is not supposed to be the best a plugin can be.  It&#8217;s about shipping a minimum viable product, so I&#8217;m a bit forgiving.  If this were 2.0 or 3.0, I&#8217;d be unrelenting.</p>



<p>There&#8217;s a lot to like about the architectural decisions.  Much of it is set up in a way that it should be relatively easy to maintain in the long term.  This is important because it means that correcting issues, such as those listed below, shouldn&#8217;t be tough to fix.</p>



<p>There are code issues that need patches. The plugin currently:</p>



<ul><li>Uses a PHP variable for textdomains (not all translation tools run in a PHP environment).</li><li>Hasn&#8217;t internationalized all of its user-facing text, so not everything can be translated.</li><li>Registers multiple options in the database instead of storing all options together, which creates unnecessary clutter.</li><li>Doesn&#8217;t clean up after itself and delete its options upon uninstall.</li></ul>



<p>These are not insurmountable issues, and they don&#8217;t break anything to the point of making the plugin unusable.  They&#8217;re just issues that need to be addressed.</p>



<h2>The Final Verdict</h2>



<p>Version 1.0 of WPHelpful lacks the feature set to be a particularly great free plugin.  It could be useful in some limited cases.  However, you&#8217;ll probably want to opt for the pro version to get the features that would make this plugin worth using.</p>



<p>WPHelpful has potential. I could see it growing an audience of 100K, 500K, or more users over time with more polishing.  It&#8217;s not there yet.  The plugin doesn&#8217;t have enough meat on its bones for me to recommend it yet, but I&#8217;m hopeful that future versions will offer a more robust experience.</p>



<p>If you&#8217;re looking for an easy-to-use free plugin that works with just posts and pages, it could serve your needs.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Sep 2019 18:01:43 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:21;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:70:"WPTavern: GitHub Adds Dependency Graphs, Security Alerts for PHP Repos";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94088";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:80:"https://wptavern.com/github-adds-dependency-graphs-security-alerts-for-php-repos";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4157:"<div class="wp-block-image"><img /></div>



<p>PHP developers everywhere can rejoice as GitHub adds the long-awaited <a href="https://github.blog/2019-09-18-dependency-graph-supports-php-repos-with-composer-dependencies/">dependency graphs feature for PHP repositories</a> that use Composer.  The feature provides security alerts, shows dependency insights, and displays the dependents of a given repository.  If enabled, it can also automatically send security fixes to the repository via pull requests.</p>



<p>GitHub initially added support for JavaScript and Ruby when <a href="https://github.blog/2017-11-16-introducing-security-alerts-on-github/">rolling out dependency graphs in 2017</a>. They added <a href="https://github.blog/2019-07-02-yarn-support-for-security-alerts/">support for Yarn lock files</a> in July of this year. This has been a boon to the JavaScript community as it alerts developers of vulnerabilities in code they&#8217;re using and shipping to users.</p>



<p>&#8220;We&#8217;re also seeing PHP and Composer grow in popularity&#8211;PHP is the fourth most popular language on GitHub and Composer is the fourth most starred PHP project,&#8221; wrote Justin Hutchings, Senior Product Manager at GitHub.  The company has taken notice of the trends.  JavaScript is a hot topic in many developer circles today, but PHP frameworks such as Laravel and Symfony continue growing in popularity and dominate among <a href="https://github.com/topics/php">popular PHP repositories</a>.</p>



<p>Composer is the <em>de facto</em> standard for PHP dependency management.  Core WordPress first <a href="https://core.trac.wordpress.org/ticket/43558">added Composer support</a> for development environments in version 5.1.  While it&#8217;s not a part of the release package, this was some small victory after a <a href="https://core.trac.wordpress.org/ticket/23912">years-long discussion</a> of adding a basic <code>composer.json</code> file to core.  Core hasn&#8217;t fully embraced Composer or any type of PHP dependency management, but plugin and theme authors are using it more than a few short years ago.  The new alerts and automatic pull requests will offer one more avenue for catching security issues with plugins and themes.</p>



<p>GitHub seems to be rolling this feature out in waves.  After checking some repositories with dependency graphs enabled, some still do not have their PHP dependencies listed.  It may take some time, but developers should start seeing dependencies appear that are listed in their <code>composer.json</code> or <code>composer.lock</code> files.</p>



<p>Public repositories should begin seeing automatic security alerts when an issue is found.  GitHub will start notifying repository owners of these alerts via web notifications or email, depending on what the account holder has set as their preference.  Developers with private repos or who have disabled dependency graphs will need to enable them to take advantage of the new feature.</p>



<p>Security alerts on old repositories could become an annoyance.  GitHub recommends archiving those repos.  &#8220;Archived repositories send a signal to the rest of the community that they aren&#8217;t maintained and don&#8217;t receive security alerts,&#8221; explained Hutchings.</p>



<p>Developers who have opted into GitHub&#8217;s <a href="https://help.github.com/en/articles/configuring-automated-security-fixes">automatic security fixes beta</a> feature can now enjoy automatic pull requests (PRs) from GitHub when vulnerabilities are found.  GitHub creates a PR with the minimum possible secure version.  The developer can then merge the PR at their discretion.</p>



<p>Dependency graphs also make for a much nicer experience when browsing a repository&#8217;s dependencies.  Previously, developers would need to dive into a project&#8217;s <code>composer.json</code> or view them from Packagist, the official package directory for Composer.  Developers can now click on a link to view a dependent repository.</p>



<p>Rolling this feature out for PHP repos is a welcome addition and should help more projects keep their code secure.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 20 Sep 2019 17:45:05 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:22;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:67:"BuddyPress: A new place to learn how to build on top of BuddyPress!";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://buddypress.org/?p=307844";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:45:"https://buddypress.org/2019/09/bp-devhub-1-0/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4575:"<p>Hi!</p>



<p>We’re very excited to officially announce the launch of a new development resources site on the BuddyPress.org network.</p>



<div class="wp-block-image"><a href="https://developer.buddypress.org/" target="_blank" rel="noreferrer noopener"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/bpdevhub-landing-page-1024x652.png" alt="" class="wp-image-307848" /></a></div>



<p>Today we are inaugurating <strong><a href="https://developer.buddypress.org/">developer.buddypress.org</a></strong> with a complete <a href="https://developer.buddypress.org/bp-rest-api/">handbook documenting the BP REST API</a>. This API will be introduced into our next major version which is scheduled on September 30, 2019. We thought you’d be interested to have a tool to help you discover the BuddyPress REST endpoints and their parameters to start playing with them (You’ll need <a href="https://buddypress.org/2019/09/buddypress-5-0-0-release-candidate/">BuddyPress 5.0.0-RC1</a> to have even more fun with it!).</p>



<div class="wp-block-spacer"></div>



<h2 id="rest-api-handbook">Using the BP REST API Handbook</h2>



<p>The main part of the handbook is the «&nbsp;Developer Endpoint Reference&nbsp;». We grouped these endpoints according to the component they belongs to.</p>



<div class="wp-block-image"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/bpdevhub-02-911x1024.png" alt="" class="wp-image-307845" /><a rel="noreferrer noopener" href="https://buddypress.org/wp-content/uploads/1/2019/09/bpdevhub-02.png" target="_blank">View the full screenshot in a new tab.</a></div>



<p>Each page of the reference is firstly introducing the component and describing the data schema of items contained into the REST responses. Then for each verb (or method), you&#8217;ll find the available arguments, their definition and an example of use with the <code><a href="https://bpdevel.wordpress.com/2019/09/12/let-us-start-using-the-bp-rest-api/">bp.apiRequest()</a></code> JavaScript function. Below is a screenshot of the method to get a specific Activity.</p>



<div class="wp-block-image"><a href="https://developer.buddypress.org/bp-rest-api/reference/activity/#retrieve-a-specific-activity"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/bpdevhub-verb-example.png" alt="" class="wp-image-307851" /></a></div>



<h2>The future of this development resources hub</h2>



<p>You can have a good idea of what’s coming next into this developer oriented site looking at its current landing page. We will first work on building the full <a href="https://buddypress.trac.wordpress.org/ticket/6812">PHP Code Reference for BuddyPress</a>: functions, classes and hooks.<br /></p>



<p>Then, we haven’t planned anything yet <img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/263a.png" alt="☺" class="wp-smiley" /> and we’re very opened to ideas and of course contributions about the «&nbsp;how&nbsp;» step and the «&nbsp;do&nbsp;» one.</p>



<div class="wp-block-spacer"></div>



<h2>About the editing workflow</h2>



<p>Unlike the BuddyPress Codex, it’s not possible for everyone to directly edit the content of the BP REST API Handbook or the future PHP Code Reference.</p>



<div class="wp-block-image"><a href="https://buddypress.trac.wordpress.org/newticket"><img src="https://buddypress.org/wp-content/uploads/1/2019/09/new-ticket.png" alt="" class="wp-image-307857" /></a></div>



<p>But you can always report issues or suggest improvements using our <a href="https://buddypress.trac.wordpress.org">Bug Tracker</a> making sure to select the «&nbsp;<strong>BuddyPress.org sites</strong>&nbsp;» option of the components dropdown of your ticket.</p>



<div class="wp-block-spacer"></div>



<h2>Props</h2>



<p>The first version of the development resources hub was built thanks to the involvement of these contributors:</p>



<p><a href="https://profiles.wordpress.org/boonebgorges/">Boone B Gorges (boonebgorges)</a>, <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby (johnjamesjacoby)</a>,&nbsp;<a href="https://profiles.wordpress.org/imath/">Mathieu Viet (imath)</a>,  <a href="https://profiles.wordpress.org/tw2113/">Michael Beckwith (tw2113)</a>,&nbsp;<a href="https://profiles.wordpress.org/espellcaste/">Renato Alves (espellcaste)</a>,&nbsp;<a href="https://profiles.wordpress.org/netweb/">Stephen Edgar (netweb)</a>.</p>



<p>Many thanks to them <img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f609.png" alt="😉" class="wp-smiley" /></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 20 Sep 2019 14:47:52 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:5:"imath";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:23;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:94:"Post Status: Salesforce Ventures invests $300 million in Automattic, at a $3 billion valuation";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:31:"https://poststatus.com/?p=68901";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:54:"https://poststatus.com/salesforce-ventures-automattic/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:6125:"<p>Salesforce Ventures is the investment arm of Salesforce. Prior to this investment in <a href="https://automattic.com/">Automattic</a>, Salesforce Ventures had <a href="https://www.salesforce.com/company/ventures/funds/">announced</a> $875 million raised across 11 fund initiatives — but none that amounts to $300 million. Previosuly, each fund has been between $50 to $125 million spread across several <a href="https://www.crunchbase.com/organization/salesforce-ventures/investments/investments_list">investments</a>.</p>



<p>I believe <a href="https://www.salesforce.com/company/ventures/">Salesforce Ventures</a> called up funds specifically for this strategic investment in Automattic, which would likely put their total dollars invested (or committed to existing funds) well beyond $1 billion, and the $300 million into Automattic would be their largest investment to date, to my knowledge.</p>



<p>Salesforce Ventures states on their website that they are exclusively seeking investments in &#8220;enterprise cloud&#8221; companies. In Automattic Founder and CEO Matt Mullenweg&#8217;s <a href="https://ma.tt/2019/09/series-d/">announcement</a> about the funding, he specifically noted how Salesforce CEO Marc Benioff, &#8220;helped open my eyes to the incredible traction WordPress <a href="https://wpvip.com/">and WP VIP</a> has seen in the enterprise market, and how much potential there still is there.&#8221; I am curious to see how Automattic changes their approach to VIP in particular, in light of this.</p>



<p>$300 million is a lot of money. Salesforce is joining Insight Venture Partners, Tiger Global, and True Ventures as primary outside investors in Automattic. </p>



<p>Given that Salesforce was the lead and only investor here, they now own a significant stake in Automattic — and it will be interesting to see what kind of confluence that enables between the two companies. Automattic CEO Matt Mullenweg tells me, &#8220;Automattic has been a long-time customer of Salesforce’s products, and we think there are lots of opportunities for closer integration.&#8221;</p>



<p>Since Automattic recently acquired Tumblr and brought on a few hundred new employees from it, it&#8217;s natural to think the new fundraising is related. I asked Matt about that, and he said it was unrelated in terms of financial justification, but they did, &#8220;disclose the Tumblr transaction to Salesforce during [their] discussions.&#8221;</p>



<p>Automattic hasn&#8217;t raised money since 2014, and it seems like this round is similar to prior ones, wherein the money helps spur their growth plans along but that they are operationally profitable — or close to it. Matt <a href="https://techcrunch.com/2019/09/19/automattic-raises-300-million-at-3-billion-valuation-from-salesforce-ventures/">told Techcrunch</a>, &#8220;The roadmap is the same. I just think we might be able to do it in five years instead of ten.&#8221;</p>



<p>Matt called the investment proof of Salesforce&#8217;s own &#8220;tremendous vote of confidence for Automattic and for the open web.&#8221; Salesforce does have some history of supporting <a href="https://opensource.salesforce.com/">open source projects</a>, although that shouldn&#8217;t be equated to an investment in Automattic as a company; it is a vote of confidence for companies that rely on open-source platforms as a part of their line of business.</p>



<p>Automattic is the single most significant contributor to WordPress and related open-source projects. It also relies on open-source software for its product development — particularly Jetpack and WooCommerce — and features like Gutenberg as the core experience for writing and site-building. How that blend of open source software and business development plays out, in the long run, is certainly of high interest to the open-source community.</p>



<p>I have long discussed on various platforms how I think there are a handful of companies that are big enough to acquire Automattic someday. I still think Automattic is more likely to go public at some point, but if they are acquired, Salesforce is definitely one of those few with the resources to make it happen, and perhaps the operational congruence as well.</p>



<p>Reaching a $3 billion valuation is an amazing feat that Automattic has achieved. Matt has said before that he believes each of their primary lines of business — WordPress.com, WooCommerce, and Jetpack — can be multi-billion dollar opportunities. I agree with him, particularly for WooCommerce. I think there&#8217;s a good chance WooCommerce will end up several times more valuable than all their other lines of business combined. <span class="pullquote alignleft">I would love to see these new funds be funnelled into the incredible opportunity in the eCommerce landscape; one only needs to look at what Shopify has done to see what&#8217;s possible, and just how successful it can be.</span> </p>



<p>I asked Matt why he was attracted to an investment from Salesforce&#8217;s VC arm, rather than an investment-only style firm. He said, &#8220;I love Salesforce’s philosophy, how they cultivate such a fantastic community, how they support progressive policies in San Francisco and the other cities they operate in, how they’ve been founder-led, their scale, their leadership, and their longevity in defining an entirely new class of software and being the most significant player in it.&#8221;</p>



<p>I love the point about Salesforce defining a class of software — and I have realized recently just how huge their developer community is — so I really appreciate this comment from Matt. Making commercial and SaaS software is a well-established business now. Automattic is in a unique position as the most powerful player in an <em>open</em> ecosystem proud of its independence. This provides many unique opportunities for Automattic the business and unique challenges for Automattic the WordPress community member.</p>



<p><em>Image credit: <a href="https://ma.tt/2019/09/series-d/">Matt Mullenweg</a>, whose blog I brazenly stole it from.</em></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 19 Sep 2019 23:05:39 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Brian Krogsgard";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:24;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:102:"WPTavern: WordPress Community Contributors to Host Free Online Diversity Workshop Ahead of WordCamp US";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93735";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:113:"https://wptavern.com/wordpress-community-contributors-to-host-free-online-diversity-workshop-ahead-of-wordcamp-us";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5663:"<p>WordCamp US will <a href="https://2019.us.wordcamp.org/2019/08/08/wordcamp-us-debuts-community-track/" rel="noopener noreferrer" target="_blank">debut a new Community Track</a> in November that will feature sessions and workshops on topics like meetups, WordCamps, diversity and inclusion, and kids/youth. Jill Binder, Allie Nimmons, Aurooba Ahmed, and David Wolfpaw will be hosting a workshop called &#8220;Creating a Welcoming and Diverse Space&#8221; at the event. In order to adequately prepare for presenting on this sensitive topic, the team will be <a href="https://make.wordpress.org/community/2019/09/13/call-for-participants-creating-a-welcoming-and-diverse-space-online-workshop-on-sun-oct-6/" rel="noopener noreferrer" target="_blank">running the workshop in a live, interactive Zoom call on Sunday, October 6</a>.</p>
<p>In light of the recent news about <a href="https://wptavern.com/php-central-europe-conference-canceled-due-to-lack-of-speaker-diversity" rel="noopener noreferrer" target="_blank">a central European PHP conference getting canceled due to a lack of a diverse lineup</a>, the broader PHP community is becoming more conscious of the importance of recruiting speakers that better represent their communities.</p>
<p>&#8220;The <a href="https://tiny.cc/wpdiversity" rel="noopener noreferrer" target="_blank">Diverse Speaker Workshops</a> that I’m running in WordPress and am <a href="https://diversein.tech/" rel="noopener noreferrer" target="_blank">bringing to other technologies</a> have been just as important for years as they are now,&#8221; training leader <a href="https://diversein.tech" rel="noopener noreferrer" target="_blank">Jill Binder</a> said. &#8220;These workshops are an essential piece to the whole puzzle for creating diverse communities, attendance at events, public speakers, and ultimately, leaders and organizers.&#8221;</p>
<p>Binder said there are many factors in society that work against having diversity in a tech event’s public speaker lineup, but one that her team is specifically tackling in these workshops is <a href="https://en.wikipedia.org/wiki/Impostor_syndrome" rel="noopener noreferrer" target="_blank">imposter syndrome</a>.</p>
<p>&#8220;Our workshops help folks bust through their impostor syndrome and develop a topic, title, pitch, bio, and outline, more confidence in public speaking, and the motivation to start speaking,&#8221; Binder said.</p>
<p>&#8220;The new workshop that Allie, Aurooba, David, and I are creating for WordCamp US on &#8216;Creating a Welcoming and Diverse Space&#8217; is another important piece to the puzzle. We are going to be teaching mindset, community, environment, speakers, and allyship. It will be an interactive workshop where people will walk away with an action list they can start implementing in their communities (whether in person or online) right away.&#8221;</p>
<p>Some organizers of tech events have claimed that for certain events it is impossible to create a diverse lineup of speakers due to the demographics of the community and lack of willing participants.</p>
<p>Binder said that in her experience it is unlikely that more diverse people are unwilling to speak but rather that the event is not being created with more kinds of people in mind. She offered a few suggestions for organizers to consider in planning ahead for a welcoming and diverse space:</p>
<ul>
<li>Have the event at different times that work for people with families. For example, don’t hold them all at 9pm at night. Weekend afternoons may work. Ask those with children what works for them.</li>
<li>Consider venues that are not centered around alcohol (like bars and pubs). This opens up the event to attendees who are under 21, recovering addicts, folks who belong to a religious group that prohibits alcohol, and many other people who don’t feel safe or welcome in an alcohol-focused environment.</li>
<li>Choose venues that have accessible alternatives to stairs, such as elevators and ramps.</li>
<li>Try to have more diversity in the organizing team.</li>
<li>Bring in more diverse speakers. Don’t know how? Check out the Diverse Speaker Workshop – <a href="https://tiny.cc/wpdiversity" rel="noopener noreferrer" target="_blank">in WordPress</a> and in <a href="https://diversein.tech/" rel="noopener noreferrer" target="_blank">other techs communities.</a></li>
</ul>
<p>She also recommends organizers directly invite more people into their communities.</p>
<p>&#8220;Ask people in your network to introduce you to diverse people they may know who work with WordPress or your technology,&#8221; Binder said. &#8220;You can even go out and find those communities in your area – online and in person – or ask people to make an introduction for you to those groups. Examples of groups: Ladies Learning Code, Black Girls Code. Form genuine, friendly relationships with community members so that they can help you reach the WordPress enthusiasts in their communities.&#8221;</p>
<p>Binder said the team will go into more detail on these topics during the workshop. Anyone who would like to learn more is welcome to attend the online public rehearsal for the workshop on October 6, at 3pm-5pm ET. This is a unique opportunity for those who cannot attend WordCamp US to join in on one of the interactive workshops. <a href="https://make.wordpress.org/community/2019/09/13/call-for-participants-creating-a-welcoming-and-diverse-space-online-workshop-on-sun-oct-6/" rel="noopener noreferrer" target="_blank">Comment on the Community team&#8217;s post</a> with contact information and workshop leaders will send the zoom link and more information.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 19 Sep 2019 22:14:48 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:25;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:47:"WPTavern: Bayleaf: A Food and Recipe Blog Theme";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94041";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:57:"https://wptavern.com/bayleaf-a-food-and-recipe-blog-theme";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5971:"<div class="wp-block-image"><img /></div>



<p>With the WordPress theme directory dominated by business-oriented themes, it&#8217;s sometimes tough to find themes that cater to more specific user groups. If you dig deep enough, you&#8217;ll find something outside the norm.  <a href="https://wordpress.org/themes/bayleaf/">Bayleaf</a> is a blog theme specifically designed for sharing food and recipes.  </p>



<p>The theme is designed and developed by <a href="https://vedathemes.com/">Vedathemes</a>.  They currently have three themes in the theme directory, which follow the same clean design trend of Bayleaf.</p>



<p>Food-related themes excite me.  In my off-time, I&#8217;m often browsing recipe blogs and looking for my next culinary experiment.  The problem with many such sites is their designs have too much noise instead of just showcasing the content visitors are looking for.  I was pleasantly surprised at the minimalist and open approach in the <a href="https://vedathemes.com/bayleaf-pro/">Bayleaf demo</a>.</p>



<p><em>Admittedly, I was drawn in by all the yummy food pics.</em></p>



<p>The theme author has obviously taken a look at the food blogs and built a design that showcases what&#8217;s possible without adding complexity.  The related posts feature is also a nice extra for site visitors who&#8217;ll likely look for related recipes.</p>



<p>Bayleaf combines the Poppins and Montserrat font families to create bold headers that are complimented by readable body copy.  The theme comes with options for displaying a sidebar on single posts or pages, but I&#8217;d recommend opting out.  The design works best without a sidebar, allowing more breathing room for sharing food images.</p>



<p>The theme is slowly building an audience since its release in February.  It currently has 1,000+ installs and a five-star rating from six reviews in the theme directory.</p>



<h2>Create a Unique Look with the Display Posts Widget</h2>



<div class="wp-block-image"><img /></div>



<p>Bayleaf&#8217;s most prominent custom feature is its Display Posts widget.  By placing this widget in the Homepage Above Content or Homepage Below Content sidebar, users have a ton of variety with how their site looks.  No two homepages need look alike.</p>



<p>The widget comes with six list, grid, and slider styles to choose from. It supports custom post types and taxonomies, so users can use it for content such as events, products, or anything else they want to showcase.</p>



<p>My first thought when viewing the demo was, <em>Not another complicated slider with a hard-to-configure customizer experience.</em> While I&#8217;m not usually a fan of sliders, configuring this one was easy.  Plus, the grid and list styles offered alternative options.</p>



<p>A lot of themes overdo features like this, offering a clunky experience within the customizer.  However, Bayleaf keeps it simple by packaging the feature as a widget with just enough variety to cover most use cases.</p>



<p>My one complaint with the Display Posts widget is that it was hard to find at first.  At this point, it should be standard practice to prefix custom widgets with the theme name.  &#8220;Bayleaf: Display Posts&#8221; would&#8217;ve been far easier to pick from the widget lineup.</p>



<h2>Handling Block Editor Support</h2>



<div class="wp-block-image"><img /></div>



<p>I tested Bayleaf against the latest public release of the Gutenberg plugin. The theme is not without a few problems, which is par for the course with most themes supporting the block editor.  The Gutenberg plugin&#8217;s development is fast-paced, and it&#8217;s tough for theme authors to keep up.  Something that works one week could break the next.</p>



<p>The theme takes a minimalist approach with regards to the editor, allowing the default editor styles to handle much of the layout.  With the break-neck pace of change, this can sometimes be a better approach than attempting to manage every style.</p>



<p>There are areas where Bayleaf could be more opinionated.  For example, the alignment and typography for the post title aren&#8217;t a one-to-one match between the editor and front end.  The content width is wider on the front end than the editor, which means the number of characters per line doesn&#8217;t match.  There are several minor items where the block editor overrules theme styles.</p>



<p>The theme doesn&#8217;t offer a 100% WYSIWYG experience, but it&#8217;s close enough at this stage and doesn&#8217;t break anything.  Most issues are trivial and will simply take some adjustment time, assuming Gutenberg development settles a bit.</p>



<h2>How Does the Code Stand Up?</h2>



<p>Bayleaf isn&#8217;t pushing any boundaries, but it&#8217;s solid in comparison to the average theme.  It&#8217;s based on the Underscores starter, which serves as the <em>de facto</em> standard for many themes in the official directory.</p>



<p>The theme doesn&#8217;t have a ton of custom code, so there are few places it could go wrong.</p>



<p>Like all themes in the official directory, it undergoes a rigorous review process.  It&#8217;s <a href="https://themes.trac.wordpress.org/query?keywords=~theme-bayleaf">Trac history</a> doesn&#8217;t show anything worrisome.  Vedathemes seems to have a good grasp of building themes that meet the official theme review standards.</p>



<h2>The Final Verdict</h2>



<p>You won&#8217;t find a boatload of options in Bayleaf.  What you will find is a clean design that gets out of the way but with enough features to have fun tinkering around on your blog for a couple of hours.  The Display Posts widget can get you pretty far with little work.</p>



<p>If you&#8217;re looking for a change to your food or recipe blog, you can&#8217;t go wrong giving this theme a run. </p>



<p>The theme could also be used for other types of sites.  There are no specific features that limit its use to only food blogging.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 19 Sep 2019 18:49:50 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:26;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:93:"WPTavern: Automattic Raises $300M in Series D Investment Round, Valuation Jumps to $3 Billion";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94026";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:101:"https://wptavern.com/automattic-raises-300m-in-series-d-investment-round-valuation-jumps-to-3-billion";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:6254:"<p><a href="https://automattic.com/" rel="noopener noreferrer" target="_blank">Automattic</a> announced another round of funding today, a $300 million Series D investment from <a href="https://www.salesforce.com/company/ventures/" rel="noopener noreferrer" target="_blank">Salesforce Ventures</a>, giving the company a $3 billion valuation post-funding. The last time Automattic raised money was $160 million in a Series C round in 2014. Since that time the company has grown to more than 950 employees and made strategic acquisitions, including <a href="https://wptavern.com/automattic-acquires-woocommerce" rel="noopener noreferrer" target="_blank">WooCommerce</a> in 2015 and <a href="https://wptavern.com/automattic-acquires-tumblr-plans-to-rebuild-the-backend-powered-by-wordpress" rel="noopener noreferrer" target="_blank">Tumblr</a> (closing in 2019).</p>
<p>CEO Matt Mulleneweg <a href="https://ma.tt/2019/09/series-d/" rel="noopener noreferrer" target="_blank">said</a> the funds will enable the company to speed up and scale its product development, as well as continue the company&#8217;s continual contributions to WordPress:</p>
<blockquote><p>For Automattic, the funding will allow us to accelerate our roadmap (perhaps by double) and scale up our existing products—including WordPress.com, WordPress VIP, WooCommerce, Jetpack, and (in a few days when it closes) Tumblr. It will also allow us to increase investing our time and energy into the future of the open source WordPress and Gutenberg.</p></blockquote>
<p>In 2016, Mullenweg <a href="https://wptavern.com/woocommerce-powers-42-of-all-online-stores" rel="noopener noreferrer" target="_blank">identified both Jetpack and WooCommerce as “multi-billion dollar opportunities”</a> that could each be larger than WordPress.com in the future. Jetpack has grown from 1+ million users in 2016 to more than 5 million today. The plugin&#8217;s product team has aggressively expanded its commercial plans and features and is one of the first to experiment with offering <a href="https://wptavern.com/jetpack-7-6-improves-amp-compatibility-adds-preview-and-upgrade-nudge-for-blocks-only-available-on-paid-plans" rel="noopener noreferrer" target="_blank">previews and commercial upgrade nudges for blocks in WordPress&#8217; editor</a>.</p>
<p>WooCommerce has also grown to more than 5 million active installs (from 1+ million in 2015 at the time of acquisition). The e-commerce platform has a more challenging market with formidable competitors like Shopify, which recently <a href="https://observer.com/2019/09/shopify-ebay-amazon-ecommerce-growth/" rel="noopener noreferrer" target="_blank">overtook eBay as the second largest shopping site after Amazon</a>. Shopify <a href="https://investors.shopify.com/Investor-News-Details/2019/Shopify-Announces-Second-Quarter-2019-Financial-Results/default.aspx" rel="noopener noreferrer" target="_blank">reported $362 million in revenue</a> during its last quarter with $153 million coming from subscriptions to the Shopify platform.</p>
<p>I asked Mullenweg about how the funding might help Automattic make WooCommerce more user-friendly and competitive. Despite going up against the seemingly indomitable e-commerce powerhouses, Mullenweg sees WooCommerce&#8217;s platform an opportunity for growing independent stores on the web.</p>
<p>&#8220;WooCommerce already represents the best way to marry content and commerce, and has a huge advantage being so tightly integrated from a user perspective with WordPress itself,&#8221; Mullenweg said. &#8220;However it also inherits some of the barriers WordPress has to adoption, particularly from new users. I think that Gutenberg will help a ton, as it&#8217;s better than any of the builders the eCommerce players have, and when that gets combined with the flexibility, control, and scalability you get from WP + WooCommerce it&#8217;s going to be huge. There&#8217;s a ton of work left to do, though, and we&#8217;re trying to grow that team as quickly as possible to keep up with the opportunity.&#8221;</p>
<p>Mullenweg declined to share any information about Jetpack and WooCommerce&#8217;s revenue today but confirmed that they have not yet eclipsed WordPress.com.</p>
<p>&#8220;What I can say is that WP.com is still our biggest business, and WooCommerce was our fastest growing last year,&#8221; he said.</p>
<p>Automattic&#8217;s most recent round of funding will help the company better monetize these products that have grown in tandem with WordPress&#8217; market share, which W3Techs puts at <a href="https://w3techs.com/technologies/details/cm-wordpress/all/all" rel="noopener noreferrer" target="_blank">34.5%</a> of the top 10 million websites. Independent stores sitting on top of this large chunk of the web represent a significant market that Automattic is currently dominating in the WordPress space.</p>
<p>The Tumblr acquisition also affords another opportunity to introduce e-commerce solutions to more of Automattic&#8217;s customers. Mullenweg previously said the Tumblr app receives 20X more daily signups than the WordPress mobile app. The social network/blogging hybrid also has a significantly younger user base, based on <a href="https://weareflint.co.uk/main-findings-social-media-demographics-uk-usa-2018" rel="noopener noreferrer" target="_blank">a 2018 study</a> that found 43 percent of internet users between the ages of 18 to 24 years old used Tumblr. It&#8217;s an untapped market for e-commerce, as Tumblr users who want to sell currently have to use a service like Shopify or Ecwid and generate a Tumblr-compatible widget.</p>
<p>Mullenweg said the acquisition hasn&#8217;t closed yet but Automattic may explore e-commerce on Tumblr in 2020.</p>
<p>&#8220;Once it closes there will be a few months of normal integration work and getting the teams working together, making sure we have harmonized policies on support, content moderation, anti-spam, ads, and all of those lower-level things,&#8221; he said. &#8220;Beyond that I’ve seen what you’re seeing — a lot of Tumblr users want access to more customization and e-Commerce. There are no specific plans yet but I imagine that’s something the team will consider for next year’s roadmap.&#8221;</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 19 Sep 2019 16:15:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:27;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:29:"Matt: Automattic’s Series D";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:22:"https://ma.tt/?p=50121";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:31:"https://ma.tt/2019/09/series-d/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3727:"<p>Today <a href="https://automattic.com/">Automattic</a> announced it has closed a new $300 million Series D, with <a href="https://www.salesforce.com/company/ventures/">Salesforce Ventures</a> taking the entire round. This puts us at a post-round valuation of $3 billion, three times what it was after our last fundraising round in 2014. It&#8217;s a tremendous vote of confidence for Automattic and for the open web.</p>



<p>I met <a href="https://twitter.com/Benioff">Marc Benioff</a> earlier this year, and it became obvious to both of us that <a href="https://salesforce.com/">Salesforce</a> and Automattic shared a lot of principles and philosophies. Marc is a mindful leader and his sensibilities and sense of purpose feel well aligned with our own mission to make the web a better place. He also helped open my eyes to the incredible traction WordPress <a href="https://wpvip.com/">and WP VIP</a> has seen in the enterprise market, and how much potential there still is there. I’ve also loved re-connecting with <a href="https://twitter.com/btaylor">Bret Taylor</a> who is now Salesforce’s President and Chief Product Officer. Bret’s <a href="https://en.wikipedia.org/wiki/Bret_Taylor">experience</a> across Google Maps, Friendfeed, Facebook, Quip, and now transforming Salesforce makes him one of the singular product thinkers out there and our discussion of Automattic’s portfolio of services have been very helpful already.</p>



<p>For Automattic, the funding will allow us to accelerate our roadmap (perhaps by double) and scale up our existing products—including <a href="https://wordpress.com/">WordPress.com</a>, <a href="https://wpvip.com/">WordPress VIP</a>, <a href="https://woocommerce.com/">WooCommerce</a>, <a href="https://jetpack.com/">Jetpack</a>, and (in a few days when it closes) <a href="https://www.tumblr.com/">Tumblr</a>. It will also allow us to increase investing our time and energy into the future of the open source <a href="https://wordpress.org/">WordPress</a> and <a href="https://wordpress.org/gutenberg/">Gutenberg</a>.</p>



<p>The Salesforce funding is also a vote of confidence for the future of work. Automattic has grown to more than 950 employees working from 71 countries, with no central office for several years now. <a href="https://distributed.blog/">Distributed work</a> is going to reshape how we spread opportunity more equitably around the world. There continue to be new heights shown of what can be achieved in a distributed fashion, with <a href="https://about.gitlab.com/2019/09/17/gitlab-series-e-funding/">Gitlab announcing a round at $2.75B earlier this week</a>.</p>



<p>Next year Automattic celebrates 15 years as a company! The timing is fortuitous as we&#8217;ve all just returned from <a href="https://ma.tt/2018/10/the-importance-of-meeting-in-person/">Automattic&#8217;s annual Grand Meetup</a>, where more than 800 of us got together in person to share our experiences, explore new ideas, and have some fun. I am giddy to work alongside these wonderful people for another 15 years and beyond.</p>



<p>If you&#8217;re curious my previous posts on our fundraising, here&#8217;s our <a href="https://ma.tt/2006/04/a-little-funding/">2006 Series A</a>, <a href="https://ma.tt/2008/01/act-two/">2008 Series B</a>, <a href="https://ma.tt/2013/05/automattic-secondary/">2013 secondary</a>, and <a href="https://ma.tt/2014/05/new-funding-for-automattic/">2014 Series C</a>. As before, happy to answer questions in the comments here. <a href="https://techcrunch.com/2019/09/19/automattic-raises-300-million-at-3-billion-valuation-from-salesforce-ventures/">I also did an exclusive interview with Romain Dillet on (WP-powered) Techcrunch</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 19 Sep 2019 13:01:36 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Matt";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:28;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:103:"WPTavern: Gutenberg 6.5 Adds Experimental Block Directory Search to Inserter and New Social Links Block";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=94000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:114:"https://wptavern.com/gutenberg-6-5-adds-experimental-block-directory-search-to-inserter-and-new-social-links-block";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3773:"<p>Gutenberg 6.5 was released today with a rough prototype that adds one-click search and installation of blocks from the block directory to the inserter. Selected blocks are automatically installed as a plugin in the background and inserted into the editor with one click.</p>
<p><a href="https://i0.wp.com/wptavern.com/wp-content/uploads/2019/09/block-directory-experiment.gif?ssl=1"><img /></a></p>
<p>The <a href="https://github.com/WordPress/gutenberg/pull/17431" rel="noopener noreferrer" target="_blank">pull request</a> for the experiment indicates that it&#8217;s still very much a work in progress. It extends the inserter to fetch a list of suggestBlocks similar to reusableBlocks, and the list is currently served from a mock API.</p>
<p>The prototype is can be turned on under the <strong>Gutenberg > Experiments</strong> menu, a relatively new Settings page that was <a href="https://github.com/WordPress/gutenberg/pull/16626" rel="noopener noreferrer" target="_blank">added in Gutenberg 6.3</a>. This menu also allows testers to enable the experimental Navigation Menu Block, Widgets Screen, and Legacy Widget Block.</p>
<p><a href="https://i0.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-18-at-8.46.59-PM.png?ssl=1"><img /></a></p>
<p>Block Navigation has also been added to the experimental Navigation Block in this release. This addition is considered a first start that is expected to evolve over time.</p>
<p><a href="https://i2.wp.com/wptavern.com/wp-content/uploads/2019/09/block-navigator.png?ssl=1"><img /></a></p>
<p>&#8220;Regardless of how the UI evolves for this block, I think there will always be a need for representing the menu structure as a child block tree with all menu hierarchies mapped out consistently and not hidden (dropdowns, etc),&#8221; Gutenberg engineer Matias Ventura <a href="https://github.com/WordPress/gutenberg/issues/16812" rel="noopener noreferrer" target="_blank">said</a>.</p>
<p>&#8220;Luckily, we already have a view that handles that representation in the &#8216;block navigator.&#8217; That means if the navigation menu block is represented through child blocks, we&#8217;ll get this view for free.&#8221;</p>
<p>In the future, Gutenberg engineers may allow for drag-and-drop reordering of items in the navigator and may explore rendering the view inline or in a modal launched from the navigation menu block.</p>
<h3>Gutenberg 6.5 Adds New Social Links Block</h3>
<p>Gutenberg 6.5 also adds a new social links block under the widgets panel. It allows users to place social links anywhere within the content by clicking on the icons and pasting in their social URLs. The gif below demonstrates how the block works, although the grey placeholder icons have since been removed in favor of opacity changes to indicate unconfigured blocks.</p>
<p><a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/social-links-block.gif?ssl=1"><img /></a></p>
<p>This release introduces a handful of other notable updates, including support for <a href="https://github.com/WordPress/gutenberg/pull/17253" rel="noopener noreferrer" target="_blank">border radius changes in the button block</a>, support for <a href="https://github.com/WordPress/gutenberg/pull/17101" rel="noopener noreferrer" target="_blank">adding captions to images in the Gallery block</a>, and the addition of <a href="https://github.com/WordPress/gutenberg/pull/16490" rel="noopener noreferrer" target="_blank">local autosaves</a>.</p>
<p>The 6.5 release post has not yet been published but the plugin update is available in the admin and a full list of enhancements and bug fixes can be found in the <a href="https://wordpress.org/plugins/gutenberg/#developers" rel="noopener noreferrer" target="_blank">changelog</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 19 Sep 2019 03:41:01 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:29;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:110:"WPTavern: EditorsKit Adds Nofollow Options for Links, Fixes Bug with Gutenberg Metaboxes Overlapping in Chrome";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93971";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:120:"https://wptavern.com/editorskit-adds-nofollow-options-for-links-fixes-bug-with-gutenberg-metaboxes-overlapping-in-chrome";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3528:"<p><a href="https://wordpress.org/plugins/block-options/" rel="noopener noreferrer" target="_blank">EditorsKit</a> is becoming somewhat of a &#8220;hotfix&#8221; plugin for Gutenberg, especially with the additions to the 1.14 release this week. Developer Jeffrey Carandang added new link formats for nofollow rel attribute options, along with a fix for an annoying bug in Chrome that causes Gutenberg metaboxes to overlap. He has been closely monitoring feedback on both Gutenberg and EditorsKit, introducing features for which users have an immediate need.</p>
<p>Google recently announced <a href="https://wptavern.com/google-announces-new-ways-to-identify-nofollow-links-progress-on-related-gutenberg-ticket-is-currently-stalled" rel="noopener noreferrer" target="_blank">new ways to identify nofollow links</a> with two additional rel attribute options for specifying links as sponsored and/or user-generated content. The Gutenberg core team has expressed hesitation on a <a href="https://github.com/WordPress/gutenberg/pull/16609#issuecomment-527921959">PR that would add nofollow link options</a>, invoking WordPress&#8217; <a href="https://wordpress.org/about/philosophy/" rel="noopener noreferrer" target="_blank">80/20 rule</a>.</p>
<p>Since the related PR doesn&#8217;t seem to be a priority, with no movement for two weeks, Carandang decided to add the nofollow and sponsored rel attribute options to EditorsKit, so users can start following Google&#8217;s recommendations without having to switch to HTML mode. He also managed to make it work with the version of Gutenberg included in core.</p>
<p><a href="https://cloudup.com/cqP3APNEF3j"><img src="https://i1.wp.com/cldup.com/kLcfVSl1UW.gif?resize=627%2C495&ssl=1" alt="Nofollow link options" width="627" height="495" /></a></p>
<p>Chrome users may have noticed that <a href="https://github.com/WordPress/gutenberg/issues/17406" rel="noopener noreferrer" target="_blank">the block editor has a nasty bug with metaboxes overlapping</a>, obscuring the main content area. This problem was introduced in the recent Chrome 77 update and is present on WordPress 5.2.3 and older versions.</p>
<p><a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/metabox-overlapping.png?ssl=1"><img /></a></p>
<p>Chrome developers are aware of the issue and a fix will be in the next release. Version 78 is expected October 22. Since it is a bug with Chrome, the Gutenberg team has opted not to release a fix/workaround for this problem. In the meantime, they recommend updating to WordPress 5.3 if it is released before the Chrome bug is fixed. This isn&#8217;t likely, as 5.3 is scheduled for mid-November.</p>
<p>The Gutenberg team also recommend using a different browser or installing the Gutenberg plugin to fix the issue. Andrea Fercia noted on the ticket that the plugin is still listed among WordPress&#8217; beta plugins and may not be advisable to use in production on some sites. Users with a technical background can implement one of several CSS solutions in the ticket, but this is a frustrating bug for users who don&#8217;t know how to apply code fixes.</p>
<p>Carandang added a fix for this bug to the most recent version of EditorsKit. So far his strategy of being responsive to users&#8217; requests seems to have been successful, as his Gutenberg utility plugin now has more than 1,000 active installs. He said he is happy to add hot fixes for EditorsKit users and will remove them once the fixes have been added to Chrome and/or the block editor.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 18 Sep 2019 19:42:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:30;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:93:"WPTavern: Behind New Packages Project Lead, Theme Review Team Launches Admin Notices Solution";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93936";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:103:"https://wptavern.com/behind-new-packages-project-lead-theme-review-team-launches-admin-notices-solution";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:7754:"<p>As part of the WordPress Theme Review Team&#8217;s plan to <a href="https://wptavern.com/wordpress-theme-review-team-seeks-to-curb-obtrusive-admin-notices-with-new-requirement-to-follow-core-design-patterns">curb obtrusive admin notices</a>, the team pushed version 1.0 of its <a href="https://github.com/WPTRT/admin-notices">Admin Notices</a> package to the public.  The new package provides a standard API for theme authors to display admin notices.</p>



<p>Ari Stathopoulos took over as the packages project lead in late August.  Stathopoulos is the primary developer and creator of the highly-rated <a href="https://kirki.org/">Kirki customizer framework</a>, which currently has 300,000+ active installs as a plugin.  However, the framework is also available as separate modules that theme authors can bundle within their themes.</p>



<p>The Admin Notices package is the third package produced by the team and the first that Stathopoulos has spearheaded.</p>



<p>Adding a basic admin notice in WordPress is relatively easy for most developers. However, handling features such as persistent dismissible actions is more complex.  The Admin Notices package handles this out of the box.</p>



<p>Some options for the package include the ability to:</p>



<ul><li>Set a title and message.</li><li>Select a type that adds in the appropriate UI class (info, success, warning, error).</li><li>Choose which admin screens the notice appears on.</li><li>Limit the message by user capability so that it doesn&#8217;t appear for all users.</li></ul>



<div class="wp-block-image"><a href="https://i0.wp.com/wptavern.com/wp-content/uploads/2019/09/wptrt-admin-notices-002.png?ssl=1" target="_blank" rel="noreferrer noopener"><img /></a></div>



<p>The above screenshot shows an example of a basic admin notice output for the four available types.  The dismiss action is handled by JavaScript and works without reloading the page.  Once dismissed, users will no longer see the notice.</p>



<p>&#8220;I think the hardest thing about it was deciding how restrictive we wanted it to be,&#8221; said Stathopoulos of the challenges building this package. The package restricts theme authors to paragraph, link, bold, and italic elements in version 1.0.  It doesn&#8217;t leave a lot of room for experimentation, but standardization is the goal.  The more elements allowed, the more likely the tool doesn&#8217;t solve the team&#8217;s problem of keeping admin notices unobtrusive.</p>



<h2>User Notifications Are a Complex Problem</h2>



<p>WordPress doesn&#8217;t provide a formal API for user notifications.  However, it does provide a standard set of CSS classes and a hook for attaching notices.  The Codex also has some examples of <a href="https://codex.wordpress.org/Plugin_API/Action_Reference/admin_notices">best practices</a>.  The lack of a formal API has left theme and plugin authors to their own devices.  Users have suffered because of wildly varying implementations and common issues such as <a href="https://wptavern.com/please-stop-abusing-wordpress-admin-notices">non-dismissible advertisements</a>.</p>



<p>Tim Hengeveld <a href="https://core.trac.wordpress.org/ticket/43484">proposed a Notification Center API</a> on Trac in 2018.  The ticket has a healthy, ongoing discussion and some UI proposals.  The proposal is still marked as &#8220;Awaiting Review,&#8221; and it&#8217;s unlikely that it&#8217;ll ship anytime sooner than WordPress 5.4 or later.</p>



<p>Currently, many plugins and themes also use admin notices for user onboarding, which is a separate problem in need of a solution.  There&#8217;s a 4-year-old ticket that discusses <a href="https://core.trac.wordpress.org/ticket/34116">WordPress new-user onboarding</a>, but there&#8217;s not much movement to solve this problem for plugins and themes.</p>



<p>While the TRT&#8217;s package doesn&#8217;t tackle all issues associated with user notifications, it does help limit some of the short-term damage.</p>



<h2>More Packages in the Works</h2>



<p>More packages are currently being built and others are in the planning stages.</p>



<p>The goal of the overall project is to provide theme authors with drop-in modules they can bundle with their themes.  The packages are all written in PHP 5.6+ in hopes to push theme authors toward more modern coding practices (relatively speaking, since PHP 7.4 will be released this year).  It will also help streamline the review process if more theme authors adopt the packages rather than building everything in-house.</p>



<p>&#8220;If we build packages for the most-requested things, we&#8217;ll hopefully empower people to build quality themes easier,&#8221; explained Stathopoulos.  &#8220;I think of packages as building blocks for themes.&#8221;</p>



<p>Stathopoulos is working on a customizer control for selecting a color with alpha transparency, which could be released as early as next week.  It will provide theme users with more control over how their colors appear for themes that implement it.</p>



<p>&#8220;After we build the basics I want to focus on packages that would enhance a11y and privacy in themes &#8211; two areas where themes are falling short,&#8221; he said.  &#8220;It would help a lot of people, and that is ultimately our goal.&#8221;</p>



<p>Theme authors have grown accustomed to installing JavaScript and CSS packages via NPM over the past few years.  However, their use of Composer as a PHP dependency manager has lagged.  In some part, this could be due to WordPress&#8217; previous reluctance to bump its minimum version of PHP.  Many packages available on Packagist, the main Composer repository, do not work with older versions of PHP.  WordPress&#8217; recent jump to PHP 5.6+ and plans to move to 7+ in the future may push more theme authors to consider PHP dependency management.</p>



<p>The TRT has a <a href="https://packagist.org/packages/wptrt/">Packagist account</a> and has made all of its packages installable via Composer.</p>



<h2>No Requirement to Use Packages Yet</h2>



<p>There are no current plans for the TRT to start requiring the use of these packages for specific features, but a few team members have proposed doing so.</p>



<p>&#8220;There are valid reasons to enforce the use of these packages, but it can&#8217;t happen overnight,&#8221; said Stathopoulos. &#8220;We want themes in the repository to have some standards, it can not be the wild west. Code quality has to improve. These packages are a way to make life easier for people, and ultimately save time for everyone.&#8221;</p>



<p>Stathopoulos is open to theme authors building custom implementations if they can improve upon what the team has built, but he prefers that authors &#8220;discuss their ideas in the package repository and submit a pull-request so that the whole community can benefit.&#8221;</p>



<p>Getting theme authors involved is one area where the team has struggled.  Contributing to the packages could benefit the entire community.  &#8220;Most people don&#8217;t even know about them since they are not listed anywhere,&#8221; said Stathopoulos.  &#8220;Theme Authors currently have to look for them, and in order to look for them someone needs to tell them they exist (which doesn&#8217;t happen).&#8221;  One of the next steps would be getting the packages listed in the TRT&#8217;s documentation.</p>



<p>Working together on common theme features could provide a bridge between theme authors and reviewers, allowing them to solve issues together.</p>



<hr class="wp-block-separator" />



<p><em>Note: The author of this article was involved with the initial theme packages proposal and a developer on its initial package releases.</em></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 18 Sep 2019 18:06:27 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:31;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:75:"HeroPress: Life Stacks Up –  From A Small Town Boy To A Geek Entrepreneur";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:56:"https://heropress.com/?post_type=heropress-essays&p=2963";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:192:"https://heropress.com/essays/life-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur/#utm_source=rss&utm_medium=rss&utm_campaign=life-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:10001:"<img width="960" height="480" src="https://s20094.pcdn.co/wp-content/uploads/2019/09/091719-min-1024x512.jpg" class="attachment-large size-large wp-post-image" alt="Pull Quote: For me, WordPress means freedom, self expression, and adaptability." /><p>A six year old is in deep thought. His gaze stuck on an intricate structure made with wooden sticks – a large rectangular box in the centre, a tall stick, some knitting threads running up and down. All this is arranged in a shelf in a common terrace wall of two middle class Indian homes.</p>
<p>The boy is holding what seems like a paper cup telephone – two paper cups with a thread running between. Soon, he smiles and throws one paper cup over the wall to the other side. His counterpart on the other side picks up his side of the &#8220;telephone&#8221; and they start talking.</p>
<p>&#8220;I made a TV using the sticks. I&#8217;m now going to set up a power line&#8230;&#8221;</p>
<p>&#8220;Awesome, I&#8217;ll be there after my homework!&#8221;</p>
<p>Aha! Now it makes sense. The kids are pretend-playing, and this one in particular is into science and model making. He has made an elaborate television model with limited resources.</p>
<p>Fast forward six years, and the boy is writing programs on a school computer. Couple years later he’s making model rockets and planes.</p>
<p>Fast forward another six years, and the boy is sitting with Bill Gates, being one of the eight national winners in a competition.</p>
<p>He goes on to launch India&#8217;s first electronic magazine, a web solutions business, local language versions of Linux and OpenOffice, a content management system, few books and a string of businesses that have made millions of dollars.</p>
<p>And he fondly remembers meeting Matt Mullenweg and Chris Lema at WordCamp San Francisco in 2014. His web agency business had gone bust around 2011, and his WordPress plugins business was picking up. Those meetings strengthened his conviction for WordPress and he doubled down on his plugins. Today his team takes care of 200,000+ active users across two dozen of their plugins – both free and premium.</p>
<p>That small town boy is me.</p>
<h3>Who I Am</h3>
<p>My name is Nirav Mehta. I live in Mumbai, and I&#8217;m super passionate content, commerce and contribution. I run three businesses – two in WordPress (<a href="https://www.storeapps.org/">StoreApps.org</a> – where we solve problems for growing WooCommerce stores,<a href="http://icegram.com/"> Icegram.com</a> – where creators find tools to inspire, engage and convert their audiences), and one SaaS business (<a href="https://www.putler.com/">Putler</a> – meaningful analytics for e-commerce).</p>
<p>I have done some or other form of writing for over two decades. I&#8217;ve done open source for my whole life and used Drupal and Joomla earlier. As a matter of fact, I created a content management system using PHP back in 2000. But I liked the simplicity and community of WordPress. So when I wanted to start two blogs in 2006, I jumped on to WordPress.</p>
<blockquote><p>And it was amazing. WordPress simplified a whole lot of things, allowed customization and had extensive plugin ecosystem.</p></blockquote>
<p>I continued blogging and tinkering with WordPress. WordPress kept growing, and when I was looking for &#8220;the next big thing&#8221; around 2011, I figured I can bet on e-commerce with WordPress.</p>
<p>There was no WooCommerce back then, and we built an extension to WPeC – an e-commerce plugin that was popular at that time. Smart Manager – the plugin we built – allowed managing products, orders and customers using an easy spreadsheet like interface. It quickly became popular. When WooCommerce came along, we ported our WPeC plugins to WooCommerce, and also became an official third-party developers with our Smart Coupons plugin. StoreApps – our WooCommerce plugins business continues to be our top business today.</p>
<p>WordPress has changed my life. For me, WordPress means freedom, self expression and adaptability.</p>
<h3>Where I Came From</h3>
<p>I&#8217;m from a small town, I am not an engineer, I didn&#8217;t do an MBA. I don&#8217;t have a godfather. But I&#8217;ve always wanted to contribute to a larger community, I&#8217;m a stickler for elegant solutions that solve practical problems and I&#8217;m ready to delay gratification. I believe grit and humility are essential. I&#8217;m a curious lifetime learner. I&#8217;ve realized that money is important, it&#8217;s a great resource. But I&#8217;ve also learnt that the joy of seeing someone benefit from your work far surpasses anything else.</p>
<p>WordPress fits perfectly here. It gives me a platform to reach out to the whole world. It&#8217;s built on community and greater good. There are lots of opportunities and entry barriers are low.</p>
<h3>What WordPress Has Given Me</h3>
<p>WordPress allowed me to exercise my creative skills and easily build solutions on top of the core platform. I am not a great marketer, and WordPress and WooCommerce enabled me to build strong businesses by tapping into their distribution prowess. WordPress was easy to learn, so when we found people with the right mindset, they became productive soon.</p>
<p>Icegram – our onsite and email marketing plugins business – is a clear example of the power of WordPress. Icegram Engage shows popups, header-footer bars and many other types of visitor engagement and call to action messages. Maintaining such a solution on a large scale would require huge server costs and sys-op efforts. We could avoid all that because we could keep most of the functionality in WordPress. It also provided a cleaner and much better user experience than typical SaaS solutions. When I wrote the initial code for it, I wanted to keep the frontend logic in JavaScript – and of course, WordPress allowed doing that. Eventually, it was also easy to migrate to a hybrid model – where complex functions are performed on our servers and rest remains in WordPress.</p>
<blockquote><p>WordPress has given me great friends. I&#8217;ve met so many talented people online and at WordCamps! Me and my WordPress friends have done amazing adventures together! And the circle keeps expanding. You will find amazing people in WordPress!</p></blockquote>
<p>When you look at my life, and if important events were plotted as a chart, you won&#8217;t see a straight curve. It&#8217;s a bundle of long lull-times with gyrating ups and downs in between. I studied behavior patterns, data modelling and visualization for Putler – our multi-system analytics solution for online businesses. I also get to see numbers from many other businesses. I wanted to analyze how businesses work. What causes success.</p>
<p>And one big, common takeaway &#8211; in both business and life &#8211; is that results are non-linear. There is no single cause to any result.</p>
<h3>Back To You</h3>
<p>It all starts simple. What you do today, is shaped by something you did earlier, and will shape something else you&#8217;ll do in the future.</p>
<p>Every little act of courage, every little getting out of your comfort zone, every new thing you learn, every setback, every little success&#8230; It all keeps building who you are.</p>
<p>You see, life stacks up!</p>
<p>Do not despair, do not lose faith. Series of actions produce a result, and you have the ability to act.</p>
<p>So stay on!</p>
<div class="rtsocial-container rtsocial-container-align-right rtsocial-horizontal"><div class="rtsocial-twitter-horizontal"><div class="rtsocial-twitter-horizontal-button"><a title="Tweet: Life Stacks Up &#8211;  From A Small Town Boy To A Geek Entrepreneur" class="rtsocial-twitter-button" href="https://twitter.com/share?text=Life%20Stacks%20Up%20%2D%20%20From%20A%20Small%20Town%20Boy%20To%20A%20Geek%20Entrepreneur&via=heropress&url=https%3A%2F%2Fheropress.com%2Fessays%2Flife-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-fb-horizontal fb-light"><div class="rtsocial-fb-horizontal-button"><a title="Like: Life Stacks Up &#8211;  From A Small Town Boy To A Geek Entrepreneur" class="rtsocial-fb-button rtsocial-fb-like-light" href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fheropress.com%2Fessays%2Flife-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur%2F" rel="nofollow" target="_blank"></a></div></div><div class="rtsocial-linkedin-horizontal"><div class="rtsocial-linkedin-horizontal-button"><a class="rtsocial-linkedin-button" href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fheropress.com%2Fessays%2Flife-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur%2F&title=Life+Stacks+Up+%26%238211%3B++From+A+Small+Town+Boy+To+A+Geek+Entrepreneur" rel="nofollow" target="_blank" title="Share: Life Stacks Up &#8211;  From A Small Town Boy To A Geek Entrepreneur"></a></div></div><div class="rtsocial-pinterest-horizontal"><div class="rtsocial-pinterest-horizontal-button"><a class="rtsocial-pinterest-button" href="https://pinterest.com/pin/create/button/?url=https://heropress.com/essays/life-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur/&media=https://heropress.com/wp-content/uploads/2019/09/091719-min-150x150.jpg&description=Life Stacks Up -  From A Small Town Boy To A Geek Entrepreneur" rel="nofollow" target="_blank" title="Pin: Life Stacks Up &#8211;  From A Small Town Boy To A Geek Entrepreneur"></a></div></div><a rel="nofollow" class="perma-link" href="https://heropress.com/essays/life-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur/" title="Life Stacks Up &#8211;  From A Small Town Boy To A Geek Entrepreneur"></a></div><p>The post <a rel="nofollow" href="https://heropress.com/essays/life-stacks-up-from-a-small-town-boy-to-a-geek-entrepreneur/">Life Stacks Up &#8211;  From A Small Town Boy To A Geek Entrepreneur</a> appeared first on <a rel="nofollow" href="https://heropress.com">HeroPress</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 18 Sep 2019 03:00:17 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Nirav Mehta";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:32;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:75:"WPTavern: GPL Author Richard Stallman Resigns from Free Software Foundation";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93898";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:86:"https://wptavern.com/gpl-author-richard-stallman-resigns-from-free-software-foundation";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:8749:"<p><a href="https://stallman.org/" rel="noopener noreferrer" target="_blank">Richard Stallman</a>, free software movement activist and originator of the &#8220;copyleft&#8221; concept, has <a href="https://www.fsf.org/news/richard-m-stallman-resigns" rel="noopener noreferrer" target="_blank">resigned from his position as director of the board and president of the Free Software Foundation (FSF)</a>, which he established in 1985. This resignation comes on the heels of his resignation from MIT’s Computer Science and Artificial Intelligence Lab (CSAIL) after remarks he made regarding a 17-year old victim of sex trafficker Jeffrey Epstein, characterizing her as seeming “entirely willing.”</p>
<p>Stallman <a href="https://www.stallman.org/archives/2019-jul-oct.html#14_September_2019_(Statements_about_Epstein)" rel="noopener noreferrer" target="_blank">blamed media coverage</a> for misinterpreting his comments as a defense of Epstein two days before announcing his <a href="https://www.stallman.org/archives/2019-jul-oct.html#16_September_2019_(Resignation)" rel="noopener noreferrer" target="_blank">resignation from MIT</a> on his personal blog:</p>
<blockquote><p>To the MIT community, I am resigning effective immediately from my position in CSAIL at MIT. I am doing this due to pressure on MIT and me over a series of misunderstandings and mischaracterizations.</p></blockquote>
<p>The remarks in question were sent on a department-wide CSAIL mailing list in response to an MIT student email calling for a protest against Jeffrey Epstein’s donation to the school. Selam Jie Gano, the MIT graduate who exposed Stallman&#8217;s comments in a <a href="https://medium.com/@selamie/remove-richard-stallman-fec6ec210794" rel="noopener noreferrer" target="_blank">post</a> on Medium, also leaked the full thread to <a href="https://www.vice.com/en_us/article/9ke3ke/famed-computer-scientist-richard-stallman-described-epstein-victims-as-entirely-willing" rel="noopener noreferrer" target="_blank">Vice</a>.</p>
<p>In the email thread, which was also circulated to undergraduate students, Stallman became pedantic about the definition of assault and the use of the term &#8216;rape&#8217; after a student pointed out the laws of the location and the victim&#8217;s age:</p>
<blockquote><p>I think it is morally absurd to define “rape” in a way that depends on minor details such as which country it was in or whether the victim was 18 years old or 17.</p></blockquote>
<p>These comments caused media organizations to dig up old posts from Stallman&#8217;s blog where he <a href="https://stallman.org/archives/2012-jul-oct.html#15_September_2012_(Censorship_of_child_pornography)" rel="noopener noreferrer" target="_blank">demands an end to the censorship of &#8220;child pornography&#8221;</a> and <a href="https://stallman.org/archives/2006-may-aug.html?fbclid=IwAR09M66FT8o8cYpAZCkW07KCWwNtXWJgvAz02H5K6_iwrGyWIhY24OuJ5Js#05%20June%202006%20(Dutch%20paedophiles%20form%20political%20party)" rel="noopener noreferrer" target="_blank">says</a> he is &#8220;skeptical of the claim that voluntarily pedophilia harms children.&#8221;</p>
<p>Why Stallman felt it necessary to lend his controversial views to public comments on rape, assault, and child sex trafficking on a public mailing list is a mystery, but he has a long history of being outspoken when it comes to politics and civil liberties.</p>
<p>This particular incident seemed to be the straw that broke the camel&#8217;s back, unleashing a flood of outrage from the the free software and broader tech communities who demanded Stallman&#8217;s removal from the FSF. Critics cited two decades of behaviors and statements that many have found to be disturbing and offensive. The Geek Feminism Wiki maintains a <a href="https://geekfeminism.wikia.org/wiki/Richard_Stallman" rel="noopener noreferrer" target="_blank">catalog</a> that includes some of these references.</p>
<p>&#8220;The free software community looks the other way while they build their empires on licenses that sustain Stallman&#8217;s power,&#8221; Software engineer  and founder of RailsBridge Sarah Mei <a href="https://twitter.com/sarahmei/status/1172283772428906496" rel="noopener noreferrer" target="_blank">said</a> in a Tweetstorm calling on the FSF to remove Stallman from his positions of influence.</p>
<p>&#8220;Your refusal to part ways with him &#8211; despite well-known incidents that have pushed women and others out of free software for decades &#8211; might have been ok 10 years ago. Maybe even two years ago. It&#8217;s not ok now.&#8221;</p>
<p>The Software Freedom Conservancy also issued a <a href="https://sfconservancy.org/news/2019/sep/16/rms-does-not-speak-for-us/" rel="noopener noreferrer" target="_blank">statement</a> calling for Stallman&#8217;s removal, titled &#8220;Richard Stallman Does Not and Cannot Speak for the Free Software Movement:&#8221;</p>
<blockquote><p>When considered with other reprehensible comments he has published over the years, these incidents form a pattern of behavior that is incompatible with the goals of the free software movement. We call for Stallman to step down from positions of leadership in our movement.</p>
<p>We reject any association with an individual whose words and actions subvert these goals. We look forward to seeing the FSF&#8217;s action in this matter and want to underscore that allowing Stallman to continue to hold a leadership position would be an unacceptable compromise. Most importantly, we cannot support anyone, directly or indirectly, who condones the endangerment of vulnerable people by rationalizing any part of predator behavior.</p></blockquote>
<p>In a <a href="https://twitter.com/sarahmei/status/817378684638068736" rel="noopener noreferrer" target="_blank">2017 Twitter thread</a>, Mei shared some context on her perspective of how Stallman&#8217;s influence has had a ripple effect of damage throughout the free software and open source communities:</p>
<blockquote><p>In the 90s, Richard Stallman&#8217;s attitude towards women alienated me (and many others) from any interest in or support for &#8220;free software.&#8221; Viewing software through the Richard Stallman/GNU/&#8221;free as in freedom&#8221; lens would have run our industry into the ground. But it was the only alternative to proprietary software for ~20 years. So lots of folks worked on it despite finding Stallman problematic. This was the period when women largely declined to be part of computing, despite having pretty reasonable representation through the 80s.</p>
<p>In the early 2000s, &#8220;open source&#8221; was a breath of fresh air. All of the usefulness! None of the built-in arrogance, privilege, or misogyny! But just because it wasn&#8217;t built in doesn&#8217;t mean it disappeared. As folks converted, the behaviors normalized by Stallman and others followed. Our drive now for diversity/inclusion wasn&#8217;t even conceivable until we discarded GNU, Stallman, and &#8220;free software&#8221; in favor of &#8220;open source.&#8221; It&#8217;s not an accident that the communities who still, today, embrace that outdated philosophy are the least diverse and the most hostile.</p></blockquote>
<p>Stallman is the author of the GPL, which he wrote with the help of lawyers. For the most part, the free software community is able to objectively separate the license from the man who conceived it. The FSF&#8217;s sister organization in Europe <a href="https://fsfe.org/news/2019/news-20190917-01.html" rel="noopener noreferrer" target="_blank">welcomed Stallman&#8217;s resignation</a>, echoing the sentiments of many who value his contributions but are unwilling to support his public representation of the organization:</p>
<blockquote><p>On 16 September, one of our independent sister organizations, the US-based Free Software Foundation (FSF), announced the resignation of Richard M. Stallman as its president. While we recognize Stallman&#8217;s role in founding the Free Software movement, we welcome the decision.</p></blockquote>
<p>The FSF has the opportunity to redefine itself after the resignation of its founder and supporters are hopeful that the free software movement can find a better way forward without Stallman&#8217;s influence.</p>
<p>&#8220;I believe in Free Software and have published most of my work open source under LGPL/GPL/AGPL (notably including Cydia, Cycript, WinterBoard, ldid, and now my work on Orchid),&#8221; software engineer Jay Freeman <a href="https://twitter.com/saurik/status/1173810527853670402" rel="noopener noreferrer" target="_blank">said</a>. &#8220;I&#8217;m glad to see Richard Stallman leave, and hope this starts a new era for the Free Software Foundation.&#8221;</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 17 Sep 2019 23:58:55 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:33;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:63:"WPTavern: Yoast to Reward Contributors with the Yoast Care Fund";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93862";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:74:"https://wptavern.com/yoast-to-reward-contributors-with-the-yoast-care-fund";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4284:"<p>Yoast, a company primarily known for its popular Yoast SEO plugin, announced a new program earlier this month called <a href="https://yoast.com/yoast-care/">Yoast Care</a>.  The project aims to reward volunteers in the WordPress community.  &#8220;Care&#8221; stands for &#8220;Community Appreciation REwards.&#8221;</p>



<p>Thousands of people contribute to WordPress.  Some choose to contribute code.  Others answer dozens of support questions every day in the forums.  Many spend their free time actively running or helping with the various <a href="https://make.wordpress.org">Make WordPress</a> teams.  Many people do it because they love WordPress or have found a home within the community, but not all of them get paid for their work toward the open-source platform.</p>



<p>Contributing untold hours is often a thankless job.  The many millions of WordPress users will never know about the time and effort these volunteers pour into the project.  They are in the trenches doing the work that keeps WordPress running.  They don&#8217;t wear capes, but they are the unsung heroes of the community.</p>



<p>&#8220;We visit a lot of WordCamps and know a lot of people. We notice that some people have a hard time making a living from just their WordPress-work,&#8221; said Marieke van de Rakt, CEO of Yoast. &#8220;We wanted to do something for these people. We can&#8217;t hire them all.&#8221;</p>



<p>Yoast Care will grant $500 to around 50 volunteers each year.  The company has already set aside $25,000 for the first year and has an open application process for nominating contributors.</p>



<p>&#8220;We&#8217;re aiming for people that do not get paid for their work on WordPress,&#8221; said van de Rakt, founder of Yoast Academy and CEO of Yoast. &#8220;It has to be a person that is active in a Make WordPress team.&#8221;</p>



<p>Some within the community have noted that Yoast is a for-profit company and that such programs are more about PR.  At the heart of the discussion is whether the fund will obscure the longstanding issue of how to properly fund contributors to open-source projects ($500 only goes so far).  Others have pointed out that the program is a step in the right direction and could push other companies to follow suit.</p>



<p>The fund could help those who need it most.  It may help a volunteer replace their worn-out laptop, cover a freelancer during a low-income month, or boost someone in need of cash flow for their new WordPress project.</p>



<p>The application process is open for anyone to fill out, but applicants can&#8217;t throw their own names into the hat.  The form for applying also asks for up to 3 references to confirm the nominee&#8217;s work. The team has already received many applications.</p>



<p>Taco Verdonscho is leading the Yoast Care project for the company&#8217;s community team.  Such a program is no small task to run, and the rewards will be spread out through the year.  </p>



<p>&#8220;It is a lot of work,&#8221; said van de Rakt.  &#8220;They&#8217;ve really thought it through (what the demands are), so I think it&#8217;s rather easy to decide whether or not the application can be rewarded. But, still after that, we need to do an interview and make it happen financially. So there are a lot of people involved.&#8221;</p>



<p>Outside of a cash reward, Yoast will feature winners in a blog post that highlights his or her contributions to WordPress.</p>



<p>Yoast is not new to community outreach and funding those in need.  Last year, the team <a href="https://wptavern.com/yoast-launches-fund-to-increase-speaker-diversity-at-tech-conferences">launched the Yoast Diversity Fund</a>.  The program was created to help minorities and other underrepresented groups afford to speak at conferences.  It covers travel, accommodations, childcare, and other costs.  The Diversity Fund is still <a href="https://yoast.com/yoast-diversity-fund/apply/">accepting applications</a>.</p>



<p>Most within the inner WordPress community know at least one or two people who deserve some appreciation for all the work they do.  If you know someone who fits this description, you can nominate them via the <a href="https://yoast.com/community/yoast-care-fund/">Yoast Care application page</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 17 Sep 2019 17:51:05 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:34;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:46:"BuddyPress: BuddyPress 5.0.0 Release Candidate";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://buddypress.org/?p=307797";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:66:"https://buddypress.org/2019/09/buddypress-5-0-0-release-candidate/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2517:"<p>Hello!</p>



<p>The first release candidate for BuddyPress 5.0.0 is now available for a last round of testing!</p>



<p>This is an important milestone as we progress toward the BuddyPress 5.0.0 final release date. &#8220;Release Candidate&#8221; means that we think the new version is ready for release, but with more than 200,000 active installs, hundreds of BuddyPress plugins and Thousands of WordPress themes, it’s possible something was missed. BuddPress 5.0.0 is&nbsp;scheduled to be released&nbsp;on&nbsp;<strong>Monday, September 30</strong>, but we need&nbsp;<em>your</em>&nbsp;help to get there—if you haven’t tried 5.0.0 yet, <strong>now is the time!</strong> </p>



<div class="wp-block-button aligncenter is-style-squared"><a class="wp-block-button__link has-background" href="https://downloads.wordpress.org/plugin/buddypress.5.0.0-RC1.zip">Download and test the 5.0.0-RC1</a></div>



<div class="wp-block-spacer"></div>



<p><em>PS: as usual you alternatively get a copy via our Subversion repository.</em></p>



<p>A detailed changelog will be part of our official release note, but&nbsp;you can get a quick overview by reading the post about the&nbsp;<a href="https://buddypress.org/2019/08/buddypress-5-0-0-beta1/">5.0.0 Beta1</a>&nbsp;release.</p>



<div class="wp-block-image"><img src="https://plugins.svn.wordpress.org/buddypress/assets/icon.svg" alt="" width="33" height="33" /></div>



<h2>Plugin and Theme Developers </h2>



<p>Please test your plugins and themes against BuddyPress 5.0.0. If you find compatibility problems, please be sure to post to this specific <a href="https://buddypress.org/support/topic/buddypress-5-0-0-release-candidate/">support topic</a> so we can figure those out before the final release.</p>



<h2>Polyglots, we need you!</h2>



<p>Do you speak a language other than English?&nbsp;<a href="https://translate.wordpress.org/projects/wp-plugins/buddypress/">Help us translate BuddyPress into many languages!</a>&nbsp;This release also marks the&nbsp;<a href="https://make.wordpress.org/polyglots/handbook/glossary/#string-freeze">string freeze</a>&nbsp;point of the 5.0.0 release schedule.</p>



<p><strong>If you think you&#8217;ve found a bug</strong>, please let us know reporting it on&nbsp;<a href="https://buddypress.org/support">the support forums</a>&nbsp;and/or&nbsp;on&nbsp;<a href="https://buddypress.trac.wordpress.org/">our development tracker</a>.</p>



<p>Thanks in advance for giving the release candidate a test drive!</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 16 Sep 2019 23:15:07 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:5:"imath";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:35;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:94:"WPTavern: New Attock WordPress Meetup Empowers Pakistani Women Freelancers and Business Owners";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93733";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:105:"https://wptavern.com/new-attock-wordpress-meetup-empowers-pakistani-women-freelancers-and-business-owners";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:10042:"<p><a href="https://2019.lahore.wordcamp.org/" rel="noopener noreferrer" target="_blank">WordCamp Lahore</a> is getting rebooted on November 30 &#8211; December 1, at the National University of Computer and Emerging Sciences. The first edition of the event was planned for 2016 but was derailed by local disagreements and ultimately <a href="https://wptavern.com/karachi-to-host-first-wordcamp-in-pakistan-following-cancellation-of-wordcamp-lahore" rel="noopener noreferrer" target="_blank">canceled</a>. For the past three years organizers have worked to strengthen their local meetup groups and follow suggestions from the WordPress Foundation before reapplying.</p>
<p>WordCamp Lahore lead organizer <a href="https://www.meshpros.com/" rel="noopener noreferrer" target="_blank">Muhammad Kashif</a> said his team is expecting more than 350 attendees, with the majority of them coming from the local community. The <a href="https://www.meetup.com/WordPress-Lahore/" rel="noopener noreferrer" target="_blank">Lahore WordPress meetup group</a> is thriving and has grown to 4,383 members who regularly meet in various groups across the area.</p>
<p>&#8220;We still have attendees from other cities and in closing I encourage them to start local chapters and offer any help they need,&#8221; Kashif said. He works as a Master Trainer for a government training program called <a href="https://www.erozgaar.pitb.gov.pk/" rel="noopener noreferrer" target="_blank">eRozgaar</a> that trains unemployed youth in more than <a href="https://www.erozgaar.pitb.gov.pk/#erb08" rel="noopener noreferrer" target="_blank">25 centers</a> across Punjab. The program was launched by the Punjab Government in March 2017 and <a href="https://www.erozgaar.pitb.gov.pk/#erb06" rel="noopener noreferrer" target="_blank">WordPress is a major part of the eRozgaar curriculum</a>.</p>
<p>&#8220;I manage the WordPress curriculum and in a recent update I have included community building, which is about Meetups and WordCamp events,&#8221; Kashif said. He reports that eRozgaar trainees have collectively earned more than $1 million US dollars to date after going through the 3.5 month-program.</p>
<p>&#8220;The program is making a big impact, especially for women who can&#8217;t go out for jobs,&#8221; Kashif said. &#8220;They are making good money from freelancing and WordPress is playing a big part in that.&#8221;</p>
<p>Kashif attributes some of Pakistan&#8217;s current economic challenges to a rapidly growing population and poor planning from past governments. The job opportunities have not grown as fast as the population, which was one of the reasons the government created the eRozgaar training program.</p>
<p>As the result of having WordPress in the curriculum that is used across so many areas of Punjab, new meetups are starting to pop up in other cities. <a href="https://www.salmanoreen.com/" rel="noopener noreferrer" target="_blank">Salma Noreen</a>, one of the program&#8217;s trainers who Kashif worked with, started <a href="https://www.meetup.com/Attock-WordPress-Meetup/" rel="noopener noreferrer" target="_blank">a meetup in Attock</a> and is the first female WordPress meetup organizer in Pakistan. She plans to apply for WordCamp Attock in 2020.</p>
<p><a href="https://i2.wp.com/wptavern.com/wp-content/uploads/2019/09/this-too.jpg?ssl=1"><img /></a>Salma Noreen, first female WordPress meetup organizer in Pakistan</p>
<p>&#8220;Attock is a small city but love for WordPress is big and I am so happy to see other women participating in the WordPress community,&#8221; Noreen said.</p>
<p>&#8220;Every year, 1000+ people graduate in this city after 16 years of education. But we don&#8217;t have many jobs in this small city, so a small number of people who are backed by financially good families can move to other big cities like Lahore and Karachi for jobs and learning opportunities. The remaining people&#8217;s future is always a question mark.</p>
<p>&#8220;Being a woman, I was more worried about women, as we have a cultural barrier that most women cannot get permission to relocate or go out of home for a regular 9 to 5 job. Introducing them to WordPress and then guiding them on how to find online clients has helped many to earn a decent living from home.&#8221;</p>
<p>For the past 10 years, Noreen worked primarily as a freelancer and has completed more than 3,500 projects in web development. She is mentoring new WordPress users in her city to become successful freelancers and online store owners using resources like Udemy courses, YouTube, public blogs, and the WordPress codex.</p>
<p>&#8220;I am still struggling but yes I am confident that one day everyone will be making enough from home,&#8221; she said.</p>
<p>The Attock WordPress meetup is averaging 60-70 attendees in recent months, where members share their knowledge, experience, and best practices. For many of those attending, the meetup group was their first introduction to the software. Noreen describes the local community as &#8220;crazy about WordPress&#8221; and eager to have their own WordCamp in 2020.</p>
<p><a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/group-photo-2.jpg?ssl=1"><img /></a>Attock WordPress meetup</p>
<p>One meetup member, Uroosa Samman, is a graduate of Environmental Science studies but is now working with WordPress after attending the monthly meetups.</p>
<p>&#8220;I didn&#8217;t have any WordPress or coding background during my education,&#8221; Samman said. &#8220;It was difficult for me to learn tech things. The meetups were very helpful and motivational for me, so I decided to start working in tech. Since the events were organized by a female organizer, it was comfortable for us to attend. I am able to provide my services as a freelancer and I am developing my own WordPress e-commerce store. If I get stuck in any issue related to WordPress, I immediately contact this community and they are always ready to help each other.&#8221;</p>
<p><a href="https://i0.wp.com/wptavern.com/wp-content/uploads/2019/09/FB_IMG_1564500844418.jpg?ssl=1"><img /></a>Women attending a recent Attock WordPress Meetup</p>
<p>Shahryar Amin, a recent college graduate, was uncertain about his future until he discovered WordPress through Noreen&#8217;s support and the Attock meetup:</p>
<blockquote><p>Just a few months ago, I was completely devastated financially. Pakistan is going through turbulent time, and its economy has never been performing this low. So, fresh graduates like me had their dreams absolutely shattered, when after four months of rigorous effort, we were unable to find a source of livelihood. That was truly a testing time.</p>
<p>Moving back to my small city, I was not much hopeful for the future. My hometown, Attock, is a remote city with limited opportunities to advance one’s career. But ironically, that turned out to be a wrong assumption. I moved back to my city after nearly four years, and it had some phenomenal changes which I couldn’t resist noticing. The most<br />
impressive of them was WordPress meetups. </p>
<p>That was the first time I became familiar with the platform. I was curious, and that got me to the very first meetup organized by Ms. Salma Noreen. She is a remarkable soul, and I can’t thank her more for putting up such effort for an ignored city like ours. I learned my basics from these meetups, and as my interest become my passion, I was spending more and more hours on learning WordPress through the internet. I had no programming skills, but fortunately one don’t need any to setup a website on WordPress. </p>
<p>As I delved further into it, I discovered some very useful plugins, like Elementor, Divi and Visual composer, and at that moment I decided to become a designer using WordPress. I won’t say that I have become an expert in WordPress, but I am paying back the community by sharing my knowledge as a speaker at the very last meetup on July 30. Also, I have been working as a freelance designer on various online platforms, and WordPress expertise has truly been rewarding me financially.</p></blockquote>
<p><a href="https://i2.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-16-at-3.01.35-PM.png?ssl=1"><img /></a>Shahryar Amin speaking at a recent Attock WordPress meetup</p>
<p>Attock resident Sania Nisar has a degree in software engineering and used to spend several days creating a simple website before discovering WordPress. She has never had any formal training through paid courses but is now working as a WordPress professional with the knowledge she gained from attending WordPress meetups and online resources.</p>
<p>&#8220;WordPress Attock is playing a vital role in empowering women in my vicinity,&#8221; Nisar said. &#8220;It is difficult for the women of Attock to travel to big cities like Islamabad to gain knowledge. However, WordPress Attock has efficiently solved this problem by providing an engaging learning platform for the women of this city. Today I am a successful freelancer and a WordPress professional.&#8221;</p>
<p><a href="https://i2.wp.com/wptavern.com/wp-content/uploads/2019/09/meetup1.jpg?ssl=1"><img /></a></p>
<p>Noreen said her team hopes to bring 15 to 20 people from Attock to attend WordCamp Lahore. The trip is expensive and takes approximately seven hours so not many will be able to make it but there will be other camps in the region that are nearer for Attock residents.</p>
<p>Last year a WordCamp was held in Islamabad, and the second WordCamp Karachi took place in August 2019. WordCamp Lahore will be Pakistan&#8217;s fourth WordCamp, held in the country&#8217;s second-most populous city. Attendees will have the opportunity to meet and connect with WordPress professionals and enthusiasts from across Pakistan. Speaker applications are open and sessions will be held in Urdu and English. Regular admission is Rs 1,700.00 and <a href="https://2019.lahore.wordcamp.org/tickets/" rel="noopener noreferrer" target="_blank">tickets are now on sale</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 16 Sep 2019 22:04:38 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:36;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"WPTavern: Justin Tadlock Joins WP Tavern";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93843";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:51:"https://wptavern.com/justin-tadlock-joins-wp-tavern";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4839:"<p>The kid scampered ahead of his classmates.  He wanted to be one of the first to set foot in the building, but he stopped as he got to the first step.  He looked up to count the floors.  One, two, three…it wasn&#8217;t <em>The Times</em>.  He was awestruck all the same.  <em>The Birmingham News</em>.</p>



<p>Truth be told, the kid was a man grown in 2007, but he was still a kid at heart.  His graduation was fast approaching.  He&#8217;d soon leave The Plains, his home of Auburn, Alabama.  He needed to get some experience at smaller, local papers before landing a job at <em>The Birmingham News</em>.  He didn&#8217;t know it while standing on those steps, but before the day was out, he&#8217;d return home and start filling out applications for every small paper across the state.</p>



<p>His only real experience with newspapers outside of university was reading about J. Jonah Jameson and Peter Parker or following the exploits of Clark Kent and Lois Lane.  He had this 1950s-esque picture in his mind of a cigar-smoking reporter wearing a cheap suit and fedora while pounding the keys of his typewriter.  He&#8217;d be working toward a Pulitzer-winning story.  Other reporters would sprint by his desk with their next big lead.  The editor would yell orders across the room as everyone rushed to beat the deadline.</p>



<p>Reality didn&#8217;t exactly match the picture in his mind.  The kid knew it wouldn&#8217;t.  On those steps of the recently-built news office, he&#8217;d need to let go of the fantasy.  He breathed deep and stepped forward at the instruction of his professor.</p>



<p>Field trips were a rare occurrence in college, but this professor was different.  His classroom was merely a part of the learning process.  Journalism was more than memorizing rules and writing a few papers each semester.  The only way to understand journalism was to step foot into an office and observe.</p>



<p>That&#8217;s the day the kid&#8217;s life changed forever.  He knew what he wanted to do after he graduated.  He wanted to work at a small-town newspaper by day and pen the great Southern American novel by night.</p>



<p>The roads people travel are rarely the direct route they set out on.</p>



<p>A few months later, the kid was living in Atlanta, Georgia, and traveling to Home Depots across half the state as a vendor.  During his summer in the Peach State, he got an opportunity to visit the CNN Center.  It was a thing of beauty.  With renewed vigor, he put in more applications at small papers.  Either no one was hiring or he didn&#8217;t have the experience.</p>



<p>He applied for other jobs.  Once he interviewed to be a used-car salesman.  However, he landed a job teaching in South Korea.  While imparting the few things he picked up about the English language to young minds, he began building his reputation in the WordPress community.  Before leaving the country, he&#8217;d bootstrapped his own WordPress theme shop in 2008.</p>



<p>After 11 years, the kid stumbled upon an opportunity to join the staff at WP Tavern, a chance to combine his passion for WordPress and writing.</p>



<p>Now a new chapter in his life begins.</p>



<h2>Allow Me to Introduce Myself</h2>



<p>My name is Justin Tadlock.  I&#8217;m the new staff writer for WP Tavern.  It&#8217;s my hope that I can bring a different perspective and produce many engaging stories for you to read long into the future.</p>



<p>You&#8217;ve probably used at least a few lines of my code to run your web site.  I&#8217;ve contributed to WordPress in some form or fashion since I started using the software in 2005.  I formerly ran Theme Hybrid, which was one of the earliest and longest-running theme shops in the WordPress ecosystem.  I also co-authored <em>Professional WordPress Plugin Development</em>.</p>



<p>Over the coming weeks and months, I plan to get to know more of you within the WordPress community.  I&#8217;ve been an avid reader of WP Tavern since its inception.  It&#8217;s always held a special place in my heart, and I want it to be an environment where everyone feels welcome to discuss all the things happening in the WordPress world.</p>



<p>It will take me a bit to get a feel for the new writing position and find my voice.  I may have a few hit-or-miss stories out of the gates, but I&#8217;m always open to feedback and criticism from our readers.  Ultimately, it&#8217;s my job to serve you the stories that you enjoy reading.</p>



<p>I&#8217;m stoked for the opportunity to get to know more of you.  I want to help you share your stories.  I want the community to know the people behind this platform that so many of us rely on in our personal and professional lives.</p>



<p>I hope I exceed your expectations for quality reporting and feature stories for WordPress.  Stay tuned.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 16 Sep 2019 16:02:22 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Justin Tadlock";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:37;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:112:"WPTavern: Adam Jacob Advocates for Building Healthy OSS Communities in “The War for the Soul of Open Source”";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93730";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:117:"https://wptavern.com/adam-jacob-advocates-for-building-healthy-oss-communities-in-the-war-for-the-soul-of-open-source";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3704:"<p><a href="https://i2.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-13-at-1.40.07-PM.png?ssl=1"><img /></a></p>
<p>Chef co-founder and former CTO <a href="https://twitter.com/adamhjk" rel="noopener noreferrer" target="_blank">Adam Jacob</a> gave a short presentation at O&#8217;Reilly Open Source Software Conference (OSCON) 2019 titled &#8220;<a href="https://www.youtube.com/watch?v=8q5o-4pnxDQ" rel="noopener noreferrer" target="_blank">The War for the Soul of Open Source</a>.&#8221; In his search for meaning in open source software today, Jacob confronts the notion of open source business models.</p>
<p>&#8220;We often talk about open source business models,&#8221; he said. &#8220;There isn&#8217;t an open source business model. That&#8217;s not a thing and the reason is open source is a channel. Open source is a way that you, in a business sense, get the software out to the people, the people use the software, and then they become a channel, which [companies] eventually try to turn into money.&#8221;</p>
<p>Companies often employ open source as a strategy to drive adoption, only to have mega corporations scoop up the software and corner the market. Jacob addressed the friction open source communities have with companies that use OSS to make billions of dollars per year running it as a service, citing Amazon Web Services (AWS) as a prime example.</p>
<p>Amid conflicts like these, it&#8217;s a challenge to find meaning in OSS via business. Jacob looked to organizations like the Free Software Foundation and Open Source Initiative but could not get on board with the methods and outcomes they pursue through their efforts.</p>
<p>He concluded that what is left is the people at the heart of OSS, who improbably come together with an overlapping sense of shared values and purpose.</p>
<p>&#8220;Each of us are a weird different shape, struggling to find our path and yet open source software gives us this ability to gather together around this resource that we turn from being scarce to being infinite,&#8221; he said.</p>
<p>&#8220;Look at your own desires, look at your own needs and the things you want in your own life. Then go out and find and build and steward communities with other people who share those values and who will embrace your purpose, and sustain each other. Because that is the true soul of open source.&#8221;</p>
<p>In December 2018, Jacob launched the <a href="https://sfosc.org/" rel="noopener noreferrer" target="_blank">Sustainable Free and Open Source Communities</a> (SFOSC) project to advocate for these ideas. Instead of focusing on protecting revenue models of OSS companies, the project&#8217;s contributors work together to collaborate on writing core principles, social contracts, and business models as guidelines for healthy OSS communities.</p>
<p>&#8220;I believe we need to start talking about Open Source not in terms of licensing models, or business models (though those things matter): instead, we should be talking about wether or not we are building sustainable communities,&#8221; Jacob said in a <a href="https://medium.com/sustainable-free-and-open-source-communities/we-need-sustainable-free-and-open-source-communities-edf92723d619" rel="noopener noreferrer" target="_blank">post</a> introducing the project. &#8220;What brings us together, as people, in this common effort around the software? What rights do we hold true for each other? What rights are we willing to trade in order to see more of the software in the world, through the investment of capital?&#8221;</p>
<p>Check out Jacob&#8217;s presentation below for a 13-minute condensed version of the inspiration behind the SFOSC project.</p>
<p></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 13 Sep 2019 21:31:17 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:38;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:80:"WPTavern: Local Lightning Now in Public Beta with Major Performance Improvements";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93700";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:91:"https://wptavern.com/local-lightning-now-in-public-beta-with-major-performance-improvements";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4418:"<p>Flywheel put its new Local Lightning app into <a href="https://localbyflywheel.com/community/t/local-lightning-public-beta/14075" rel="noopener noreferrer" target="_blank">public beta</a> this week. The app is an improved version of the company&#8217;s local WordPress development tool, formerly known as Pressmatic before Flywheel <a href="https://wptavern.com/flywheel-acquires-wordpress-local-development-tool-pressmatic" rel="noopener noreferrer" target="_blank">acquired</a> it from Clay Griffiths and renamed it to &#8220;<a href="https://localbyflywheel.com/" rel="noopener noreferrer" target="_blank">Local by Flywheel</a>.&#8221;</p>
<p>Since its acquisition in 2016, Local has gained many fans, particularly developers who had grown tired of debugging local development environments. Local has proven to be a reliable app that saves many wasted hours. It also allows developers to quickly switch between PHP and MySQL versions as well as Apache and nginx.</p>
<p>Overall, Local users enjoy the app&#8217;s features but have found performance to be <a href="https://localbyflywheel.com/community/t/speed-issues-on-local-machine/360" rel="noopener noreferrer" target="_blank">a continual issue</a>. Users reported having one-minute page loads in the backend, on small, uncomplicated sites. These speed issues began to drive users away from Local to other products, as many found that working locally was slower than creating a test site with their hosting companies.</p>
<p>Even booting up the app can be abysmally slow, as demonstrated in a video Griffiths shared announcing the Local Lightning beta:</p>
<p><a href="https://cloudup.com/cBmHR2MODnR"><img src="https://i1.wp.com/cldup.com/K7hEKiIfjA.gif?resize=600%2C338&ssl=1" alt="Local lightning comparison" width="600" height="338" /></a></p>
<p>&#8220;To chart a more reliable and powerful path forward, we’re rebuilding Local’s core architecture and moving away from virtualization in favor of native, system-level software to run WordPress locally,&#8221; Griffiths said.</p>
<p>The new Local Lightning app reduces system requirements and the minimum disk space requirement has decreased by more than 18GB. Griffiths also reports that the download size for Local is 50% less than before.</p>
<p>Local is also now available on Linux, thanks to the new architecture in the updated app. Linux availability has been a frequent request since Local was originally launched.</p>
<blockquote class="twitter-tweet">
<p lang="en" dir="ltr"><img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f916.png" alt="🤖" class="wp-smiley" /> Hey, guess what? LOCAL IS NOW AVAILABLE FOR LINUX!</p>
<p><img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f427.png" alt="🐧" class="wp-smiley" /> We’ve built it into the public beta of Local Lightning. </p>
<p><img src="https://s.w.org/images/core/emoji/12.0.0-1/72x72/26a1.png" alt="⚡" class="wp-smiley" /> Get it here: <a href="https://t.co/7LOgLYWeLc">https://t.co/7LOgLYWeLc</a> <a href="https://t.co/N6klXH6BKl">pic.twitter.com/N6klXH6BKl</a></p>
<p>&mdash; Local by Flywheel (@LocalbyFlywheel) <a href="https://twitter.com/LocalbyFlywheel/status/1172175142883139584?ref_src=twsrc%5Etfw">September 12, 2019</a></p></blockquote>
<p></p>
<p>Local Lightning and Local by Flywheel have been developed as two separate applications in order to allow users to migrate their sites at their own pace. They can also run alongside each other and are named differently. &#8220;Local by Flywheel&#8221; is now the old version and the new Local Lightning app is simply known as &#8220;Local.&#8221; Users can migrate their sites by exporting from the old version and dragging them into the new app for automatic import. The beta is lacking some features that were previously available, including hot-swapping development environments and support for Varnish HTTP Cache. Griffiths&#8217; team is working on restoring feature parity with the original app.</p>
<p>When asked in the product&#8217;s forums about the general release date, a Flywheel representative said that it &#8220;will definitely be by the end of the year.&#8221; Users who want to join the public beta can download the latest version for Mac, Windows, or Linux from the <a href="https://localbyflywheel.com/community/t/local-lightning-public-beta/14075" rel="noopener noreferrer" target="_blank">Local Lightning announcement post</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 13 Sep 2019 16:17:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:39;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:90:"WPTavern: WPHindi Plugin Instantly Converts Text from English to Hindi in the Block Editor";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=92869";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:106:"https://wptavern.com/wphindi-plugin-instantly-converts-text-from-english-to-hindi-live-in-the-block-editor";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3607:"<p>Hindi is one of the world&#8217;s top languages, with more than 520 million native and non-native speakers, and is the <a href="https://thelogicalindian.com/news/hindi-fastest-growing-language/" rel="noopener noreferrer" target="_blank">fastest growing language in India</a>. In a region where English is also commonly spoken, many Hindi publishers have the unique requirement of being able to switch back and forth between the two languages when writing articles.</p>
<p><a href="https://wordpress.org/plugins/wphindi/" rel="noopener noreferrer" target="_blank">WPHindi</a> is a new plugin that was developed to help WordPress users stay inside the editor instead of copying and pasting from third-party tools.</p>
<p>It offers a block that instantly converts text from English to Hindi as users are typing. It also works with the Classic Editor. The block supports intelligent auto-suggestions that make it easy to correct typos. Users can quickly enable/disable WPHindi with one click when switching between languages. It also works seamlessly with the rich text options inside the editor.</p>
<p></p>
<p>The plugin was originally created by the team at <a href="https://www.zozuk.com" rel="noopener noreferrer" target="_blank">Zozuk</a>, a WordPress support and maintenance service, as a custom solution for a client.</p>
<p>&#8220;They are a big publisher in the Hindi content space and with a lot of writers,&#8221; Zozuk representative Aditya Rathore said. &#8220;Using tools outside the WordPress dashboard was becoming a huge productivity killer for them.&#8221;</p>
<p>After this request Zozuk contacted 54 Hindi content publishers who are WordPress users and found that 39 of them were facing the same problem.</p>
<p>&#8220;It was more than enough to realize the scale of the problem and we decided to make the plugin available for free to every webmaster including our partners,&#8221; Rathore said. &#8220;The scale of the problem and how important it was to solve it, proved to be our element of inspiration for WPHindi.&#8221;</p>
<p>Data from a <a href="https://www.theatlas.com/charts/r1q1GxrJW" rel="noopener noreferrer" target="_blank">KPMG-Google study</a> indicates that 201 million Hindi users, which comprise 38% of the Indian internet user base, will be online by 2021.</p>
<div class="atlas-chart"><img src="https://i2.wp.com/s3.us-east-1.amazonaws.com/qz-production-atlas-assets/charts/atlas_r1q1GxrJW.png?w=627&ssl=1" /></div>
<p></p>
<p>WPHindi is currently the only solution of its kind in the WordPress space. An older plugin called <a href="https://www.monusoft.com/products/wordpress-plugin" rel="noopener noreferrer" target="_blank">Hindi Writer</a> performed a similar function for converting text in the comment box but it was not available from the official plugin directory and has not been updated since 2006.</p>
<p>Hindi publishers have also used tools like <a href="http://google.com/intl/hi/inputtools/try/" rel="noopener noreferrer" target="_blank">google.com/intl/hi/inputtools/try/</a> and <a href="http://easyhindityping.com" rel="noopener noreferrer" target="_blank">easyhindityping.com</a> but these are not tailored to WordPress and have to be open in a separate window. WPHindi provides text conversion directly in the editor, speeding up writers&#8217; workflow.</p>
<p>Rathore said Zozuk plans to monetize the plugin in the future with an add-on that will allow users to comment in Hindi on the frontend. The plugin is currently in development. The team is is also working on releasing similar plugins for other languages like Bengali and Marathi.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 12 Sep 2019 18:52:14 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:40;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:102:"WPTavern: Richard Best Releases Free Audio and Ebook: “A Practical Guide to WordPress and the GPL”";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93615";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:106:"https://wptavern.com/richard-best-releases-free-audio-and-ebook-a-practical-guide-to-wordpress-and-the-gpl";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2768:"<p>If you&#8217;re itching to go deeper into the legal aspects of navigating WordPress&#8217; relationship to the GPL license, <a href="https://richardbestlaw.com/" rel="noopener noreferrer" target="_blank">Richard Best</a> has recently made his ebook (and the audio version) called &#8220;<a href="https://richardbestlaw.com/2019/09/10/a-practical-guide-to-wordpress-and-the-gpl/" rel="noopener noreferrer" target="_blank">A Practical Guide to WordPress and the GPL</a>&#8221; available for free. Best, a technology and public lawyer based in New Zealand, had previously sold the book with other products as part of a <a href="http://wpandlegalstuff.com/a-practical-guide-to-wordpress-and-the-gpl-get-it/" rel="noopener noreferrer" target="_blank">business package</a> that is still available for purchase. After receiving feedback on his most recent post titled &#8220;<a href="http://wpandlegalstuff.com/taking-gpld-code-proprietary/" rel="noopener noreferrer" target="_blank">Taking GPL’d code proprietary</a>,&#8221; he found that the issues addressed in the book are still relevant and decided to release it for free.</p>
<p>The first two sections provide a brief history of WordPress, its adoption of the GPL, and a summary of the license. These sections are a bit dry, but Chapter 3 is where it gets more interesting, particularly for theme and plugin developers who have questions about licensing GPL-derivatives. Best explores the practical application of the GPL in common business scenarios:</p>
<ul>
<li>If I modify the core WordPress software or a GPL’d theme or plugin, must I release the source code of the modified versions(s) to the public?</li>
<li>I’m a theme/plugin developer. I’ve put huge effort into writing my theme/plugin and I’m going to release it under the GPL but I want to make sure that everyone who receives my theme or plugin, even if from someone else, is obliged to pay me a licensing fee or notify me that they have it. Can I do that?</li>
<li>I’ve purchased some fully GPL’d themes or plugins from a commercial theme or plugin provider. May I sell those themes or plugins from my own website for my own benefit or publish those themes or plugins on my own website and give them away for free?</li>
</ul>
<p>Subsequent chapters cover controversies surrounding &#8220;GPL non-compliant&#8221; sales models, applications of copyright law, GPL compatibility with other licenses, and trademarks. Both the audio and the PDF ebook are <a href="https://richardbestlaw.com/2019/09/10/a-practical-guide-to-wordpress-and-the-gpl/" rel="noopener noreferrer" target="_blank">available for download</a> on Best&#8217;s website. The text of the book is licensed under the Creative Commons Attribution 4.0 International License.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 11 Sep 2019 22:24:33 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:41;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:121:"WPTavern: Google Announces New Ways to Identify Nofollow Links, Progress on Related Gutenberg Ticket Is Currently Stalled";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93592";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:131:"https://wptavern.com/google-announces-new-ways-to-identify-nofollow-links-progress-on-related-gutenberg-ticket-is-currently-stalled";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4775:"<p>This week Google <a href="https://webmasters.googleblog.com/2019/09/evolving-nofollow-new-ways-to-identify.html?fbclid=IwAR2pPFs1IaEfRNj7w_xPO1TLw-1RQSWwUeRiKjYZBwSENf7A1f426mDlz0I" rel="noopener noreferrer" target="_blank">announced</a> changes to the 15-year old <a href="https://support.google.com/webmasters/answer/96569" rel="noopener noreferrer" target="_blank">nofollow attribute</a> that was previously recommended for identifying links related to advertising, sponsors, or content for which users are not intending to pass along ranking credit. The nofollow attribute is no longer a catchall for these types of instances, as Google has introduced two new rel values (&#8220;sponsored&#8221; and &#8220;ugc&#8221;) to further specify the purpose of the link to the search engine:</p>
<ul>
<li><strong>rel=&#8221;sponsored&#8221;</strong>:</li>
<p> Use the sponsored attribute to identify links on your site that were created as part of advertisements, sponsorships or other compensation agreements.</p>
<li><strong>rel=&#8221;ugc&#8221;</strong>:</li>
<p> UGC stands for User Generated Content, and the ugc attribute value is recommended for links within user generated content, such as comments and forum posts.</p>
<li><strong>rel=&#8221;nofollow&#8221;</strong>:</li>
<p> Use this attribute for cases where you want to link to a page but don’t want to imply any type of endorsement, including passing along ranking credit to another page.
</ul>
<p>Google is also shifting to using a &#8220;hint model&#8221; for interpreting the new link attributes:</p>
<blockquote><p>When nofollow was introduced, Google would not count any link marked this way as a signal to use within our search algorithms. This has now changed. All the link attributes &#8212; sponsored, UGC and nofollow &#8212; are treated as hints about which links to consider or exclude within Search. We’ll use these hints &#8212; along with other signals &#8212; as a way to better understand how to appropriately analyze and use links within our systems.</p></blockquote>
<p>The announcement includes a few notable instructions regarding usage. Although all the new link attributes are working today as hints for ranking purposes, there is no need to change existing links. For sponsored links, Google recommends switching over to using rel=”sponsored” if or when it is convenient. Users can also specify multiple rel values (e.g. rel=&#8221;ugc sponsored&#8221;). Google plans to use the hints for crawling and indexing purposes beginning March 1, 2020.</p>
<p>The new ways to identify nofollow links impacts not only how users create links in their sites but also <a href="https://wordpress.org/plugins/search/nofollow/" rel="noopener noreferrer" target="_blank">plugins that add the nofollow attribute</a> sitewide or other otherwise. Plugin authors will want to reevaluate the options provided in their products.</p>
<p>Progress on the relevant <a href="https://github.com/WordPress/gutenberg/pull/16609" rel="noopener noreferrer" target="_blank">Gutenberg PR for adding a nofollow option</a> has stalled and is not currently listed for any upcoming milestones. Last week Gutenberg designer Mark Uraine expressed hesitation on adding this feature to the plugin.</p>
<p>&#8220;I’m hesitant on this one,&#8221; Uraine <a href="https://github.com/WordPress/gutenberg/pull/16609#issuecomment-527921959" rel="noopener noreferrer" target="_blank">said</a>. &#8220;I think it’s been a long-standing discussion and there are reasons behind not including this option in the Classic Editor.</p>
<p>&#8220;How does it adhere to the WordPress 80/20 rule? We’re looking to implement this as an option (not a decision)… so will 80% of WP users benefit from it?&#8221;</p>
<p>Gutenberg users are continuing to advocate on the ticket for the necessity of nofollow link options.</p>
<p>&#8220;Now, with Gutenberg, you can only add a nofollow by switching to the HTML version and manually add the nofollow attribute,&#8221; Andreas de Rosi said. &#8220;It&#8217;s a big pain. I don&#8217;t know how to best implement it (I am not a programer), but this is an important feature the Gutenberg editor should have.&#8221;</p>
<p>Paal Joachim Romdahl <a href="https://github.com/WordPress/gutenberg/pull/16609#issuecomment-528838567" rel="noopener noreferrer" target="_blank">commented</a> on the ticket, requesting a simple way for plugins to extend the link dialog box if the Gutenberg team decides to reject the PR for adding nofollow options.</p>
<p>More general <a href="https://github.com/WordPress/gutenberg/pull/13190" rel="noopener noreferrer" target="_blank">discussion</a> regarding how to implement link settings extensibility is open in a separate ticket on the Gutenberg repository.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 11 Sep 2019 18:41:31 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:42;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:109:"WPTavern: Kioken Blocks: The New Street Fighter-Inspired Block Collection that Is Taking Aim at Page Builders";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93524";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:119:"https://wptavern.com/kioken-blocks-the-new-street-fighter-inspired-block-collection-that-is-taking-aim-at-page-builders";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:7918:"<p>With the proliferation of block collection plugins over the past year, <a href="https://wordpress.org/plugins/kioken-blocks/" rel="noopener noreferrer" target="_blank">Kioken Blocks</a> is a relatively unknown newcomer that you may have missed. Compared to competitors with thousands of users like <a href="https://wordpress.org/plugins/coblocks/" rel="noopener noreferrer" target="_blank">CoBlocks</a> (30K+), <a href="https://wordpress.org/plugins/atomic-blocks/" rel="noopener noreferrer" target="_blank">Atomic Blocks</a> (20K+), <a href="https://wordpress.org/plugins/stackable-ultimate-gutenberg-blocks/" rel="noopener noreferrer" target="_blank">Stackable</a> (10K+), and <a href="https://wordpress.org/plugins/ultimate-addons-for-gutenberg/" rel="noopener noreferrer" target="_blank">Ultimate Addons for Gutenberg</a> (100K+), Kioken is a small fish in a big pond of page builder utilities.</p>
<p>You might have seen Kioken Blocks in action recently without knowing it, if you checked out Matias Ventura&#8217;s demo introducing the concept of “<a href="https://wptavern.com/gutenberg-team-explores-the-future-of-full-site-editing-with-new-prototype" rel="noopener noreferrer" target="_blank">block areas</a>.&#8221; The plugin was first released two months ago but is already starting to differentiate itself with some innovative design features, block templates, and layouts. Its name was inspired by the <a href="https://streetfighter.com/" rel="noopener noreferrer" target="_blank">Street Fighter</a> arcade game and major releases are named for different character moves.</p>
<p>Kioken&#8217;s most recent release includes a new Vertical Text setting that allows users to rotate paragraphs and headings for a special effect in more complex layouts.</p>
<p></p>
<p>Inside the block editor, users can flip the vertical text rotation, adjust the alignment, add margins, dropcaps, and apply other standard text settings to the selection.</p>
<p><a href="https://i0.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-10-at-1.49.07-PM.png?ssl=1"><img /></a></p>
<p>Kioken currently includes 17 blocks, all created with an emphasis on providing an aesthetic base design that will seamlessly fit into a user&#8217;s theme, with further customization options for each block. The blocks are not cookie cutter repeats of other collections but rather offer their own distinct styles and features.</p>
<p>For example, the <a href="https://kiokengutenberg.com/blocks/kinetic-posts-block/" rel="noopener noreferrer" target="_blank">Kinetic Posts</a> block allows users to list blog posts, including custom post types, inside a grid, columns/list, or slider with multiple different layout options. Users can run custom queries, such as ordering randomly, or by name, popularity, date, and by post type with custom taxonomy queries.</p>
<p><a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-10-at-2.06.08-PM.png?ssl=1"><img /></a></p>
<p>Kioken Blocks creator Onur Oztaskiran said he focuses on adding features and blocks that are not commonly available already. This includes some under the hoods usability features, such as custom block appenders, lighter block highlighter on block selection on dark backgrounds, and block settings change indicators in the sidebar.</p>
<p>&#8220;I try to add blocks that people don’t have access to yet,&#8221; Oztaskiran said. &#8220;So I don’t spend my time on creating accordions or team blocks but rather add things that enrich your content building process in the same fashion premium page building tools do (Kinetic Wrapper block, Animator and Vertical Text extensions are some of these).&#8221;</p>
<h3>Kioken Blocks Aims to Provide a Faster, Simpler Alternative to Complex Page Builder Plugins</h3>
<p>Oztaskiran has a design background, having previously worked as the design lead at Udemy, Noon.com, and Qordoba, but he taught himself how to build blocks in order to push the limits of WordPress&#8217; page building capabilities.</p>
<p>&#8220;Kioken Blocks started out as a personal hobby to learn Gutenberg development and test out if I can do something with GB that would replace mine and everyone else’s page building workflow with WordPress, using only Gutenberg by extending it.</p>
<p>&#8220;I am a designer and not so great developer. I’ve mostly built Kioken Blocks following Gutenberg resources on the web and GitHub, most of the time by learning from the Gutenberg GitHub repo.&#8221;</p>
<p>Oztaskiran&#8217;s personal site, <a href="https://monofactor.com/" rel="noopener noreferrer" target="_blank">monofactor.com</a>, was built with nothing but Gutenberg and Kioken Blocks, including the fancy animations reminiscent of Themeforest products, along with the layout. The site is a good example of how the block editor and a few custom blocks can enable users to create beautiful, complex layouts without having to use a heavy, over-engineered page builder plugin.</p>
<p>&#8220;I took a leap of faith in Gutenberg when it was first released and started developing for it since I&#8217;m also a user and hate many things about page builder plugins,&#8221; Oztaskiran said. &#8220;I love to hate Gutenberg as well, but right now I can&#8217;t stop using it.&#8221;</p>
<p>Oztaskiran used page builder plugins in the past and even created extensions for some of them, but ultimately his frustrations inspired him to go all in on Gutenberg block development.</p>
<p>&#8220;With page builders, what took me away from them most was the MBs of resources they add to my sites, and the complexity of content editing in the editor, the long learning curve for some of them, and most importantly you need to be a &#8216;pro&#8217; to create complex layouts and engaging, rich content,&#8221; Oztaskiran said.</p>
<p>As a result of these frustrations, he decided to focus on speed and usability in Kioken Blocks. Oztaskiran said he is satisfied to have developed a product that allows users to create animated, complex layouts in minutes, much faster than he was able to do in other platforms. Kioken&#8217;s predefined block presets allow users to insert elements like background hero sections, product intros, sliding testimonials, and other page elements, making it easy to quickly design a site. These types of elements further blur the line between predefined block templates and themes.</p>
<p><a href="https://i2.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-10-at-4.57.56-PM.png?ssl=1"><img /></a></p>
<p>&#8220;What amazes me with Gutenberg is you only need a lightweight unbloated GB compatible theme and nothing else,&#8221; Oztaskira said. &#8220;You can create amazing things.&#8221;</p>
<p>He is currently maintaining the plugin by himself without a team but the project is very time consuming. He sells commercial block templates through the plugin&#8217;s upgrade page and the user base is growing, so is considering making some partnerships in the future. Kioken Blocks only has approximately 100+ active installs at the moment, but Oztaskiran reports that his conversion rate is about 6-7% on selling Pro licenses, which include priority support and commercial block templates and layouts.</p>
<p>Despite identifying himself as just &#8220;a designer and a crappy developer,&#8221; Oztaskiran&#8217;s investment in learning Gutenberg block development is starting to pay off.</p>
<p>&#8220;You don’t need to be a pro dev to understand the logic, and with having an average JS knowledge you can get on board to GB development in a short time,&#8221; he said.&#8221;</p>
<p>&#8220;I indeed had ups and downs with Gutenberg, and Kioken Blocks aims to cover for those &#8216;downs.&#8217; I’ve been trying to build a tool for the editor so that some day you will only need Gutenberg and no other page building tools to create engaging and beautiful content.&#8221;</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 10 Sep 2019 22:53:51 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:43;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"BuddyPress: BuddyPress 5.0.0-beta2";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://buddypress.org/?p=307715";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:54:"https://buddypress.org/2019/09/buddypress-5-0-0-beta2/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2888:"<p>Hello BuddyPress contributors!</p>



<p><strong>5.0.0-beta2</strong> is available for testing, you can&nbsp;<a href="https://downloads.wordpress.org/plugin/buddypress.5.0.0-beta2.zip">download it here</a>&nbsp;or get a copy via our Subversion repository. This is really important for us to have your feedback and testing help.</p>



<p>Since <a href="https://buddypress.org/2019/08/buddypress-5-0-0-beta1/">5.0.0-beta1</a>:</p>



<ul><li>We&#8217;ve brought some improvements to string i18n into the BP REST API code.</li><li>We&#8217;ve also improved the JavaScript function we are making available in this release to ease your clients BP REST API Requests.</li></ul>



<h2>5.0.0 final release is approaching!</h2>



<p>The <strong>Release Candidate (RC) is scheduled on September 16</strong>: at this time BuddyPress 5.0.0 will be in a string freeze. It means we won&#8217;t change i18n strings anymore for this release to leave enough time to our beloved polyglot contributors to <a href="https://translate.wordpress.org/projects/wp-plugins/buddypress/">translate BuddyPress</a> into their native languages. If you&#8217;re a good english writer or copywriter you can still help us to <a href="https://buddypress.trac.wordpress.org/ticket/8132#comment:5">polish the text</a> we plan to use to inform about the 5.0.0 new features.</p>



<p>If you are still using our Legacy Template Pack and think it&#8217;s important to include a Twenty Nineteen companion stylesheet into this release, <strong>September 16 is also the deadline to make it happen</strong>. Please test, contribute and improve the patch attached to this <a href="https://buddypress.trac.wordpress.org/ticket/8103">ticket</a>.</p>



<div class="wp-block-image"><img src="https://plugins.svn.wordpress.org/buddypress/assets/icon.svg" alt="" width="33" height="33" /></div>



<p>Let&#8217;s use the coming days to make sure your BuddyPress plugins or your theme or your specific WordPress configuration are ready for BuddyPress 5.0.0 : <strong>we need you to help us help you</strong>: please <a href="https://downloads.wordpress.org/plugin/buddypress.5.0.0-beta2.zip">download and test 5.0.0-beta2</a>!</p>



<div class="wp-block-button aligncenter is-style-squared"><a class="wp-block-button__link has-background" href="https://downloads.wordpress.org/plugin/buddypress.5.0.0-beta2.zip">Download and test 5.0.0-beta2</a></div>



<div class="wp-block-spacer"></div>



<p>5.0.0 is almost ready (Targeted release date is <a href="https://bpdevel.wordpress.com/2019/09/06/bp-dev-chat-summary-september-4/">September 30, 2019</a>), but please do not run this Beta 2 release in a production environment just yet. Let us know of any issues you find in <a href="https://buddypress.org/support">the support forums</a> and/or on <a href="https://buddypress.trac.wordpress.org/">our development tracker</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 10 Sep 2019 17:45:07 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:5:"imath";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:44;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:102:"WPTavern: Creative Commons Releases New WordPress Plugin for Attributing Content with Gutenberg Blocks";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93506";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:113:"https://wptavern.com/creative-commons-releases-new-wordpress-plugin-for-attributing-content-with-gutenberg-blocks";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2484:"<p><a href="https://creativecommons.org/" rel="noopener noreferrer" target="_blank">Creative Commons</a> has released an <a href="https://wordpress.org/plugins/creative-commons/" rel="noopener noreferrer" target="_blank">official WordPress plugin</a> for attributing and licensing content. It is an updated and revamped version of the organization&#8217;s <a href="https://github.com/tarmot/wp-cc-plugin" rel="noopener noreferrer" target="_blank">WPLicense</a> plugin. It is also loosely based on an old plugin called <a href="https://wordpress.org/plugins/license/" rel="noopener noreferrer" target="_blank">License</a>, which seems to have been abandoned after not receiving any updates for six years.</p>
<p>The new Creative Commons plugin is an attribution tool that is compatible with the block editor. It comes with eight different blocks for licensing any post, page, image, or other type of media.</p>
<p><a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/creative-commons-gutenberg-blocks.png?ssl=1"><img /></a></p>
<p>The block settings allow the user to specify the Attribution text, add additional text after the license, and customize the block&#8217;s background and text colors.</p>
<p><a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/creative-commons-block-customization.png?ssl=1"><img /></a></p>
<p>The plugin also retains several features from the older versions, including the ability to set a default license and display it as a widget or in the footer. Users can license their entire sites or license some posts, pages, or images differently on a per-content basis using the CC Gutenberg blocks. It is also multisite compatible, where network admins can license the entire network with a default license or allow site admins to choose their own. License information can be displayed with &#8220;One Click Attribution&#8221; for images.</p>
<p>Software developer <a href="https://ahmadbilal.dev/" rel="noopener noreferrer" target="_blank">Ahmad Bilal</a> worked on the Creative Commons plugin with help from a mentor as part of his Google Summer of Code project. This update is long overdue, as the older version of the plugin was not compatible with newer versions of WordPress beyond 3.8.1. The new plugin is compatible with the latest version of WordPress (5.2.2) and is now <a href="https://wordpress.org/plugins/creative-commons/" rel="noopener noreferrer" target="_blank">available in the official plugin directory</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 10 Sep 2019 03:58:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:45;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:84:"WPTavern: Gutenberg Team Explores the Future of Full-Site Editing with New Prototype";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93512";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:95:"https://wptavern.com/gutenberg-team-explores-the-future-of-full-site-editing-with-new-prototype";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:6839:"<p>From its inception, the block editor was always intended to be more than just an editor for the main content area. Gutenberg phase 2 brings the block editor to other parts of the site, including widgets, menus, and other aspects of site customization. Matias Ventura, one of the lead engineers on the project, has offered a glimpse of the team&#8217;s vision for how the block editor will tackle full-site editing with an intriguing new <a href="https://make.wordpress.org/core/2019/09/05/defining-content-block-areas/" rel="noopener noreferrer" target="_blank">prototype</a>.</p>
<p>Ventura shared a video demo, introducing the concept of &#8220;block areas,&#8221; which he said would include headers, footers, sidebars, and any other meaningful template part outside of the post content that contains blocks. In the example below, every element on the page is made of blocks and can be directly manipulated by the user.</p>
<p></p>
<p>The prototype wasn&#8217;t necessarily created to prescribe a specific implementation but rather shows some of the possibilities of how block areas could be organized within the page. Each block area is saved separately and any of the template parts can have a distinct name. Ventura suggested they might be saved as individual posts in an internal custom post type, which can be isolated and edited individually or within the scope of the whole page. This would allow for different view modes and possibly even a design mode with a grid overlay:</p>
<p><a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-09-at-9.25.03-AM.png?ssl=1"><img /></a></p>
<p>The prototype demonstrates the possibility of drilling down into the individual blocks nested within theme templates and post content. This offers users a better understanding of the page structure and allows them to easily navigate nested blocks.</p>
<p><a href="https://i0.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-09-at-9.21.27-AM.png?ssl=1"><img /></a></p>
<p>Ventura&#8217;s writeup is somewhat technical and implementation details are still being actively discussed across several tickets on GitHub, but initial community reactions to the prototype have been positive overall.</p>
<h3>A Closer Look at How Block Areas Could Replace the Customizer</h3>
<p>With WordPress closing in on the one year anniversary of having the block editor in core, the interface presented in the block areas prototype seems immediately more familiar than the Customizer. Full-site editing in the Gutenberg era will fundamentally change how users approach their site designs. The block editor stands to unify customization and content interfaces that were previously unable to make the jump into full-on frontend editing.</p>
<p>&#8220;It&#8217;s too early to say for sure, but in a world where everything is a block, there isn&#8217;t much need for the Customizer&#8217;s current interface where the preview is disconnected from the controls in a separate window,&#8221; Customizer component maintainer Weston Ruter said. &#8220;If theme templates are built entirely with blocks which support direct manipulation, then it&#8217;s essentially a frontend editing paradigm.&#8221;</p>
<p>Ruter, who was instrumental in architecting a great deal of the Customizer, said the current interface, which splits the design and controls into separate windows, was necessary because so many of the controls required full-page reloads. The split interface ensures that the controls don&#8217;t constantly disappear while the page reloads to display the changes.</p>
<p>&#8220;The better Customizer integrations are the live &#8216;postMessage&#8217; updating-controls which didn&#8217;t require reloads (e.g. color picker),&#8221; Ruter said. &#8220;More recently the &#8216;selective refresh&#8217; capability also facilitated themes and plugins to re-generate partial templates without having to reload the entire page. In theory, those capabilities did allow for <a href="https://github.com/xwp/wp-customize-inline-editing" rel="noopener noreferrer" target="_blank">inline editing</a> without having to reload the page.&#8221;</p>
<p>While the Customizer gave users more control over their site designs, the component has always struggled to provide powerful controls and live refreshing in the same interface with a limited amount of page real estate. Ruter highlighted a few of the advantages of making the block editor the primary vehicle for customization in WordPress.</p>
<p>&#8220;Blocks bring a common interface to be able to do such inline editing for any part of the page, not just special areas in the Customizer preview that get the extra user interface goodies added,&#8221; he said. &#8220;And so with this common block interface with direct manipulation paradigm, there&#8217;s no need for a separate controls panel and there is no need to do full page reloads to do preview changes. So there would be no need for the current Customizer interface.&#8221;</p>
<p>Although much of the Customizer is likely to become obsolete in the new era of Gutenberg-powered full-site editing, the <a href="https://make.wordpress.org/core/2016/10/12/customize-changesets-technical-design-decisions/" rel="noopener noreferrer" target="_blank">Customizer changeset</a> is one key concept that Ruter thinks could be preserved. This is the code that enables users to <a href="https://make.wordpress.org/core/2017/11/03/new-features-and-enhancements-with-customizer-changesets-in-4-9/" rel="noopener noreferrer" target="_blank">stage and schedule sitewide design changes</a>.</p>
<p>&#8220;This is independent of the current Customizer interface and relates to the underlying data model of WordPress,&#8221; he said. &#8220;If changes made to Gutenberg blocks were put into such a changeset prior to being published, then the changes could be previewed across a site before going live. The need for this has not been so apparent until now because the changes have been scoped to post content. But once the block data being manipulated across various entities of a site, then it becomes important to have some place to stage those changes prior to going live.&#8221;</p>
<p>Plugin and theme developers will want to monitor the conversations surrounding the implementation of block areas for full site editing. When this prototype become a reality, it will have a significant impact on themes and plugins that are currently extending the Customizer. Many product developers will need to re-architect their solutions to be better suited to site customization that is powered by the block editor. Ventura lists all the relevant GitHub issues in his post <a href="https://make.wordpress.org/core/2019/09/05/defining-content-block-areas/" rel="noopener noreferrer" target="_blank">introducing content-block areas</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 09 Sep 2019 19:48:07 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:46;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:83:"WPTavern: First Look at Twenty Twenty: New WordPress Default Theme based on Chaplin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93474";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:94:"https://wptavern.com/first-look-at-twenty-twenty-new-wordpress-default-theme-based-on-chaplain";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3576:"<p>Anders Norén unveiled the <a href="https://make.wordpress.org/core/2019/09/06/introducing-twenty-twenty/" rel="noopener noreferrer" target="_blank">designs for the new Twenty Twenty theme</a> today. As speculated earlier this week, <a href="https://wptavern.com/anders-noren-to-design-twenty-twenty-default-theme-shipping-in-wordpress-5-3" rel="noopener noreferrer" target="_blank">WordPress will repurpose Noren&#8217;s Chaplin theme</a> in order to expedite shipping the new default theme on the constrained <a href="https://make.wordpress.org/core/5-3/" rel="noopener noreferrer" target="_blank">5.3 release timeline</a>.</p>
<p>Although the new default theme will be based on Chaplin, it will not retain the same style.</p>
<p>&#8220;Using an existing theme as a base will help us get going on development faster,&#8221; Norén said. &#8220;Very little of the style of Chaplin will remain though, so it will still look and feel very much like its own thing.&#8221;</p>
<p>The screenshots he shared in the announcement look like a completely different theme. With just a few color and typography changes, along with a centered column for content, Twenty Twenty has its own distinct character.</p>
<p>
<a href="https://wptavern.com/first-look-at-twenty-twenty-new-wordpress-default-theme-based-on-chaplain/twenty-twenty-sub-page"><img width="150" height="150" src="https://i2.wp.com/wptavern.com/wp-content/uploads/2019/09/twenty-twenty-sub-page.jpg?resize=150%2C150&ssl=1" class="attachment-thumbnail size-thumbnail" alt="" /></a>
<a href="https://wptavern.com/first-look-at-twenty-twenty-new-wordpress-default-theme-based-on-chaplain/twenty-twenty-single-post"><img width="150" height="150" src="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/twenty-twenty-single-post.jpg?resize=150%2C150&ssl=1" class="attachment-thumbnail size-thumbnail" alt="" /></a>
</p>
<p>Norén said he designed it to be a flexible, all-purpose theme suitable for businesses, organizations, and blogs, depending on the combination of blocks.</p>
<p>&#8220;The promise of the block editor is to give users the freedom to design and structure their sites as they see fit,&#8221; he said in the <a href="https://make.wordpress.org/core/2019/09/06/introducing-twenty-twenty/" rel="noopener noreferrer" target="_blank">post</a> introducing Twenty Twenty. &#8220;The responsibility of a theme is to empower users to create their inspired vision by making the end result look as good, and work as well, as the user intended.&#8221;</p>
<p>The theme uses <a href="https://rsms.me/inter/" rel="noopener noreferrer" target="_blank">Inter</a> for the typeface, selected for its legibility and bold personality when used in headings. It also comes in a Variable Font version, which Norén said will be a first for WordPress default themes. The benefits are that it reduces the number of requests and decreases the page size.</p>
<p>Those who are adventurous can <a href="https://github.com/WordPress/twentytwenty" rel="noopener noreferrer" target="_blank">download Twenty Twenty right now from GitHub</a> and play around with the theme in its current state. Once it is stable, Norén and his team plan to merge it into core and continue development on Trac. There will be weekly meetings held in the #core-themes Slack channel for those who want to contribute to the design and development. The first one is scheduled for <a href="https://www.timeanddate.com/worldclock/fixedtime.html?iso=20190909T1900" rel="noopener noreferrer" target="_blank">Monday, September 9, 2019, 02:00 PM CDT</a>.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 07 Sep 2019 03:16:12 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:47;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:62:"WPTavern: Google Releases Native Lazyload Plugin for WordPress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93443";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:73:"https://wptavern.com/google-releases-native-lazyload-plugin-for-wordpress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5493:"<p><a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/Screen-Shot-2019-09-06-at-10.26.38-AM.png?ssl=1"><img /></a></p>
<p>The most recent release of Chrome (76) added a new “loading” attribute that supports <a href="https://wptavern.com/chrome-76-adds-native-lazy-loading-wordpress-contributors-continue-discussion-regarding-core-support" rel="noopener noreferrer" target="_blank">native lazy loading in the browser</a>. An implementation for WordPress core is still under discussion. In the meantime, plugins that enable this for WordPress sites are starting to pop up, and Google has just released one of its own.</p>
<p><a href="https://wordpress.org/plugins/native-lazyload/" rel="noopener noreferrer" target="_blank">Native Lazyload</a> was created by Google engineer Felix Arntz and the team behind the official AMP and PWA plugins for WordPress. It lazy loads images and iframes with the new loading attribute for browsers that support it. It also includes a fallback mechanism for browsers that do not yet support it, but this can be disabled with a filter. The plugin has no settings &#8211; users simply activate it and it works.</p>
<p>In a <a href="https://felix-arntz.me/blog/native-lazy-loading-wordpress/" rel="noopener noreferrer" target="_blank">post</a> introducing the new plugin, Arntz explains why current lazy loading options, which require custom JavaScript, are not always good for performance:</p>
<blockquote><p>Lazy-loading has for a long time not been a switch you can just toggle to make it work. It was not a browser feature, so it typically required loading and running custom JavaScript logic to make it work. Unfortunately, JavaScript itself is an expensive resource, so lazy-loading as it’s been done so far might in certain cases actually have a negative impact on performance (e.g. if a page doesn’t contain any images or only contains a single image that’s immediately visible). Furthermore, if a user had disabled JavaScript in their browsers, lazy-loading wouldn’t work at all.</p></blockquote>
<p>The plugin uses a similar implementation that is being discussed in the core ticket. Arntz described it as a &#8220;progressive enhancement,&#8221; where a user&#8217;s website performance will &#8220;magically improve without intervention,&#8221; as more browsers add support for the loading attribute.</p>
<p>With the release of this plugin, and Google&#8217;s input on the related trac ticket, it&#8217;s clear that the company is interested in seeing WordPress core support the new loading attribute. Chrome Engineering Manager <a href="https://addyosmani.com/" rel="noopener noreferrer" target="_blank">Addy Osmani</a> <a href="https://core.trac.wordpress.org/ticket/44427#comment:38" rel="noopener noreferrer" target="_blank">commented</a> on the ticket 10 days ago to lend his support for the effort and make a few recommendations.</p>
<p>&#8220;I&#8217;m very supportive of core getting support for native lazy-loading in a non-destructive manner,&#8221; Osmani said.</p>
<p>&#8220;The ideal change I would love to see in lazy-load plugins is deferring to native lazy-loading where supported and applying their fallback where it is not.&#8221; Osmani estimates that more than 17K origins are already using loading=lazy, according to Google&#8217;s telemetry.</p>
<p>Andy Potts, a software engineer at the BBC <a href="https://medium.com/bbc-design-engineering/native-lazy-loading-has-arrived-c37a165d70a5" rel="noopener noreferrer" target="_blank">reported</a> seeing major performance improvements after adopting native lazy loading. He implemented it on one of the company&#8217;s internal products, a site with approximately 3,000 active users per day:</p>
<p>&#8220;One of the most common actions on the site involves running a query which renders a list of up to 100 images — which I thought seemed like the ideal place to experiment with native lazy loading,&#8221; Potts said.</p>
<p>&#8220;Adding the loading attribute to the images <strong>decreased the load time on a fast network connection by ~50% — it went from ~1 second to &lt; 0.5 seconds, as well as saving up to 40 requests to the server</strong>. All of those performance enhancements just from adding one attribute to a bunch of images!&#8221;</p>
<p>Kris Gunnars, who operates <a href="http://searchfacts.com" rel="noopener noreferrer" target="_blank">searchfacts.com</a>, added Google&#8217;s new Native Lazyload plugin to his site and <a href="https://wordpress.org/support/topic/very-powerful-lazy-loading-plugin/" rel="noopener noreferrer" target="_blank">reported</a> remarkable performance improvements, especially on mobile.</p>
<p>&#8220;After I installed this, my mobile PageSpeed score went from 92 to 96 and it also shaved a whopping 1.5 seconds off of my Time to Interactive score,&#8221; Gunnars said.</p>
<p>With WordPress powering <a href="https://w3techs.com/technologies/details/cm-wordpress/all/all" rel="noopener noreferrer" target="_blank">34.5%</a> of the top 10 million websites, core support for native lazy loading stands to make a huge impact on the overall performance of the web. Progress on the <a href="https://core.trac.wordpress.org/ticket/44427" rel="noopener noreferrer" target="_blank">ticket</a> has been slow, as contributors continue discussing the best approach. In the meantime, users who are anxious to implement it on their sites can install any one of a number of plugins that are already available.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 06 Sep 2019 19:14:10 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:48;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:56:"WordPress.org blog: People of WordPress: Abdullah Ramzan";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"https://wordpress.org/news/?p=7086";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://wordpress.org/news/2019/09/people-of-wordpress-abdullah-ramzan/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5588:"<p><em>You’ve probably heard that WordPress is open-source software, and may know that it’s created and run by volunteers. WordPress enthusiasts share many examples of how WordPress changed people’s lives for the better. This monthly series shares some of those lesser-known, amazing stories.</em></p>



<h2><strong>Meet Abdullah Ramzan, from Lahore, Punjab, Pakistan.</strong></h2>



<p>Abdullah Ramzan was born and brought up in the under-developed city of <a href="https://en.wikipedia.org/wiki/Layyah"><strong>​Layyah​</strong></a>, which is situated in Southern Punjab, Pakistan and surrounded by desert and the river ​Sindh​.</p>



<p>He graduated from college in his home town and started using a computer in ​2010​ when he joined <a href="https://gcuf.edu.pk/"><strong>​Government College University Faisalabad​</strong></a>. Abdullah’s introduction to WordPress happened while he was finishing the last semester of his degree. His final project was based in WordPress.</p>



<p>Ramzan’s late mother was the real hero in his life, helping him with his Kindergarten homework and seeing him off to school every day.&nbsp;</p>



<p>Before her heart surgery, Ramzan visited her in the hospital ICU, where she hugged him and said: ​“<strong>Don’t worry, everything will be good</strong>.” Sadly, his mother died during her surgery. However, her influence on Ramzan’s life continues.</p>



<h3><strong>Start of Ramzan’s Career:</strong></h3>



<p>After graduation, Ramzan struggled to get his first job. He first joined PressTigers<strong>​</strong> as a Software Engineer and met Khawaja Fahad Shakeel<a href="https://twitter.com/FahadShakeel"><strong>​</strong></a>, his first mentor. Shakeel provided Ramzan with endless support. Something had always felt missing in his life, but he felt like he was on the right track for the first time in his life when he joined the WordPress community.&nbsp;</p>



<h3><strong>Community – WordCamps and Meetups:</strong></h3>



<p>Although Ramzan had used WordPress since ​2015​, attending WordPress meetups and open source contributions turned out to be a game-changer for him. He learned a lot from the WordPress community and platform, and developed strong relationships with several individuals. One of them is <a href="https://twitter.com/jainnidhi03"><strong>​</strong></a>Nidhi Jain​ from Udaipur India who he works with on WordPress development. The second is <a href="https://twitter.com/desrosj"><strong>​</strong></a>Jonathan Desrosiers​ who he continues to learn a lot from.</p>



<p>In addition, Usman Khalid<a href="https://twitter.com/Usman__Khalid"><strong>​</strong></a>, the lead organizer of WC Karachi, mentored Ramzan, helping him to develop his community skills.&nbsp;</p>



<p>With the mentorship of these contributors, Ramzan is confident supporting local WordPress groups and helped to organize ​WordCamp Karachi​, where he spoke for the first time at an international level event. He believes that WordPress has contributed much to his personal identity.&nbsp;</p>



<img src="https://i0.wp.com/wordpress.org/news/files/2019/09/AbdullahRamzan.jpeg?resize=632%2C422&ssl=1" alt="Abdullah Ramzan among a group of community members at WordCamp Karachi 2018" class="wp-image-7088" />Abdullah Ramzan at WordCamp Karachi 2018



<h3><strong>WordPress and the Future:</strong></h3>



<p>As a <a href="https://www.meetup.com/WordPress-Lahore/members/?op=leaders&sort=name"><strong>​co-organizer of WordPress Meetup Lahore,​</strong></a> he would love to involve more people in the community leadership team, to provide a platform for people to gather under one roof, to learn and share something with each other. </p>



<p>But he has loftier ambitions. Impressed by <a href="https://walktowc.eu/">Walk to WordCamp Europe</a>, Abdullah is seriously considering walking to WordCamp Asia. He also one day hopes for the opportunity to serve his country as a senator of Pakistan<a href="http://www.senate.gov.pk/"><strong>​</strong></a> and intends to enter the next senate election.</p>



<h3><strong>Words of Encouragement</strong></h3>



<p>Abdullah Ramzan knows there is no shortcut to success. “You have to work hard to achieve your goals,” explained Ramzan. He still has much he wishes to accomplish and hopes to be remembered for his impact on the project.</p>



<p>Abdullah believes WordPress can never die as long as people don’t stop innovating to meet new demands. The beauty of WordPress is that it is made for everyone.</p>



<p>Ramzan encouraged, “If you seriously want to do something for yourself, do something for others first. Go for open source, you’ll surely learn how to code. You’ll learn how to work in a team. Join local meetups, meet with the folks: help them, learn from them, and share ideas.”</p>



<hr class="wp-block-separator" />



<div class="wp-block-image"><img src="https://i1.wp.com/wordpress.org/news/files/2019/07/heropress_large_white_logo.jpg?resize=109%2C82&ssl=1" alt="" class="wp-image-7025" width="109" height="82" /></div>



<p><em>This post is based on an article originally published on HeroPress.com, a community initiative created by <a href="https://profiles.wordpress.org/topher1kenobe/">Topher DeRosia</a>. HeroPress highlights people in the WordPress community who have overcome barriers and whose stories would otherwise go unheard.</em></p>



<p><em>Meet more WordPress community members over at </em><a href="https://heropress.com/"><em>HeroPress.com</em></a><em>!</em></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 06 Sep 2019 18:21:22 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Yvette Sonneveld";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:49;a:6:{s:4:"data";s:13:"
	
	
	
	
	
	
";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:74:"WPTavern: WordSesh EMEA Schedule Published, Registration Opens September 9";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"https://wptavern.com/?p=93428";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:84:"https://wptavern.com/wordsesh-emea-schedule-published-registration-opens-september-9";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3153:"<p><a href="https://wptavern.com/wordsesh-emea-coming-september-25-a-new-virtual-wordpress-event-for-europe-middle-east-and-africa" rel="noopener noreferrer" target="_blank">WordSesh EMEA</a>, a 12-hour virtual conference designed for the WordPress community in the Middle East, Europe, and Africa, has <a href="https://wordsesh.com/" rel="noopener noreferrer" target="_blank">published the full schedule</a> for the upcoming event. The lineup includes speakers from the UK to Cape Town to Sri Lanka, and other parts of the wider world of WordPress. Approximately 8 of the 11 speakers selected are from the targeted regions for this event. The remaining three are located in the U.S.</p>
<p><a href="https://i1.wp.com/wptavern.com/wp-content/uploads/2019/09/wordsesh-emea-speakers.jpg?ssl=1"><img /></a></p>
<p>WordSesh EMEA&#8217;s schedule features a healthy mix of topics, including multiple sessions on using Gatsby with WordPress, image optimization, webops, managing a business with mental illness, building SaaS apps with WordPress and Laravel, and Jetpack.</p>
<p><a href="https://muhammad.dev/" rel="noopener noreferrer" target="_blank">Muhammad Muhsin</a>, a Sri Lanka-based React developer at <a href="https://rtcamp.com" rel="noopener noreferrer" target="_blank">rtCamp</a>, will be presenting a session on using WordPress as a headless CMS with Gatsby. After Gatsby introduced themes, he started converting WordPress themes to Gatsby and experimenting with using WPGraphQL to get the content. He is also the lead developer for the <a href="http://GatsbyWPThemes.com" rel="noopener noreferrer" target="_blank">GatsbyWPThemes.com</a> project.</p>
<p>If you have ever heard the marketing term &#8220;digital experience platform&#8221; (DXP) and wondered what all the buzz is about, Karim Marucchi, CEO of <a href="https://crowdfavorite.com/" rel="noopener noreferrer" target="_blank">Crowd Favorite</a>, has a session titled &#8220;What&#8217;s All The Fuss About DXPs, and Why Should I Care?&#8221; He will explore a recent trend where enterprise clients are moving away from content management towards DXP&#8217;s that integrate online marketing tools.</p>
<p><a href="https://ahmadbilal.dev" rel="noopener noreferrer" target="_blank">Ahmad Bilal</a>, a developer based in Pakistan and 2019 Google Summer of Code student, will be presenting a session on GitHub Actions and how to automatically deploy a plugin to WordPress.org when tagging a new version on GitHub.</p>
<p>WordSesh EMEA provides an opportunity for viewers in the Middle East, Europe, and Africa to see virtual conference sessions during regular daytime hours, but it also gives viewers in the Western hemisphere a chance to hear speakers who they may never meet at a local WordCamp. It is scheduled for Wednesday, September 25, 2019, from 7:00-19:00 UTC. A handy dropdown on the schedule allows viewers to select their own timezone for the schedule display. Sessions will be conducted in English for this first EMEA event and will also be live captioned.</p>
<p>WordSesh EMEA is free for all to attend online and registration for tickets will be open Monday, September 9.</p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 05 Sep 2019 20:30:12 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sarah Gooding";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";O:42:"Requests_Utility_CaseInsensitiveDictionary":1:{s:7:" * data";a:8:{s:6:"server";s:5:"nginx";s:4:"date";s:29:"Tue, 01 Oct 2019 09:11:03 GMT";s:12:"content-type";s:8:"text/xml";s:4:"vary";s:15:"Accept-Encoding";s:13:"last-modified";s:29:"Tue, 01 Oct 2019 09:00:08 GMT";s:15:"x-frame-options";s:10:"SAMEORIGIN";s:4:"x-nc";s:9:"HIT ord 1";s:16:"content-encoding";s:2:"br";}}s:5:"build";s:14:"20190920194706";}}