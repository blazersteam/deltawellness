0+�]<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:2909;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-09-20 18:08:27";s:13:"post_date_gmt";s:19:"2019-09-20 18:08:27";s:12:"post_content";s:6282:"[vc_row][vc_column][vc_column_text]Delta Wellness Privacy Statement

<strong>PRIVACY STATEMENT</strong>

<strong>SECTION 1 WHAT DO WE DO WITH YOUR INFORMATION?</strong>

When you purchase something from our store, as part of the buying and selling process, we collect the personal information you give us such as your name, address and email address.
When you browse our store, we also automatically receive your computerâ€™s internet protocol (IP) address in order to provide us with information that helps us learn about your browser and operating system.
Email marketing (if applicable): With your permission, we may send you emails about our store, new products and other updates.

<strong>SECTION 2 CONSENT</strong>

How do you get my consent?
When you provide us with personal information to complete a transaction, verify your credit card, place an order, arrange for a delivery or return a purchase, we imply that you consent to our collecting it and using it for that specific reason only.
If we ask for your personal information for a secondary reason, like marketing, we will either ask you directly for your expressed consent, or provide you with an opportunity to say no.

<strong>How do I withdraw my consent?</strong>
If after you opt-in, you change your mind, you may withdraw your consent for us to contact you, for the continued collection, use or disclosure of your information, at anytime, by contacting us at Contact@deltawellnesscbd.com or mailing us at:
610 S Waverly Rd, Lansing, MI 48917

<strong>SECTION 3 DISCLOSURE</strong>

We may disclose your personal information if we are required by law to do so or if you violate our Terms of Service.

<strong>SECTION 4 THIRD-PARTY SERVICES</strong>

In general, the third-party providers used by us will only collect, use and disclose your information to the extent necessary to allow them to perform the services they provide to us.
However, certain third-party service providers, such as payment gateways and other payment transaction processors, have their own privacy policies in respect to the information we are required to provide to them for your purchase-related transactions.
For these providers, we recommend that you read their privacy policies so you can understand the manner in which your personal information will be handled by these providers.
In particular, remember that certain providers may be located in or have facilities that are located a different jurisdiction than either you or us. So if you elect to proceed with a transaction that involves the services of a third-party service provider, then your information may become subject to the laws of the jurisdiction(s) in which that service provider or its facilities are located.
As an example, if you are located in Canada and your transaction is processed by a payment gateway located in the United States, then your personal information used in completing that transaction may be subject to disclosure under United States legislation, including the Patriot Act.
Once you leave our storeâ€™s website or are redirected to a third-party website or application, you are no longer governed by this Privacy Policy or our websiteâ€™s Terms of Service.

<strong>Links</strong>
When you click on links on our store, they may direct you away from our site. We are not responsible for the privacy practices of other sites and encourage you to read their privacy statements.

<strong>SECTION 5 SECURITY</strong>

To protect your personal information, we take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.
If you provide us with your credit card information, the information is encrypted using secure socket layer technology (SSL) and stored with a AES-256 encryption. Although no method of transmission over the Internet or electronic storage is 100% secure, we follow all PCI-DSS requirements and implement additional generally accepted industry standards.

<strong>SECTION 6 COOKIES</strong>

Here is a list of cookies that we use. We™ve listed them here so you that you can choose if you want to opt-out of cookies or not.
_session_id, unique token, sessional, Allows to store information about your session (referrer, landing page, etc).
_shopify_visit, no data held, Persistent for 30 minutes from the last visit, Used by our website provider™s internal stats tracker to record the number of visits
_shopify_uniq, no data held, expires midnight (relative to the visitor) of the next day, Counts the number of visits to a store by a single customer.
cart, unique token, persistent for 2 weeks, Stores information about the contents of your cart.
_secure_session_id, unique token, sessional
storefront_digest, unique token, indefinite If the shop has a password, this is used to determine if the current visitor has access.

<strong>SECTION 7  AGE OF CONSENT</strong>

By using this site, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.

<strong>SECTION 8 CHANGES TO THIS PRIVACY POLICY</strong>

We reserve the right to modify this privacy policy at any time, so please review it frequently. Changes and clarifications will take effect immediately upon their posting on the website. If we make material changes to this policy, we will notify you here that it has been updated, so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we use and/or disclose it.
If our store is acquired or merged with another company, your information may be transferred to the new owners so that we may continue to sell products to you.

<strong>QUESTIONS AND CONTACT INFORMATION</strong>

If you would like to: access, correct, amend or delete any personal information we have about you, register a complaint, or simply want more information contact our Privacy Compliance Officer at Contact@deltawellnesscbd.com or by mail at
610 S Waverly Rd, Lansing, MI 48917[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:17:"Privacy Statement";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:17:"privacy-statement";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-09-20 18:38:33";s:17:"post_modified_gmt";s:19:"2019-09-20 18:38:33";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:41:"http://deltawellnesscbd.com/?page_id=2909";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}